import React from "react";
import { Grid, Typography, useTheme } from "@material-ui/core";

export default function CateList({
  cates,
  selected,
  handleChoose,
}: {
  cates: string[];
  selected: string[];
  handleChoose: (label: string) => void;
}) {
  const theme = useTheme();

  return (
    <Grid container spacing={1}>
      {cates.map((cate) => (
        <Grid item key={cate} style={{ cursor: "pointer" }}>
          <div
            onClick={() => handleChoose(cate)}
            style={{
              padding: "8px 10px",
              border: "1px solid #ddd",
              borderColor:
                (selected.includes(cate) && theme.palette.primary.main) ||
                "#ddd",
              backgroundColor: selected.includes(cate) && "#EDE9FB",
              borderRadius: 4,
            }}
          >
            <Typography
              color={(selected.includes(cate) && "primary") || undefined}
            >
              {cate}
            </Typography>
          </div>
        </Grid>
      ))}
    </Grid>
  );
}
