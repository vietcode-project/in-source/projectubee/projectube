import { Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Image from "next/image";
import React, { ReactElement } from "react";

const useStyles = makeStyles({
  root: {
    padding: "10px",
    backgroundColor: "#CD5038",
    maxHeight: "80px",
    width: "313px",
    height: "72px",
    borderRadius: "20px",
  },
  img: {
    borderRadius: "100%",
  },
  bold: {
    fontWeight: "bold",
    color: "white",
  },
  light: {
    fontWeight: 400,
    color: "white",
  },
  thin: {
    fontSize: 12,
    fontWeight: 100,
    color: "white",
  },
});

interface Props {
  src;
  title;
  id;
  des?;
}
export function Usercard(props: Props): ReactElement {
  const classes = useStyles();

  return (
    <div>
      <Grid container className={classes.root} alignItems="center">
        <Grid item xs={3}>
          <Image
            className={classes.img}
            src={props.src}
            width={48}
            height={48}
          ></Image>
        </Grid>
        <Grid container item xs={8} alignItems="center">
          <Grid container direction="row" justify="space-between">
            <Typography variant="h5" className={classes.bold}>
              {props.title}
            </Typography>
            <Typography variant="h5" className={classes.light}>
              {props.id}
            </Typography>
          </Grid>
          <Typography variant="h5" className={classes.thin}>
            {props.des}
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
}
