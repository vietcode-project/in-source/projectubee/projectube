import { gql, useQuery } from "@apollo/client";
import {
  Divider,
  Grid,
  makeStyles,
  Typography,
  IconButton,
} from "@material-ui/core";
import Image from "next/image";
import { useRouter } from "next/router";
import * as React from "react";
import { useState, useEffect } from "react";
import { useDate } from "components/Calendar";
import { DetailedData as UDetData } from "../../graphql/UserFragment";

const useStyles = makeStyles((theme) => ({
  Calen: {
    width: 42,
    height: 42,
    position: "relative",
    marginTop: 4,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
    borderRadius: "100%",
    "&:hover": {
      backgroundColor: "#EDE9FB",
      color: "#532BDC",
      transition: "all .3s",
    },
    "&:hover >*": {
      color: "#532BDC",
      backgroundColor: "#532BDC",
    },
  },
  CalenS: {
    width: 42,
    height: 42,
    marginTop: 4,
    position: "relative",
    display: "flex",
    justifyContent: "center",
    cursor: "pointer",
    alignItems: "center",
    borderRadius: "100%",
    color: "white",
    background: "#532BDC",
  },
  CalendotS: {
    position: "absolute",
    width: 4,
    height: 4,
    background: "#532BDC",
    borderRadius: "100%",
    left: " 45.24%",
    right: "45.24%",
    top: "73.81%",
    bottom: "16.67%",
  },
  Calendotother: {
    position: "absolute",
    width: 4,
    height: 4,
    background: "#DEE4ED",
    borderRadius: "100%",
    left: " 45.24%",
    right: "45.24%",
    top: "73.81%",
    bottom: "16.67%",
  },
  pre: {
    color: "#DEE4ED",
  },
}));

export default function Calendar() {
  const { selectedDate, setSelectedDate } = useDate();
  const router = useRouter();
  const { error, data } = useQuery(TIME_QUERY);

  useEffect(() => {
    setSelectedDate(new Date());
  }, []);

  const timeofevent = [];
  if (data) {
    data.me.subscribing_events.forEach((e) => {
      timeofevent.push(new Date(e.start_time).toDateString());
    });
  }
  if (error) {
    router.push("/404");
  }

  const DAYS_OF_THE_WEEK = ["CN", "T2", "T3", "T4", "T5", "T6", "T7"];
  const MONTHS = [
    "Tháng 1",
    "Tháng 2",
    "Tháng 3",
    "Tháng 4",
    "Tháng 5",
    "Tháng 6",
    "Tháng 7",
    "Tháng 8",
    "Tháng 9",
    "Tháng 10",
    "Tháng 11",
    "Tháng 12",
  ];

  const start = new Date();
  start.setDate(1);
  // const [selectedDate, setSelectedDate] = useState(new Date());
  const [date, setDate] = useState(start);
  const [month, setMonth] = useState(date.getMonth());
  const [year, setYear] = useState(date.getFullYear());
  const [lastDay, setLastDay] = useState(getlastDay(date));
  const [prevLastDay, setprevlastDay] = useState(getprevLastDay(date));
  const [lastDayIndex, setlastDayIndex] = useState(getlastDayIndex(date));
  const [firstDayIndex, setfirstDayIndex] = useState(getfirstDayIndex(date));

  function CalendarBlock(props) {
    const classes = useStyles();
    var isEvented = false;
    const holdingvar = new Date(props.year, props.month, props.day);

    const handleClick = () => {
      setSelectedDate(holdingvar);
    };

    if (timeofevent.includes(holdingvar.toDateString())) {
      isEvented = true;
    }

    return holdingvar.toDateString() == selectedDate.toDateString() ? (
      <Grid item className={classes.CalenS}>
        <Typography align="center" variant="body1">
          {props.day}
        </Typography>
      </Grid>
    ) : (
      <Grid item className={classes.Calen} onClick={handleClick}>
        {isEvented ? (
          <>
            {props.type != "main" ? (
              <>
                <Typography
                  align="center"
                  variant="body1"
                  className={classes.pre}
                  style={{ background: "transparent" }}
                >
                  {props.day}
                </Typography>
                <div className={classes.Calendotother}></div>
              </>
            ) : (
              <>
                <Typography
                  align="center"
                  variant="body1"
                  style={{ color: "#532BDC", background: "transparent" }}
                >
                  {props.day}
                </Typography>
                <div className={classes.CalendotS}></div>
              </>
            )}
          </>
        ) : (
          <>
            {props.type != "main" ? (
              <Typography
                align="center"
                variant="body1"
                className={classes.pre}
                style={{ background: "transparent" }}
              >
                {props.day}
              </Typography>
            ) : (
              <Typography
                align="center"
                variant="body1"
                style={{ background: "transparent" }}
              >
                {props.day}
              </Typography>
            )}
          </>
        )}
      </Grid>
    );
  }

  function monthdebugger(month, action) {
    switch (action) {
      case "+":
        switch (month) {
          case 11:
            return 0;
          default:
            return month + 1;
        }
      case "-":
        switch (month) {
          case 0:
            return 11;
          default:
            return month - 1;
        }
    }
  }

  function yeardebugger(year, month, action) {
    switch (action) {
      case "+": {
        if (month == 11) {
          return year + 1;
        }
        return year;
      }

      case "-": {
        if (month == 0) {
          return year - 1;
        }
        return year;
      }
      default:
        return year;
    }
  }

  function getlastDay(date) {
    return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
  }

  function getprevLastDay(date) {
    return new Date(date.getFullYear(), date.getMonth(), 0).getDate();
  }

  function getfirstDayIndex(date) {
    return date.getDay();
  }

  function getlastDayIndex(date) {
    return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDay();
  }

  var nextDays = 7 - lastDayIndex - 1;

  const arr = [];
  const demo = [];

  for (let x = firstDayIndex; x > 0; x--) {
    arr.push(prevLastDay - x + 1);
    demo.push({
      day: prevLastDay - x + 1,
      month: monthdebugger(month, "-"),
      year: yeardebugger(year, month, "-"),
      type: "other",
    });
  }

  for (let i = 1; i <= lastDay; i++) {
    arr.push(i);
    demo.push({ day: i, month: month, year: year, type: "main" });
  }

  for (let j = 1; j <= nextDays; j++) {
    arr.push(j);
    demo.push({
      day: j,
      month: monthdebugger(month, "+"),
      year: yeardebugger(year, month, "+"),
      type: "other",
    });
  }

  useEffect(() => {
    setMonth(date.getMonth());
    setYear(date.getFullYear());
    setLastDay(getlastDay(date));
    setprevlastDay(getprevLastDay(date));
    setlastDayIndex(getlastDayIndex(date));
    setfirstDayIndex(getfirstDayIndex(date));
  }, [date]);

  return (
    <>
      <Grid
        container
        direction="column"
        style={{
          width: 360,
          padding: "16px 21px",
          background: "#FFFFFF",
          borderRadius: "8px",
          border: " 1px solid #DEE4ED",
        }}
      >
        <Grid
          style={{ marginBottom: "24px" }}
          container
          justify="space-between"
          alignItems="center"
        >
          <IconButton
            style={{
              height: 24,
              width: 24,
              background: " #FFFFFF",
              border: "1px solid #DEE4ED",
              borderRadius: 4,
              padding: 8,
            }}
            onClick={() => {
              setDate(new Date(year, month - 1, 1));
            }}
          >
            <Image src="/Icon_more.svg" width={8} height={8} />
          </IconButton>
          <Typography variant="h3">{MONTHS[month] + " " + year}</Typography>
          <IconButton
            style={{
              height: 24,
              width: 24,
              background: " #FFFFFF",
              border: "1px solid #DEE4ED",
              borderRadius: 4,
              padding: 8,
            }}
            onClick={() => {
              setDate(new Date(year, month + 1, 1));
            }}
          >
            <Image src="/Icon_moreright.svg" width={8} height={8} />
          </IconButton>
        </Grid>
        <Grid container justify="space-between">
          {DAYS_OF_THE_WEEK.map((e, i) => (
            <Typography
              variant="h4"
              color="primary"
              align="center"
              key={i}
              style={{ width: 42 }}
            >
              {e}
            </Typography>
          ))}
        </Grid>
        <Divider style={{ margin: "16px 0px 8px 0px" }} />
        <Grid container justify="space-between">
          {demo.map((e, i) => (
            <CalendarBlock
              key={i}
              day={e.day}
              month={e.month}
              year={e.year}
              type={e.type}
            ></CalendarBlock>
          ))}
        </Grid>
      </Grid>
    </>
  );
}

const TIME_QUERY = gql`
  query {
    me {
      ...UserDetailedFragment
    }
  }
  ${UDetData()}
`;
