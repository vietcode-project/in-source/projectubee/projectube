import { createContext, useContext, useState } from "react";

interface DateContext {
  selectedDate: Date;
  setSelectedDate: (d: Date) => void;
}

const SelectedDateContext = createContext<DateContext>({
  selectedDate: new Date(),
  setSelectedDate: () => {},
});

const SelectedDateProvider = ({ children }) => {
  const [selectedDate, setSelectedDate] = useState(new Date());

  return (
    <SelectedDateContext.Provider value={{ selectedDate, setSelectedDate }}>
      {children}
    </SelectedDateContext.Provider>
  );
};

function useDate() {
  const context = useContext(SelectedDateContext);
  if (context === undefined) {
    throw new Error("useCount must be used within a CountProvider");
  }
  return context;
}
export { useDate, SelectedDateProvider };
