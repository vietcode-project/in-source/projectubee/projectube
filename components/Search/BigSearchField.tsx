import {
  Button,
  CircularProgress,
  InputAdornment,
  Paper,
  Popper,
  TextField,
  Typography,
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { makeStyles } from "@material-ui/core";
import Image from "next/image";
import React, { ChangeEvent, useEffect, useState } from "react";
import { useLazyQuery, useQuery } from "@apollo/client";
import { useRouter } from "next/router";
import { SearchCard } from "components/Search/SearchCard";
import IOrg from "types/Org";

const useStyles = makeStyles((theme) => ({
  search: {
    borderRadius: "6px",
    position: "relative",
  },
  searchIcon: {
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 10,
  },
  inputText: {
    borderRadius: "6px",
    color: "#8D9198",
    width: "100%",
    backgroundColor: "#F8F8F8",
    borderColor: "#F8F8F8",
    padding: theme.spacing(0.5, 0.5, 0.5, 0.5),
    cursor: "pointer",
  },
  inputRoot: {
    paddingLeft: "10px",
  },
  inputInput: {
    "&::placeholder": {
      fontWeight: 400,
      fontSize: "16px",
      color: "#8D9198",
    },
    cursor: "pointer",
  },
}));

interface IProps {
  queryevent?: any;
  queryorg?: any;
  queryrecruitment?: any;
}

export const BigSearchField = ({
  queryevent,
  queryorg,
  queryrecruitment,
}: IProps) => {
  const classes = useStyles();
  const router = useRouter();
  const [searchinput, setsearchinput] = useState("");
  const [searchResult, setSearchResult] = useState([]);

  const [event_search, { data: dataEvent, loading: loadingEvent }] =
    useLazyQuery(queryevent);

  const [org_search, { data: dataOrg, loading: loadingOrg }] =
    useLazyQuery(queryorg);

  useEffect(() => {
    if (queryevent) {
      event_search({
        variables: {
          filterEvent: { name: { re: searchinput } },
        },
      });
    }

    if (queryorg) {
      org_search({
        variables: {
          filterOrg: { name: { re: searchinput } },
        },
      });
    }
  }, [searchinput]);

  useEffect(() => {
    if (searchinput === "") {
      setSearchResult([]);
    }
    setSearchResult([...(dataOrg?.orgs || []), ...(dataEvent?.events || [])]);
  }, [dataOrg, dataEvent]);

  const keyPress = (e) => {
    if (e.keyCode == 13) {
      router.push({
        pathname: "/search",
        query: { keyword: searchinput },
      });
    }
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
    const { value } = e.target;
    if (value === "") {
      setSearchResult([]);
    }
    setsearchinput(value);
  };

  return (
    <>
      <Autocomplete
        placeholder="Tìm kiếm sự kiện và tổ chức"
        id="search-dropdown"
        freeSolo
        fullWidth
        options={searchResult.map((result) => result.name)}
        loading={loadingOrg || loadingEvent}
        renderInput={(params) => (
          <>
            <TextField
              {...params}
              onChange={handleChange}
              onKeyDown={keyPress}
              placeholder="Tìm kiếm sự kiện và tổ chức.."
              className={classes.inputText}
              InputProps={{
                ...params.InputProps,
                classes: {
                  root: classes.inputRoot,
                  input: classes.inputInput,
                },
                startAdornment: (
                  <InputAdornment position="start">
                    <Image
                      className={classes.searchIcon}
                      src="/Icon_search.svg"
                      width={12}
                      height={12}
                    />
                  </InputAdornment>
                ),
                endAdornment: (
                  <>
                    {loadingOrg || loadingEvent ? (
                      <CircularProgress color="inherit" size={20} />
                    ) : null}
                  </>
                ),
                disableUnderline: true,
              }}
            />
          </>
        )}
        renderOption={(option) => {
          return (
            <>
              <SearchCard
                data={searchResult.find((result) => result.name === option)}
              />
            </>
          );
        }}
        ListboxProps={{ style: { maxHeight: "400px" } }}
        PaperComponent={({ children }) => {
          return (
            <Paper style={{ width: "100%" }}>
              <div
                style={{
                  fontSize: "16px",
                  fontWeight: 400,
                  lineHeight: "16px",
                  paddingTop: "24px",
                  paddingLeft: "20px",
                  minWidth: "350px",
                  paddingBottom: "20px",
                }}
              >
                Kết quả tìm kiếm
              </div>
              {children}
              {!loadingEvent && !loadingOrg && searchResult.length === 0 && (
                <Typography
                  variant="h4"
                  style={{
                    width: "100%",
                    fontSize: "12px",
                    fontWeight: 400,
                    lineHeight: "16px",
                    paddingLeft: "20px",
                    paddingBottom: "20px",
                    color: "#8D9198",
                  }}
                >
                  Không có kết quả tìm kiếm phù hợp
                </Typography>
              )}
            </Paper>
          );
        }}
        PopperComponent={(props) => {
          return (
            <Popper
              {...props}
              style={{ marginTop: "8px", zIndex: 10 }}
              disablePortal={true}
              placement="bottom"
              modifiers={{
                flip: {
                  enabled: false,
                },
                hide: {
                  enabled: false,
                },
                preventOverflow: {
                  enabled: false,
                  boundariesElement: "scrollParent",
                },
              }}
            />
          );
        }}
      />
    </>
  );
};
