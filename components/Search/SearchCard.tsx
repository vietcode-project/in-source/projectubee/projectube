import { Grid, makeStyles, Typography } from "@material-ui/core";
import Link from "next/link";
import React, { ReactElement } from "react";

const useStyle = makeStyles((theme) => ({
  root: {
    padding: "10px",
  },
  img: {
    borderRadius: "12px",
  },
  categories: {
    padding: theme.spacing(0.5, 1, 0.5, 1),
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
}));

export const SearchCard = (props) => {
  const classes = useStyle();
  const data = props.data;

  return (
    <>
      {data?.__typename === "Recruit" && (
        <Link href={`/recruit/${data?.id}`}>
          <a>dcm</a>
        </Link>
      )}
      {data?.__typename !== "Recruit" && (
        <Link
          href={`/${
            data?.__typename && data?.__typename === "Event" ? "events" : "orgs"
          }/${data?.id}`}
        >
          <a>
            <Grid
              className={classes.root}
              container
              spacing={1}
              justify="space-between"
            >
              <Grid item>
                <div
                  className={classes.img}
                  style={{
                    width: "60px",
                    paddingTop: "50px",
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                    backgroundRepeat: "no-repeat",
                    backgroundImage: `url(${
                      data?.coverUrl || data?.avatarUrl
                        ? data?.coverUrl || data?.avatarUrl
                        : "/temp-cover.jpg"
                    })`,
                  }}
                />
              </Grid>
              <Grid item style={{ flex: 1, paddingLeft: "10px" }}>
                <Grid container>
                  <Grid item>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      style={{ paddingBottom: "6px" }}
                    >
                      {data?.start_time
                        ? new Date(data?.start_time).toLocaleDateString(
                            "default",
                            {
                              weekday: "long",
                              year: "numeric",
                              month: "long",
                              day: "numeric",
                            }
                          )
                        : "Tổ chức"}
                    </Typography>

                    <Typography
                      variant="h4"
                      style={{ paddingBottom: "8px", fontSize: "19px" }}
                    >
                      {data?.name?.length > 28
                        ? data?.name?.slice(0, 28) + "..."
                        : data?.name}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={1}
                  justify="space-between"
                  alignItems="center"
                >
                  <Grid item>
                    <Grid container spacing={1}>
                      {data?.categories &&
                        data?.categories?.map(
                          (cate, i) =>
                            i < 6 && (
                              <Grid item key={cate}>
                                <div className={classes.categories}>
                                  <Typography
                                    variant="body2"
                                    color="textPrimary"
                                  >
                                    {cate}
                                  </Typography>
                                </div>
                              </Grid>
                            )
                        )}
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </a>
        </Link>
      )}
    </>
  );
};
