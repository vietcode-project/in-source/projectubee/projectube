import { Button } from "@material-ui/core";
import Image from "next/image";
import React from "react";

export function SearchMobileButton({ state, onClick }) {
  return (
    <Button
      variant="outlined"
      aria-haspopup="true"
      onClick={onClick}
      style={{
        padding: "12px",
        minWidth: "0",
        border: "1px solid #DEE4ED",
        marginRight: "8px",
        display: state ? "none" : null,
      }}
    >
      <Image src="/Icon_search.svg" width={16} height={16} />
    </Button>
  );
}
