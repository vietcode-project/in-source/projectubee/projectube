import {
  Button,
  CircularProgress,
  Dialog,
  Grid,
  Typography,
  useMediaQuery,
  Theme,
} from "@material-ui/core";
import React from "react";
export interface Props {
  open: boolean;
  button1ctn: string;
  button2ctn: string;
  stylebtn1?: Object;
  stylebtn2?: Object;
  Title: string;
  context: string;
  handleClose: () => void;
  handleConfirm: React.FormEventHandler;
  handleBack: () => void;
  loading: boolean;
}

export function ConfirmUpdate({
  handleBack,
  handleClose,
  button1ctn,
  button2ctn,
  Title,
  context,
  open,
  stylebtn1,
  stylebtn2,
  handleConfirm,
  loading,
}: Props) {
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.between(0, 384)
  );

  return (
    <Dialog
      maxWidth="md"
      onClose={handleClose}
      open={open}
      PaperProps={{
        style: {
          width: "429px",
          height: isDevicePhone ? "auto" : "202px",
          textAlign: "center",
        },
      }}>
      <Grid
        container
        direction="column"
        justify="space-around"
        alignItems="center"
        style={{ height: "100%", padding: "32px 58px" }}>
        <Grid item style={{ marginBottom: isDevicePhone && "8px" }}>
          <Typography variant="h3">
            <b>{Title}</b>
          </Typography>
        </Grid>
        <Grid item style={{ marginBottom: isDevicePhone && "8px" }}>
          <Typography variant="h5" color="textSecondary" align="center">
            {context}
          </Typography>
        </Grid>
        <Grid item container justify="space-evenly">
          <Grid item style={{ marginBottom: isDevicePhone && "8px" }}>
            <Button variant="outlined" style={stylebtn1} onClick={handleBack}>
              {button1ctn}
            </Button>
          </Grid>

          <Button
            variant="contained"
            style={stylebtn2}
            color="primary"
            onClick={handleConfirm}
            disabled={loading}>
            {loading ? (
              <CircularProgress color="inherit" size={25} />
            ) : (
              button2ctn
            )}
          </Button>
        </Grid>
      </Grid>
    </Dialog>
  );
}
