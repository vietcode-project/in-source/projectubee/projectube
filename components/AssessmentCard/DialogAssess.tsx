import {
  Dialog,
  Typography,
  TextField,
  Grid,
  IconButton,
  makeStyles,
  Button,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import { gql, useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import { ORG_QUERY } from "graphql/Queries";

const useStyles = makeStyles(() => ({
  star: {
    "&:hover": {
      cursor: "pointer",
    },
  },
}));

const DialogAssess = ({
  openAssess,
  closeAssess,
  type,
  id,
  initialRate,
  initialComment,
  name,
}) => {
  const [rate, setRate] = useState(initialRate);
  const [comment, setComment] = useState(initialComment);
  const [state, setState] = useState("");
  const classes = useStyles();
  const router = useRouter();
  //Mutation
  const [
    createEventAssessment,
    {
      data: EventAssessment,
      loading: loadingEventAssessment,
      error: ErrorEventASS,
    },
  ] = useMutation(CREATE_EVENT_ASSESSMENT_MUTATION);

  const [
    createOrgAssessment,
    { data: OrgAssessment, loading: loadingOrgAssessment, error: ErrorOrgASS },
  ] = useMutation(CREATE_ORG_ASSESSMENT_MUTATION, {
    // refetchQueries: () => [{ query: ORG_QUERY, variables: { id: id } }],
  });

  const [
    updateAssessment,
    {
      data: dataUpdateAssessment,
      loading: loadingUpdateAssessment,
      error: errorUpdateAssessment,
    },
  ] = useMutation(UPDATE_ASSESSMENT);

  const handleSubmitAssessment = () => {
    if (initialComment === "") {
      if (type === "Event") {
        createEventAssessment({
          variables: {
            event_id: id,
            rate: rate,
            comment: comment,
          },
        })
          .then(() => router.reload())
          .catch((err) => console.error(err));
      }

      if (type === "Org") {
        createOrgAssessment({
          variables: {
            org_id: id,
            rate: rate,
            comment: comment,
          },
        })
          .then(() => {
            router.reload();
          })
          .catch((err) => console.error(err));
      }
    } else {
      updateAssessment({
        variables: {
          assessment_id: id,
          rate: rate,
          comment: comment,
        },
      })
        .then(() => router.reload())
        .catch((err) => console.error(err));
    }
  };

  //   Star display
  var rows = [];
  for (let i = 1; i < 6; i++) {
    rows.push(
      <Grid item style={{ marginRight: "16px" }} key={i}>
        <Image
          src={i > rate ? "/Icon_nostar.svg " : "/Icon_star.svg"}
          width={16}
          height={15}
          onClick={() => setRate(i)}
          className={classes.star}
        />
      </Grid>
    );
  }

  //  Comment
  const handleCommentChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.persist();
    setComment(e.target.value);
  };

  return (
    <Dialog
      PaperProps={{
        style: {
          width: "64%",
          height: "450px",
          paddingTop: 44,
          paddingLeft: 44,
          paddingRight: 44,
          paddingBottom: 0,
          position: "relative",
        },
      }}
      scroll="body"
      maxWidth="md"
      open={openAssess}
      onClose={closeAssess}
    >
      <IconButton
        aria-label="close"
        style={{ position: "absolute", right: "32px", top: "32px" }}
        onClick={closeAssess}
      >
        <CloseIcon />
      </IconButton>
      <Typography
        style={{
          textAlign: "center",
          color: "#000000",
          fontWeight: 700,
          fontSize: "24px",
          marginBottom: "28px",
        }}
      >
        Đăng nhận xét, đánh giá cho {name}
      </Typography>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        style={{ marginBottom: "24px" }}
      >
        {rows}
      </Grid>
      <TextField
        multiline
        fullWidth
        placeholder="Thêm nhận xét của bạn tại đây..."
        variant="outlined"
        value={comment}
        onChange={handleCommentChange}
        rows={9}
        style={{ marginBottom: "16px" }}
      />
      <Grid item xs={12} container justify="flex-end">
        <Button
          variant="contained"
          color="primary"
          disabled={
            rate === 0 ||
            comment === "" ||
            loadingEventAssessment ||
            loadingOrgAssessment ||
            EventAssessment ||
            OrgAssessment
          }
          style={{ padding: "12px 32px", fontSize: "16px" }}
          onClick={handleSubmitAssessment}
        >
          Đăng nhận xét
        </Button>
      </Grid>
    </Dialog>
  );
};

const CREATE_ORG_ASSESSMENT_MUTATION = gql`
  mutation ($org_id: String!, $rate: Float!, $comment: String!) {
    create_org_assessment(
      org_id: $org_id
      data: { rate: $rate, comment: $comment }
    ) {
      rate
      comment
    }
  }
`;

const CREATE_EVENT_ASSESSMENT_MUTATION = gql`
  mutation ($event_id: String!, $rate: Float!, $comment: String!) {
    create_event_assessment(
      event_id: $event_id
      data: { rate: $rate, comment: $comment }
    ) {
      rate
      comment
    }
  }
`;

const UPDATE_ASSESSMENT = gql`
  mutation ($assessment_id: String!, $rate: Float!, $comment: String!) {
    update_assessment(
      assessment_id: $assessment_id
      data: { rate: $rate, comment: $comment }
    ) {
      rate
      comment
    }
  }
`;

export default DialogAssess;
