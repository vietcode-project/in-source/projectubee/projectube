import { Button, Grid, makeStyles, Typography } from "@material-ui/core";
import React, { ReactElement } from "react";
import Image from "next/image";
import { averageParam } from "utils/math";
import { useRouter } from "next/router";

const useStyles = makeStyles(() => ({
  root: {
    padding: "18px 24px 24px 24px",
    border: "1px solid #DEE4ED",
    borderRadius: 8,
    width: "100%",
    height: 131,
  },
  btn: {
    padding: "13px 16px",
    textAlign: "center",
    border: "1px solid #EDE9FB",
    borderRadius: "6px",
    color: "#532BDC",
    fontSize: "16px",
    lineHeight: "100%",
  },
}));

const RateCard = ({ rated, openAssess, assessments, user }): ReactElement => {
  const classes = useStyles();
  const router = useRouter();

  return (
    <Grid container className={classes.root} justify="space-between">
      <Grid item xs={12} container justify="space-between" alignItems="center">
        <Grid item container xs={7} alignItems="center">
          <Grid item style={{ marginRight: "16px" }}>
            <Image src="/Icon_star.svg" width={19} height={19} />
          </Grid>
          {assessments?.length ? (
            <>
              <Typography
                style={{
                  fontSize: "24px",
                  fontWeight: 700,
                  color: "#1B2431",
                  paddingBottom: "6px",
                }}
              >
                {averageParam(assessments, "rate")}
              </Typography>
              <Typography
                style={{
                  fontSize: "14px",
                  fontWeight: 400,
                  color: "#1B2431",
                }}
              >
                /5
              </Typography>
            </>
          ) : (
            <Typography
              style={{ fontSize: "14px", fontWeight: 400, color: "#1B2431" }}
            >
              Chưa có đánh giá
            </Typography>
          )}
        </Grid>
        <Grid item xs={3} container justify="flex-end">
          <Typography
            style={{
              fontSize: "12px",
              fontWeight: 400,
              color: "#8D9198",
              display: assessments?.length ? "" : "none",
            }}
          >
            {assessments?.length ? assessments.length : 0} đánh giá
          </Typography>
        </Grid>
      </Grid>
      <Grid item xs={12} container>
        <Button
          fullWidth
          style={{
            width: "100%",
            margin: "16px auto 8px auto",
            boxShadow: "none",
          }}
          color="primary"
          variant={rated ? "contained" : "outlined"}
          className={classes.btn}
          onClick={
            user?.type == "User"
              ? openAssess
              : () => {
                  if (!user) {
                    router.push("/signin");
                  }
                }
          }
          disabled={rated || user?.type == "Org"}
        >
          {user?.type == "User" && rated
            ? "Bạn đã đánh giá rồi nhé"
            : "Thêm nhận xét"}
        </Button>
      </Grid>
    </Grid>
  );
};

export default RateCard;
