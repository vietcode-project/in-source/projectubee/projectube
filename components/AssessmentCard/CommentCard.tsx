import {
  Avatar,
  Grid,
  IconButton,
  makeStyles,
  Typography,
  Menu,
  MenuItem,
} from "@material-ui/core";
import React, { ReactElement, useState } from "react";
import Image from "next/image";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import DialogAssess from "./DialogAssess";
import { ConfirmUpdate } from "components/Confirmation";
import { gql, useMutation } from "@apollo/client";
import { useRouter } from "next/router";

const useStyles = makeStyles(() => ({
  root: {
    padding: "16px 0px 16px 16px",
    borderTop: "1px solid #DEE4ED",
    width: "100%",
  },
}));

const CommentCard = ({ data, type, iconState }): ReactElement => {
  const classes = useStyles();
  const router = useRouter();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [openAssess, setOpenAssess] = React.useState<boolean>(false);
  const [openConfirm, setOpenConfirm] = React.useState<boolean>(false);

  const [
    deleteAssessment,
    { loading: loadingDeleteAssess, error: errorDeleteAssess },
  ] = useMutation(DELETE_ASSESSMENT);

  //   Star display
  var rows = [];
  for (let i = 1; i < 6; i++) {
    rows.push(
      <Grid item style={{ marginRight: "8px" }} key={i}>
        <Image
          src={i > data.rate ? "/Icon_nostar.svg " : "/Icon_star.svg"}
          width={15}
          height={14}
        />
      </Grid>
    );
  }

  //dropdown icon handle comment
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const dropdown = (
    <Menu
      id="simple-menu"
      anchorEl={anchorEl}
      getContentAnchorEl={null}
      open={Boolean(anchorEl)}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      onClose={handleClose}
    >
      <MenuItem style={{ width: "210px" }} onClick={() => setOpenAssess(true)}>
        <Typography variant="h5">Chỉnh sửa</Typography>
      </MenuItem>
      <MenuItem onClick={() => setOpenConfirm(true)}>
        <Typography variant="h5" color="error">
          Xóa nhận xét
        </Typography>
      </MenuItem>
    </Menu>
  );

  //delete comment
  const handleDeleteComment = () => {
    deleteAssessment({
      variables: {
        assessment_id: data.id,
      },
    })
      .then(() => router.reload())
      .catch((err) => console.log(err));
  };
  return (
    <Grid item container xs={12} className={classes.root} direction="row">
      <Grid item xs={3}>
        <Avatar
          src={data.user.avatarUrl ? data.user.avatarUrl : "/temp-ava.jpg"}
          style={{ width: 60, height: 60 }}
        />
        <Grid>
          <Typography>
            {data.user.name && data.user.name.length > 10
              ? data.user.name.slice(0, 10) + "..."
              : data.user.name}
          </Typography>
        </Grid>
      </Grid>
      <Grid item xs={9}>
        <Grid
          item
          container
          style={{ marginBottom: 13 }}
          justify="space-between"
          direction="row"
        >
          <Grid item container direction="row" xs={6}>
            {rows}
          </Grid>

          {iconState && (
            <Grid item xs={2} container justify="flex-end">
              <IconButton
                aria-haspopup="true"
                style={{ padding: 0 }}
                onClick={handleClick}
              >
                <MoreHorizIcon />
              </IconButton>
              {dropdown}
              <DialogAssess
                openAssess={openAssess}
                closeAssess={() => setOpenAssess(false)}
                type={type}
                name={data.name}
                id={data.id}
                initialComment={data.comment}
                initialRate={data.rate}
              />
              <ConfirmUpdate
                Title="Bạn muốn xóa nhận xét?"
                context={`Hành động này sẽ xóa nhận xét `}
                button1ctn="Quay lại"
                button2ctn="Lưu thay đổi"
                open={openConfirm}
                handleBack={() => setOpenConfirm(false)}
                handleClose={() => setOpenConfirm(false)}
                handleConfirm={handleDeleteComment}
                loading={loadingDeleteAssess}
                stylebtn1={{ color: "#532BDC" }}
                stylebtn2={{ background: "#E4593B", color: "white" }}
              />
            </Grid>
          )}
        </Grid>
        <Typography
          style={{
            color: "#1B2431",
            fontSize: "18px",
            marginBottom: "10px",
          }}
        >
          {data.comment}
        </Typography>
        {/* <Typography style={{ color: "#8D9198", fontSize: "16px" }}>
          Posted on Jan 21 2021
        </Typography> */}
      </Grid>
    </Grid>
  );
};

const DELETE_ASSESSMENT = gql`
  mutation ($assessment_id: String!) {
    delete_assessment(assessment_id: $assessment_id)
  }
`;

export default CommentCard;
