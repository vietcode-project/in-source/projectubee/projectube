import {
  Button,
  Dialog,
  Grid,
  makeStyles,
  MenuItem,
  Select,
  Slider,
  TextField,
  Typography,
  useMediaQuery,
  Theme,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import axios from "axios";
import React from "react";

const useStyle = makeStyles(() => ({
  select: {
    marginBottom: 8,
    background: "white",
    fontWeight: 200,
    height: 36,
    "& > div": { padding: "7.5px 0 7.5px 16px" },
  },
}));

interface Question {
  question: string;
  context: string;
  type?: string;
}

interface IInput {
  id?: string;
  problem: string;
  detail: string;
  question1: number;
  question2: number;
  question3: number;
  question4: number;
}

interface Props {
  open: boolean;
  email;
  questions: Question[];
  handleClose: () => void;
  handleSubmit: () => void;
}

export function FeedbackForm({
  open,
  handleClose,
  handleSubmit,
  email,
  questions,
}: Props) {
  const classes = useStyle();
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );
  const [input, setInput] = React.useState<IInput>({
    id: email,
    problem: "",
    detail: "",
    question1: 3,
    question2: 3,
    question3: 3,
    question4: 3,
  });

  function handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  }

  const handleSubmitFeedback = () => {
    axios
      .put("https://vcsurvey.herokuapp.com/", input)
      .catch((e) => console.log(e))
      .then((res) => {
        handleSubmit();
      });
  };

  return (
    <Dialog
      maxWidth="md"
      onClose={handleClose}
      open={open}
      fullScreen={isDevicePhone}
    >
      <Grid style={{ padding: "40px" }}>
        <Grid container spacing={2} direction="column">
          <Grid item>
            <Button
              onClick={handleClose}
              variant="outlined"
              startIcon={<ArrowBackIcon />}
            >
              <Typography variant="button">Quay lại</Typography>
            </Button>
          </Grid>
          <Grid item>
            <Typography
              style={{
                fontSize: isDevicePhone ? "19px" : "24px",
                marginBottom: 8,
              }}
            >
              <b>Phản hồi về Projectube</b>
            </Typography>
            <Typography variant="h5" color="textSecondary">
              Đánh giá của bạn sẽ giúp Projectube ngày càng hoàn thiện hơn
            </Typography>
          </Grid>
          <Grid item>
            <Select
              fullWidth
              MenuProps={{
                getContentAnchorEl: null,
                anchorOrigin: {
                  vertical: "bottom",
                  horizontal: "left",
                },
              }}
              variant="outlined"
              className={classes.select}
              value={input.problem}
              name="problem"
              onChange={handleInputChange}
              displayEmpty
            >
              <MenuItem value="" disabled>
                <Typography color="textSecondary">Vấn đề của bạn</Typography>
              </MenuItem>
              <MenuItem value={"Không đăng nhập được"}>
                Không đăng nhập được
              </MenuItem>
              <MenuItem value={"Không nhận được mail confirm"}>
                Không nhận được mail confirm
              </MenuItem>
              <MenuItem value={"Quên mật khẩu"}>Quên mật khẩu</MenuItem>
              <MenuItem value={"Không tạo được tổ chức/sự kiện"}>
                Không tạo được tổ chức/sự kiện
              </MenuItem>
              <MenuItem value={"Tốc độ load thông tin"}>
                Tốc độ load thông tin
              </MenuItem>
              <MenuItem value={"Khác"}>
                Khác (xin hãy ghi chi tiết ở dưới)
              </MenuItem>
            </Select>
            <TextField
              fullWidth
              variant="outlined"
              name="detail"
              multiline
              onChange={handleInputChange}
              rows={4}
              placeholder="Mô tả chi tiết"
            ></TextField>
          </Grid>
          {questions.map((ques, i) => (
            <Grid item key={i}>
              <Typography variant="h5" style={{ marginBottom: 8 }}>
                {ques.question}
              </Typography>
              <Typography
                variant="h5"
                style={{ marginBottom: 16 }}
                color="textSecondary"
              >
                {ques.context}
              </Typography>
              <Grid container>
                <Slider
                  style={{ width: "90%", margin: "auto", marginBottom: 16 }}
                  defaultValue={3}
                  value={input[`question${i + 1}`]}
                  valueLabelDisplay="off"
                  name={`question${i + 1}`}
                  onChange={(e: any, newValue: number) => {
                    setInput((prev) => ({
                      ...prev,
                      [`question${i + 1}`]: newValue,
                    }));
                  }}
                  step={1}
                  marks={[
                    { value: 0, label: "0: 😢" },
                    { value: 1, label: 1 },
                    { value: 2, label: 2 },
                    { value: 3, label: 3 },
                    { value: 4, label: 4 },
                    { value: 5, label: "5: 😍" },
                  ]}
                  min={0}
                  max={5}
                ></Slider>
              </Grid>
            </Grid>
          ))}
          <Grid
            item
            xs={12}
            container
            direction="row"
            justify={isDevicePhone ? "space-evenly" : "flex-end"}
          >
            {isDevicePhone && (
              <Grid item xs={5}>
                <Button
                  fullWidth
                  variant="outlined"
                  color="secondary"
                  onClick={handleClose}
                >
                  Đóng
                </Button>
              </Grid>
            )}

            <Grid item xs={5}>
              <Button
                fullWidth
                variant="contained"
                color="primary"
                disabled={!(input.problem != "")}
                onClick={handleSubmitFeedback}
              >
                Gửi phản hồi
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Dialog>
  );
}
