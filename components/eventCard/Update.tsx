import React, { ChangeEvent, useEffect, useMemo, useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import Button from "@material-ui/core/Button";
import {
  TextField,
  useTheme,
  Select,
  MenuItem,
  makeStyles,
  useMediaQuery,
  Theme,
  IconButton,
  CircularProgress,
} from "@material-ui/core";
import { useRouter } from "next/router";
import CloseIcon from "@material-ui/icons/Close";
import { gql, useMutation } from "@apollo/client";
import CateList from "../Catelist";
import IEvent from "types/Event";
import cates from "config/categories.json";
import cities from "config/cities.json";
import { Alert } from "@material-ui/lab";

interface Props {
  open: boolean;
  handleClose: () => void;
  event: IEvent;
}

interface IInput {
  cover: File;
  name: string;
  categories: string[];
  description: string;
  start_time: Date | string;
  location: string;
  register_link: string;
}

const useStyles = makeStyles(() => ({
  menuPaper: {
    maxHeight: 200,
  },
}));

export function UpdateEventDialogs({ handleClose, open, event }: Props) {
  const theme = useTheme();
  const router = useRouter();
  const classes = useStyles();
  const [raweventData, setRawEventData] = useState<IEvent>();
  const [updateEvent, { loading: loadingUpdateEvent, data, error }] =
    useMutation(UPDATE_EVENT_MUTATION);
  const [locked, setLocked] = useState(false);
  const [coverPreview, setCoverPreview] = useState("");
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  const [isHeavyFile, setIsHeavyFile] = useState<boolean>(false);
  const [input, setInput] = useState<IInput>({
    cover: null,
    name: "",
    categories: [],
    description: "",
    start_time: null,
    location: "",
    register_link: null,
  });

  useEffect(() => {
    if (event) {
      setRawEventData(event);
      setInput({
        cover: null,
        name: event.name,
        categories: event.categories,
        description: event.description,
        start_time: convert(new Date(event.start_time)),
        location: event.location,
        register_link: event.register_link,
      });
      if (event.coverUrl) setCoverPreview(event.coverUrl);
    }
  }, []);

  const resetChange = () => {
    if (raweventData.coverUrl) {
      setCoverPreview(raweventData.coverUrl);
    } else {
      setCoverPreview("");
    }
    setInput({
      cover: null,
      name: raweventData.name,
      categories: raweventData.categories,
      description: raweventData.description,
      start_time: raweventData.start_time,
      location: raweventData.location,
      register_link: raweventData.register_link,
    });
  };

  function convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  function onSelectCover(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    if (e?.target?.files[0]?.size / 1024 / 1024 > 2) {
      setIsHeavyFile(true);
      setTimeout(() => {
        setIsHeavyFile(false);
      }, 5000);
    } else {
      setInput((prev) => ({ ...prev, cover: e.target.files[0] }));
      const objectUrl = URL.createObjectURL(e.target.files[0]);
      setCoverPreview(objectUrl);
    }
  }

  const removeNullPoint = (obj: Object) => {
    return Object.keys(obj)
      .filter((k) => obj[k] != null)
      .reduce((a, k) => ({ ...a, [k]: obj[k] }), {});
  };

  const handleInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    e.persist();
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!locked) {
      updateEvent({
        variables: {
          event_id: event.id,
          ...removeNullPoint(input),
        },
      })
        .catch((err) => console.error(err))
        .then((res) => {
          router.reload();
        });
      setLocked(true);
    }
  };

  if (error && locked) {
    setLocked(() => false);
  }

  return (
    <Dialog
      maxWidth="md"
      onClose={handleClose}
      open={open}
      fullScreen={isDevicePhone}
      PaperProps={{
        style: {
          padding: theme.spacing(7, 6),
          width: isDevicePhone ? "100%" : "60%",
          minWidth: "400px",
        },
      }}
    >
      <form onSubmit={handleSubmit}>
        <Grid
          container
          alignItems="center"
          style={{ marginBottom: theme.spacing(3) }}
        >
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography
            style={{ fontSize: isDevicePhone ? "1.5em" : "24px" }}
            display="inline"
          >
            Chỉnh sửa sự kiện {input.name}
          </Typography>
        </Grid>

        <Typography variant="h2">Ảnh bìa sự kiện</Typography>
        <Typography color="textSecondary" gutterBottom>
          Ảnh bìa sự kiện sẽ giúp nhiều người chú ý hơn
        </Typography>

        <Grid
          container
          justify="center"
          style={{
            paddingTop: "28.125%",
            paddingBottom: "28.125%",
            border: `${
              coverPreview == undefined
                ? "1px solid transparent"
                : "1px solid #DEE4ED"
            }`,
            borderRadius: "8px",
            backgroundImage: `${
              coverPreview != undefined ? `url(${coverPreview})` : ""
            }`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
        >
          <Grid item>
            {!coverPreview && (
              <Typography variant="h3">Kéo thả ảnh vào đây</Typography>
            )}
          </Grid>
        </Grid>
        <br />
        <Button variant="outlined" component="label">
          <Typography variant="button">Chọn ảnh từ thiết bị của bạn</Typography>
          <input type="file" accept="image/*" onChange={onSelectCover} hidden />
        </Button>

        <Divider style={{ margin: theme.spacing(4, 0) }} />

        <Typography variant="h2" style={{ marginBottom: theme.spacing(4) }}>
          Thông tin sự kiện
        </Typography>
        <Grid
          container
          style={{ marginBottom: theme.spacing(4) }}
          justify="space-between"
        >
          <Grid
            item
            xs={isDevicePhone ? 12 : 5}
            style={{
              width: isDevicePhone && "100%",
              marginBottom: isDevicePhone && "8px",
            }}
          >
            <Typography style={{ marginBottom: theme.spacing(2) }}>
              Tên sự kiện
            </Typography>
            <TextField
              style={{
                marginBottom: isDevicePhone && "8px",
              }}
              onChange={handleInputChange}
              value={input.name}
              name="name"
              placeholder="Nhập tên sự kiện của tổ chức"
              fullWidth
            />
          </Grid>
          <Grid
            item
            xs={isDevicePhone ? 12 : 5}
            style={{
              width: isDevicePhone && "100%",
              marginBottom: isDevicePhone && "8px",
            }}
          >
            <Typography style={{ marginBottom: theme.spacing(2) }}>
              Link tham gia sự kiện
            </Typography>
            <TextField
              onChange={handleInputChange}
              value={input.register_link}
              name="register_link"
              placeholder="Nhập đường link đăng kí của sự kiện"
              fullWidth
            />
          </Grid>
        </Grid>

        <div style={{ marginBottom: theme.spacing(4) }}>
          <Typography gutterBottom>Chủ đề của sự kiện</Typography>
          {(input.categories.length > 3 && (
            <Typography
              variant="body2"
              color="error"
              style={{ marginBottom: theme.spacing(2) }}
            >
              Bạn đã chọn quá giới hạn, vui lòng chọn lại
            </Typography>
          )) || (
            <Typography
              variant="body2"
              color="secondary"
              style={{ marginBottom: theme.spacing(2) }}
            >
              Vui lòng chọn tối đa 3 chủ đề
            </Typography>
          )}
          <CateList
            cates={cates}
            selected={input.categories}
            handleChoose={(label) => {
              if (input.categories.includes(label)) {
                setInput((prev) => ({
                  ...prev,
                  categories: prev.categories.filter((c) => c !== label),
                }));
              } else {
                setInput((prev) => ({
                  ...prev,
                  categories: [...prev.categories, label],
                }));
              }
            }}
          />
        </div>

        <div style={{ marginBottom: theme.spacing(4) }}>
          <Grid
            container
            justify="space-between"
            alignItems="center"
            style={{ marginBottom: theme.spacing(2) }}
          >
            <Grid item>
              <Typography>Giới thiệu sự kiện</Typography>
            </Grid>
            <Grid item>
              <Typography variant="body2" color="textSecondary">
                {(input.description && 5000 - input.description.length) || 5000}{" "}
                ký tự còn lại
              </Typography>
            </Grid>
          </Grid>

          <TextField
            onChange={handleInputChange}
            value={input.description}
            inputProps={{ maxLength: 5000 }}
            name="description"
            variant="outlined"
            label="Nhập văn bản"
            fullWidth
            rows={6}
            multiline
          />
        </div>

        <div style={{ marginBottom: theme.spacing(3) }}>
          <Grid
            container
            spacing={2}
            direction={isDevicePhone ? "column" : "row"}
          >
            <Grid item xs={isDevicePhone ? 12 : 6}>
              <Typography>Ngày tổ chức</Typography>
              <TextField
                onChange={handleInputChange}
                value={input.start_time}
                name="start_time"
                type="date"
                variant="outlined"
                fullWidth
              />
            </Grid>
            <Grid item xs={isDevicePhone ? 12 : 6}>
              <Typography>Địa điểm tổ chức</Typography>
              <Select
                fullWidth
                MenuProps={{
                  getContentAnchorEl: null,
                  anchorOrigin: {
                    vertical: "bottom",
                    horizontal: "left",
                  },
                  classes: { paper: classes.menuPaper },
                }}
                value={input.location}
                onChange={handleInputChange}
                variant="outlined"
                name="location"
              >
                {cities.map((city, index) => (
                  <MenuItem key={index} value={city}>
                    {city}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
          </Grid>
        </div>

        <Divider style={{ marginBottom: theme.spacing(3) }} />

        <Grid container spacing={1} justify="flex-end">
          <Grid item xs={isDevicePhone ? 5 : 3}>
            <Button
              fullWidth
              onClick={resetChange}
              variant="outlined"
              color="primary"
              style={{
                padding: "9px 20px",
              }}
              disabled={
                raweventData &&
                input.name == raweventData.name &&
                input.location == raweventData.location &&
                input.categories == raweventData.categories &&
                input.description == raweventData.description &&
                input.start_time == raweventData.start_time &&
                !input.cover
              }
            >
              <Typography display="inline">Hoàn tác thay đổi</Typography>
            </Button>
          </Grid>
          <Grid item xs={isDevicePhone ? 5 : 2}>
            <Button
              onClick={handleSubmit}
              variant="contained"
              color="primary"
              fullWidth
              disabled={
                !(
                  input.name != "" &&
                  input.location != "" &&
                  input.categories.length <= 3 &&
                  input.categories.length >= 1 &&
                  input.description != "" &&
                  input.start_time &&
                  !locked &&
                  !loadingUpdateEvent &&
                  !data
                )
              }
            >
              {!loadingUpdateEvent && !data ? (
                "Lưu thay đổi"
              ) : (
                <CircularProgress color="inherit" size={25} />
              )}
            </Button>
          </Grid>
        </Grid>
      </form>
      {isHeavyFile && (
        <div style={{ position: "fixed", top: 30, left: 30 }}>
          <Alert severity="error">
            Dung lượng ảnh được chọn đã vượt quá 2MB
          </Alert>
        </div>
      )}
    </Dialog>
  );
}

const UPDATE_EVENT_MUTATION = gql`
  mutation (
    $name: String!
    $event_id: String!
    $description: String!
    $start_time: DateTime
    $location: String
    $cover: Upload
    $categories: [String!]
    $register_link: String
  ) {
    update_event(
      data: {
        name: $name
        description: $description
        start_time: $start_time
        location: $location
        cover: $cover
        categories: $categories
        register_link: $register_link
      }
      event_id: $event_id
    ) {
      id
      name
    }
  }
`;
