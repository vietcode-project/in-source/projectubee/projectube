import React, { ReactElement } from "react";
import { Button, Grid, makeStyles, Typography } from "@material-ui/core";

import Link from "next/link";

const useStyles = makeStyles(() => ({
  root: {
    width: "100%",
    borderRadius: "15px",
    height: "445px",
  },
}));

interface Props {
  id: string;
  name: string;
  start_time: Date;
  coverUrl: string;
  org_name: string;
}

export function GiantEventCard(props: Props): ReactElement {
  const classes = useStyles();

  return (
    <Grid item style={{ width: "inherits" }}>
      <Link href={`/events/${props.id}`}>
        <a>
          <Grid
            item
            className={classes.root}
            style={{
              display: "flex",
              alignItems: "flex-end",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundImage: `linear-gradient(180deg, rgba(27, 36, 49, 0) 43.23%, rgba(27, 36, 49, 0.3) 67.19%, #1B2431 100%),url(${
                props.coverUrl ? props.coverUrl : "/temp-cover.jpg"
              })`,
            }}
          >
            <Grid
              style={{ padding: "24px 32px" }}
              container
              justify="space-between"
              alignItems="center"
            >
              <Grid>
                <Typography variant="h2" style={{ color: "white" }}>
                  <b>{props.name.slice(0, 100)}</b>
                </Typography>
                <Typography variant="h2" style={{ color: "white" }}>
                  by. {props.org_name}
                </Typography>
                <Typography style={{ color: "#8D9198" }}>
                  {new Date(props.start_time).toLocaleDateString("en-US", {
                    weekday: "short",
                    day: "numeric",
                    month: "short",
                    year: "numeric",
                  })}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </a>
      </Link>
    </Grid>
  );
}
