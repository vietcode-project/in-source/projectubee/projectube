import React, { ReactElement } from "react";
import Image from "next/image";
import Link from "next/link";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Typography, Grid, Avatar } from "@material-ui/core";
import IEvent from "types/Event";
import { AvatarGroup } from "@material-ui/lab";

interface Props extends IEvent {}

const useStyles = makeStyles((theme) => ({
  root: {
    border: "1px solid #DEE4ED",
    borderRadius: "15px",
    padding: theme.spacing(2),
    width: "inherits",
  },
  cover: {
    borderRadius: 12,
  },
  userAva: {
    width: 32,
    height: 32,
    border: `3px solid ${theme.palette.background.paper}`,
    borderRadius: "20px",
    backgroundPosition: "center",
    backgroundSize: "contain",
  },
  cate: {
    padding: "6px 10px",
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
  location: {
    background: "#EDE9FB",
    borderRadius: 4,
    padding: "6px 12px",
  },
}));

export function LargeEventCard({
  id,
  name,
  coverUrl,
  start_time,
  categories,
  subscribers,
  location,
}: Props): ReactElement {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Link href={`/events/${id}`}>
      <a>
        <div className={classes.root}>
          <div
            style={{
              width: "100%",
              paddingTop: "67%",
              backgroundSize: "cover",
              backgroundPosition: "center",
              backgroundRepeat: "no-repeat",
              backgroundImage: `url(${
                coverUrl ? coverUrl : "/temp-cover.jpg"
              })`,
            }}
            className={classes.cover}
          />

          <div style={{ height: theme.spacing(2) }} />

          <Typography variant="body2" color="textSecondary" gutterBottom>
            {start_time
              ? new Date(start_time).toLocaleDateString("en-US", {
                  weekday: "short",
                  day: "numeric",
                  month: "short",
                  year: "numeric",
                })
              : "chưa có"}
          </Typography>

          <Typography
            variant="h3"
            gutterBottom
            style={{ maxWidth: "100%", maxHeight: 22, overflow: "hidden" }}
          >
            {name}
          </Typography>

          {categories ? (
            <Grid container spacing={1} style={{ marginBottom: "6px" }}>
              {categories.slice(0, 2).map((cate) => (
                <Grid item key={cate}>
                  <div className={classes.cate}>
                    <Typography variant="body2">{cate}</Typography>
                  </div>
                </Grid>
              ))}
            </Grid>
          ) : (
            <i>
              <Typography variant="body2">Chưa phân loại</Typography>
            </i>
          )}
          <Grid container alignItems="center" justify="space-between">
            <Grid item xs={6}>
              <Grid container alignItems="center">
                {(subscribers &&
                  subscribers.length &&
                  subscribers.slice(0, 3).map((u, i) => (
                    <Grid
                      key={i}
                      style={{
                        marginRight: `${i != 2 ? "-10px" : "0"}`,
                      }}
                    >
                      <Avatar className={classes.userAva} src={u.avatarUrl} />
                    </Grid>
                  ))) || (
                  <i>
                    <Typography variant="body2" gutterBottom></Typography>
                  </i>
                )}
                <Grid
                  style={{
                    paddingLeft: "3px",
                    zIndex: -10,
                  }}
                >
                  <Typography color="primary">
                    {subscribers?.length > 3 && subscribers?.length - 3}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <div
                style={{
                  padding: theme.spacing(0.8, 1.6),
                  backgroundColor: "#EDE9FB",
                  borderRadius: 4,
                }}
              >
                <Typography variant="body2" style={{ color: "#532BDC" }}>
                  {location.slice(0, 10) || (
                    <>
                      Chưa có
                      <br />
                      địa điểm
                    </>
                  )}
                </Typography>
              </div>
            </Grid>
          </Grid>
        </div>
      </a>
    </Link>
  );
}
