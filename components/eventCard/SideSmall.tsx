import { gql, useMutation } from "@apollo/client";
import {
  Avatar,
  Button,
  Divider,
  Grid,
  IconButton,
  makeStyles,
  Typography,
  useTheme,
  Theme,
  Tooltip,
} from "@material-ui/core";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Link from "next/link";
import { httpsautoadd } from "utils/https";

interface StyleProps {
  subbed: boolean;
}

const useStyles = makeStyles<Theme, StyleProps>((theme: Theme) => ({
  cardDetail: {
    borderRadius: 8,
    border: "1px solid #DEE4ED",
    padding: theme.spacing(2),
  },
  notibtn: {
    color: ({ subbed }) => `${subbed ? "#532BDC" : "#DEE4ED"}`,
    border: ({ subbed }) =>
      `${subbed ? "1px solid #532BDC" : "1px solid #DEE4ED"}`,
    borderRadius: "8px",
    width: 40,
    height: 40,
    "&:hover": {
      color: "#532BDC",
      border: "1px solid #532BDC",
    },
    transition: "0.3s ease-in-out",
  },
  categories: {
    padding: theme.spacing(1, 1.25),
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
  userList: {
    display: "flex",
    flexFlow: "wrap row",
  },
  userAva: {
    width: 40,
    height: 40,
    border: `3px solid ${theme.palette.background.paper}`,
    borderRadius: "20px",
    backgroundPosition: "center",
    backgroundSize: "contain",
  },
  unsubbtn: {
    background: "#EDE9FB",
    color: "#532BDC",
    "&:hover": {
      background: "#EDE9FB",
      color: "#532BDC",
    },
  },
}));

export default function SideSmallEventCard({ user, data }) {
  const theme = useTheme();
  const router = useRouter();
  const [subbed, setSubbed] = useState(false);
  const classes = useStyles({ subbed: subbed });

  const [subcribe, { error: errorsub, loading: loadingsub, data: datasub }] =
    useMutation(EVENT_SUB, {
      variables: {
        event_id: data?.id,
      },
    });

  const [
    unsubcribe,
    { error: errorunsub, loading: loadingunsub, data: dataunsub },
  ] = useMutation(EVENT_UNSUB, {
    variables: {
      event_id: data?.id,
    },
  });

  useEffect(() => {
    if (
      data?.id &&
      user?.subscribing_events?.some(({ id }) => id == data?.id)
    ) {
      setSubbed(true);
    }
  }, [user, data?.id]);

  const handlesub = async (e: any) => {
    e.preventDefault();

    if (!user) {
      router.push("/signin");
      return;
    }

    if (!subbed) {
      try {
        await subcribe().then(() => setSubbed(true));
      } catch (err) {}
    }
  };

  const handleunsub = async (e: any) => {
    e.preventDefault();
    if (subbed) {
      try {
        await unsubcribe().then(() => setSubbed(false));
      } catch (err) {}
    }
  };

  return (
    <div className={classes.cardDetail} style={{ marginBottom: "16px" }}>
      <Typography variant="h2" color="textPrimary" gutterBottom>
        {data && data.name}
      </Typography>

      <Grid container spacing={1}>
        {data &&
          data.categories &&
          data.categories.map((cate) => (
            <Grid item key={cate}>
              <div className={classes.categories}>
                <Typography variant="body2" color="textPrimary">
                  {cate}
                </Typography>
              </div>
            </Grid>
          ))}
      </Grid>

      <Divider
        style={{
          margin: theme.spacing(3, 0),
          backgroundColor: "#DEE4ED",
        }}
      />

      <Grid container spacing={3}>
        <Grid
          item
          xs={data && data?.location?.length > 14 ? 12 : 4}
          style={{
            borderRight: `${
              data && data?.location?.length > 14 ? "" : "1px solid #DEE4ED"
            }`,
          }}
        >
          <Typography variant="body2" color="textSecondary" gutterBottom>
            {(data && "Địa Điểm") || "Hình Thức"}
          </Typography>
          <Typography variant="body1">{data && data.location}</Typography>
        </Grid>

        <Grid item xs={8}>
          <Typography variant="body2" color="textSecondary" gutterBottom>
            Ngày tổ chức
          </Typography>
          <Typography variant="body1">
            {data &&
              new Date(data.start_time).toLocaleDateString("default", {
                weekday: "long",
                year: "numeric",
                month: "long",
                day: "numeric",
              })}
          </Typography>
        </Grid>
      </Grid>

      <Typography
        variant="body2"
        color="textSecondary"
        gutterBottom
        style={{ marginTop: theme.spacing(2) }}
      >
        Lượt quan tâm
      </Typography>

      <Grid container alignItems="center" className={classes.userList}>
        {(data &&
          data?.subscribers &&
          data?.subscribers?.length &&
          data?.subscribers?.slice(0, 6).map((u, i) => (
            <Grid
              key={i}
              style={{
                marginRight: `${i != 5 ? "-12px" : "0"}`,
              }}
            >
              <Avatar className={classes.userAva} src={u.avatarUrl} />
            </Grid>
          ))) || (
          <i>
            <Typography variant="body2" gutterBottom>
              Chưa có lượt quan tâm
            </Typography>
          </i>
        )}
        <Grid
          style={{
            paddingLeft: "3px",
            zIndex: -10,
          }}
        >
          <Typography color="primary">
            {data &&
              data?.subscribers?.length > 6 &&
              data?.subscribers?.length - 6}
          </Typography>
        </Grid>
      </Grid>

      <Grid
        container
        style={{
          margin: "16px auto 8px auto",
        }}
      >
        <Grid item xs={9} style={{ marginRight: 8 }}>
          <Button
            style={{
              width: "100%",
              boxShadow: "none",
            }}
            variant="contained"
            color="primary"
            disabled={user?.type == "Org" || !data?.register_link}
            onClick={() => {
              router.replace(httpsautoadd(`${data?.register_link}`));
            }}
          >
            Đăng kí tham gia sự kiện
          </Button>
        </Grid>
        <Grid item xs={2}>
          <Tooltip title={subbed ? "Hủy theo dõi" : "Đăng kí theo dõi"}>
            <IconButton
              className={classes.notibtn}
              disabled={user?.type == "Org"}
              onClick={subbed ? handleunsub : handlesub}
            >
              <NotificationsIcon style={{ width: 27, height: 27 }} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      <Typography align="center" variant="body2" color="primary">
        Thông báo mới sẽ được gửi về Email bạn
      </Typography>
    </div>
  );
}

const EVENT_SUB = gql`
  mutation ($event_id: String!) {
    subscribe_event(event_id: $event_id)
  }
`;
const EVENT_UNSUB = gql`
  mutation ($event_id: String!) {
    unsubscribe_event(event_id: $event_id)
  }
`;
