import React, { ReactElement, useState } from "react";
import Link from "next/link";
// import Image from "next/image";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useRouter } from "next/router";
import { gql, useMutation } from "@apollo/client";
import {
  IconButton,
  Menu,
  MenuItem,
  useMediaQuery,
  Theme,
} from "@material-ui/core";
import { ConfirmUpdate } from "components/Confirmation";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import IEvent from "types/Event";
import { UpdateEventDialogs } from ".";

const useStyle = makeStyles((theme) => ({
  root: {},
  img: {
    borderRadius: "12px",
  },
  categories: {
    padding: theme.spacing(1),
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
  bold: {
    fontWeight: 800,
    fontSize: "16px",
  },
  light: {
    fontWeight: 300,
  },
  thin: {
    fontWeight: 100,
    color: "white",
  },
  loca: {
    backgroundColor: "#EDE9FB",
    borderRadius: "5px",
    padding: "6px 11px",
  },
}));

interface Props extends IEvent {}

export function ResponsiveSideEventCard(props: Props): ReactElement {
  const classes = useStyle();
  const [open, setOpen] = useState(false);
  const [open2, setOpen2] = useState(false);
  const router = useRouter();
  const isDeviceSmallPhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.between(0, 330)
  );
  const [deleteevent, { loading: loadingDeleteEvent }] =
    useMutation(DELETE_EVENT);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handledelete = async (event_id) => {
    const { data, errors } = await deleteevent({
      variables: { event_id: event_id },
    });

    if (errors) {
    }
    if (data) {
      router.reload();
    }
  };

  const dropdown = (
    <Menu
      id="simple-menu"
      anchorEl={anchorEl}
      getContentAnchorEl={null}
      open={Boolean(anchorEl)}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      onClose={handleClose}>
      <MenuItem style={{ width: "210px" }} onClick={() => setOpen(true)}>
        <Typography variant="h5">Chỉnh sửa</Typography>
      </MenuItem>
      <MenuItem onClick={() => setOpen2(true)}>
        <Typography variant="h5" color="error">
          Xóa sự kiện
        </Typography>
      </MenuItem>
    </Menu>
  );

  return (
    <Grid
      item
      style={{ position: "relative", width: "inherit", padding: "13px 16px" }}>
      <Grid style={{ position: "absolute", top: 0, right: 0 }}>
        {router.pathname != "/" && (
          <IconButton onClick={handleClick} aria-haspopup="true">
            <MoreHorizIcon />
          </IconButton>
        )}
        <ConfirmUpdate
          Title="Bạn muốn xóa sự kiện?"
          context={`Hành động này sẽ xóa vĩnh viễn các thông tin của "${props.name}"`}
          button1ctn="Quay lại"
          button2ctn="Lưu thay đổi"
          open={open2}
          handleBack={() => setOpen2(false)}
          handleClose={() => setOpen2(false)}
          handleConfirm={() => handledelete(props.id)}
          loading={loadingDeleteEvent}
          stylebtn1={{ color: "#532BDC" }}
          stylebtn2={{ background: "#E4593B", color: "white" }}
        />
        {
          <UpdateEventDialogs
            event={{ ...props }}
            open={open}
            handleClose={() => setOpen(false)}
          />
        }
      </Grid>
      <Link href={`/events/${props.id}`}>
        <a>
          <Grid
            container
            spacing={1}
            className={classes.root}
            direction={isDeviceSmallPhone ? "column" : "row"}
            justify="space-between">
            <Grid item>
              <div
                className={classes.img}
                style={{
                  width: "117px",
                  height: "97px",
                  backgroundSize: "cover",
                  backgroundPosition: "center",
                  backgroundRepeat: "no-repeat",
                  backgroundImage: `url(${
                    props.coverUrl ? props.coverUrl : "/temp-cover.jpg"
                  })`,
                }}
              />
            </Grid>
            <Grid item style={{ flex: 1, paddingLeft: "8px" }}>
              <Grid container>
                <Grid item style={{ marginTop: "9px" }}>
                  <Typography variant="body2" color="textSecondary">
                    {new Date(props.start_time).toLocaleDateString("default", {
                      weekday: "long",
                      year: "numeric",
                      month: "long",
                      day: "numeric",
                    })}
                  </Typography>

                  <Typography
                    variant="h2"
                    style={{
                      fontSize: "19px",
                      marginTop: "6px",
                      marginBottom: "8px",
                    }}>
                    {props.name.length > 20
                      ? props.name.slice(0, 20) + "..."
                      : props.name}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                container
                spacing={1}
                justify="space-between"
                alignItems="center">
                <Grid item>
                  <Grid container spacing={1}>
                    {props.categories &&
                      props.categories.map(
                        (cate, i) =>
                          i < 3 && (
                            <Grid item key={cate}>
                              <div className={classes.categories}>
                                <Typography variant="body2" color="textPrimary">
                                  {cate}
                                </Typography>
                              </div>
                            </Grid>
                          )
                      )}
                  </Grid>
                </Grid>
                <Grid item>
                  <Typography
                    variant="body2"
                    color="primary"
                    className={classes.loca}>
                    {props.location.length > 28
                      ? props.location.slice(0, 28) + "..."
                      : props.location}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </a>
      </Link>
      {dropdown}
    </Grid>
  );
}
const DELETE_EVENT = gql`
  mutation ($event_id: String!) {
    delete_event(event_id: $event_id)
  }
`;
