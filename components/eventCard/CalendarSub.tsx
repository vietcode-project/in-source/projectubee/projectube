import React, { ReactElement } from "react";
import Link from "next/link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

interface Props {
  id: string;
  name: string;
  categories: string[];
  coverUrl: string;
  start_time: Date;
}

const useStyle = makeStyles((theme) => ({
  root: {
    background: "#FFFFFF",
    "&:hover": {
      background: "#F8F8F8",
    },
    borderRadius: "8px",
    width: 360,
    padding: "8px ",
    margin: 0,
    marginTop: 16,
  },

  categories: {
    padding: theme.spacing(1),
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
  bold: {
    fontWeight: 800,
    fontSize: "16px",
  },
  light: {
    fontWeight: 300,
  },
  thin: {
    fontWeight: 100,
    color: "white",
  },
  loca: {
    backgroundColor: "#EDE9FB",
    borderRadius: "5px",
    padding: "6px 11px",
  },
}));

export function CalendarSubeventCard(props: Props): ReactElement {
  const classes = useStyle();

  return (
    <Link href={`/events/${props.id}`}>
      <a>
        <Grid
          container
          spacing={2}
          justify="space-between"
          alignItems="center"
          className={classes.root}>
          <Grid item>
            <div
              style={{
                width: "117px",
                height: "97px",
                borderRadius: "12px",
                backgroundSize: "cover",
                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundImage: `url(${props.coverUrl || "/temp-cover.jpg"})`,
              }}></div>
          </Grid>
          <Grid item style={{ flex: 1 }}>
            <Grid container>
              <Grid item>
                <Typography variant="body2" color="textSecondary">
                  {new Date(props.start_time).toLocaleDateString("default", {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                  })}
                </Typography>

                <Typography variant="h2" style={{ fontSize: "19px" }}>
                  {props.name}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              container
              spacing={1}
              justify="space-between"
              alignItems="center">
              <Grid item>
                <Grid container spacing={1}>
                  {props.categories &&
                    props.categories.map(
                      (cate, i) =>
                        i < 6 && (
                          <Grid item key={cate}>
                            <div className={classes.categories}>
                              <Typography variant="body2" color="textPrimary">
                                {cate}
                              </Typography>
                            </div>
                          </Grid>
                        )
                    )}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </a>
    </Link>
  );
}
