import React, { ReactElement } from "react";
import Link from "next/link";
import Image from "next/image";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import Event from "../../types/Event";
import { Avatar, Button } from "@material-ui/core";

interface Props extends Event {}

const useStyle = makeStyles((theme) => ({
  root: {
    marginTop: "24px",
  },
  img: {
    borderRadius: "10px",
  },
  userAva: {
    width: 40,
    height: 40,
    border: `3px solid ${theme.palette.background.paper}`,
    borderRadius: "20px",
    backgroundPosition: "center",
    backgroundSize: "contain",
  },
  categories: {
    padding: "6px 10px",
    border: "1px solid #DEE4ED",
    borderRadius: 4,
    marginRight: 8,
  },
  bold: {
    fontWeight: 800,
    fontSize: "16px",
  },
  light: {
    fontWeight: 300,
  },
  thin: {
    fontWeight: 100,
    color: "white",
  },
  loca: {
    backgroundColor: "#EDE9FB",
    borderRadius: "5px",
    padding: "6px 11px",
  },
}));

export function SideEventCard(props: Props): ReactElement {
  const classes = useStyle();

  return (
    <Grid>
      <Link href={`/events/${props.id}`}>
        <a>
          <Grid
            container
            spacing={3}
            justify="space-between"
            className={classes.root}
          >
            <Grid item xs={4}>
              <div
                className={classes.img}
                style={{
                  width: "100%",
                  paddingTop: "67%",
                  backgroundSize: "cover",
                  backgroundPosition: "center",
                  backgroundRepeat: "no-repeat",
                  backgroundImage: `url(${
                    props.coverUrl ? props.coverUrl : "/temp-cover.jpg"
                  })`,
                }}
              />
            </Grid>
            <Grid item xs={8}>
              <Grid container justify="space-between">
                <Grid item xs={12}>
                  <Typography variant="body2" color="textSecondary">
                    {new Date(props.start_time).toLocaleDateString("default", {
                      weekday: "long",
                      year: "numeric",
                      month: "long",
                      day: "numeric",
                    })}
                  </Typography>
                  <Typography variant="h2">
                    {props.name.slice(0, 50)}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container style={{ margin: "8px 0" }}>
                {props.categories &&
                  props.categories.map((cate) => (
                    <Grid item key={cate}>
                      <div className={classes.categories}>
                        <Typography variant="body2" color="textPrimary">
                          {cate}
                        </Typography>
                      </div>
                    </Grid>
                  ))}
              </Grid>
              <Grid
                container
                spacing={2}
                alignItems="center"
                justify="space-between"
              >
                <Grid item>
                  <Grid container alignItems="center">
                    {(props.subscribers &&
                      props.subscribers.length &&
                      props.subscribers.slice(0, 6).map((u, i) => (
                        <Grid
                          key={i}
                          style={{
                            marginRight: `${i != 5 ? "-12px" : "0"}`,
                          }}
                        >
                          <Avatar
                            className={classes.userAva}
                            src={u.avatarUrl}
                          />
                        </Grid>
                      ))) || (
                      <i>
                        <Typography variant="body2" gutterBottom>
                          Chưa có lượt quan tâm
                        </Typography>
                      </i>
                    )}
                    <Grid
                      style={{
                        paddingLeft: "3px",
                        zIndex: -10,
                      }}
                    >
                      <Typography color="primary">
                        {props.subscribers.length > 6 &&
                          props.subscribers.length - 6}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Typography
                    variant="body2"
                    color="primary"
                    className={classes.loca}
                  >
                    {props.location}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </a>
      </Link>
    </Grid>
  );
}
