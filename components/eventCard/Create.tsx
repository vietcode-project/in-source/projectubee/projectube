import React, { ChangeEvent, useEffect, useMemo, useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";

import Button from "@material-ui/core/Button";
import {
  TextField,
  useTheme,
  Select,
  MenuItem,
  makeStyles,
  useMediaQuery,
  Theme,
  IconButton,
  CircularProgress,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { useRouter } from "next/router";
import { gql, useMutation } from "@apollo/client";
import CateList from "../Catelist";
import cates from "config/categories.json";
import cities from "config/cities.json";
import PolicyCheckbox from "components/PolicyCheckbox";
import { Alert } from "@material-ui/lab";

interface Props {
  open: boolean;
  handleClose: () => void;
}

interface IInput {
  cover: File;
  name: string;
  categories: string[];
  description: string;
  start_time: Date | string;
  location: string;
  register_link: string;
}

const useStyles = makeStyles(() => ({
  menuPaper: {
    maxHeight: 200,
  },
}));

export function CreateEventDialogs({ handleClose, open }: Props) {
  const theme = useTheme();
  const router = useRouter();
  const classes = useStyles();
  const [checked, setChecker] = useState(false);
  const [createEvent, { loading: loadingCreateEvent, data }] = useMutation(
    CREATE_EVENT_MUTATION
  );
  const [locked, setLocked] = useState(false);
  const [coverPreview, setCoverPreview] = useState("");
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );
  const [isHeavyFile, setIsHeavyFile] = useState<boolean>(false);

  const [input, setInput] = useState<IInput>({
    cover: null,
    name: "",
    categories: [],
    description: "",
    start_time: new Date().toISOString().slice(0, 10),
    location: "",
    register_link: "",
  });

  function onSelectCover(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    if (e?.target?.files[0]?.size / 1024 / 1024 > 2) {
      setIsHeavyFile(true);
      setTimeout(() => {
        setIsHeavyFile(false);
      }, 5000);
    } else {
      setInput((prev) => ({ ...prev, cover: e.target.files[0] }));
      const objectUrl = URL.createObjectURL(e.target.files[0]);
      setCoverPreview(objectUrl);
    }
  }

  const handleInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    e.persist();
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };
  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!locked) {
      setLocked(() => true);
      createEvent({
        variables: {
          name: input.name,
          description: input.description,
          categories: input.categories,
          register_link: input.register_link,
          start_time:
            typeof input.start_time == "string"
              ? new Date(input.start_time)
              : input.start_time,
          cover: input.cover,
          location: input.location,
        },
      })
        .then(({ data, errors }) => {
          setLocked(() => false);
          if (errors) console.error("Create error: " + errors);
          if (data) {
            router.reload();
          }
        })
        .catch((err) => console.error(err));
    }
  };

  return (
    <Dialog
      maxWidth="md"
      onClose={handleClose}
      fullScreen={isDevicePhone}
      open={open}
      PaperProps={{
        style: {
          padding: theme.spacing(7, 6),
          width: isDevicePhone ? "100%" : "60%",
          minWidth: "400px",
        },
      }}
    >
      <form onSubmit={handleSubmit}>
        <Grid
          container
          alignItems="center"
          style={{ marginBottom: theme.spacing(3) }}
        >
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography
            style={{ fontSize: isDevicePhone ? "1.5em" : "24px" }}
            display="inline"
          >
            Tạo sự kiện tổ chức {name}
          </Typography>
        </Grid>
        <Typography variant="h2">Ảnh bìa sự kiện</Typography>
        <Typography color="textSecondary" gutterBottom>
          Ảnh bìa sự kiện sẽ giúp nhiều người chú ý hơn (Vui lòng chọn ảnh dưới
          2MB)
        </Typography>

        <Grid
          container
          justify="center"
          style={{
            paddingTop: "28.125%",
            paddingBottom: "28.125%",
            border: `${
              coverPreview == undefined
                ? "1px solid transparent"
                : "1px solid #DEE4ED"
            }`,
            borderRadius: "8px",
            backgroundImage: `${
              coverPreview != undefined ? `url(${coverPreview})` : ""
            }`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
        >
          <Grid item>
            {!input.cover && (
              <Typography variant="h3">Kéo thả ảnh vào đây</Typography>
            )}
          </Grid>
        </Grid>
        <br />
        <Button variant="outlined" component="label">
          <Typography variant="button">Chọn ảnh từ thiết bị của bạn</Typography>
          <input type="file" accept="image/*" onChange={onSelectCover} hidden />
        </Button>

        <Divider style={{ margin: theme.spacing(4, 0) }} />

        <Typography variant="h2" style={{ marginBottom: theme.spacing(4) }}>
          Thông tin sự kiện
        </Typography>
        <Grid
          container
          style={{ marginBottom: theme.spacing(4) }}
          justify="space-between"
        >
          <Grid
            item
            xs={isDevicePhone ? 12 : 5}
            style={{
              width: isDevicePhone && "100%",
              marginBottom: isDevicePhone && "8px",
            }}
          >
            <Typography style={{ marginBottom: theme.spacing(2) }}>
              Tên sự kiện
            </Typography>
            <TextField
              style={{
                marginBottom: isDevicePhone && "8px",
              }}
              onChange={handleInputChange}
              value={input.name}
              name="name"
              placeholder="Nhập tên sự kiện của tổ chức"
              fullWidth
            />
          </Grid>
          <Grid
            item
            xs={isDevicePhone ? 12 : 5}
            style={{
              width: isDevicePhone && "100%",
              marginBottom: isDevicePhone && "8px",
            }}
          >
            <Typography style={{ marginBottom: theme.spacing(2) }}>
              Link tham gia sự kiện
            </Typography>
            <TextField
              onChange={handleInputChange}
              value={input.register_link}
              name="register_link"
              placeholder="Nhập đường link đăng kí của sự kiện"
              fullWidth
            />
          </Grid>
        </Grid>

        <div style={{ marginBottom: theme.spacing(4) }}>
          <Typography gutterBottom>Chủ đề của sự kiện</Typography>
          {(input.categories.length > 3 && (
            <Typography
              variant="body2"
              color="error"
              style={{ marginBottom: theme.spacing(2) }}
            >
              Bạn đã chọn quá giới hạn, vui lòng chọn lại
            </Typography>
          )) || (
            <Typography
              variant="body2"
              color="secondary"
              style={{ marginBottom: theme.spacing(2) }}
            >
              Vui lòng chọn tối đa 3 chủ đề
            </Typography>
          )}
          <CateList
            cates={cates}
            selected={input.categories}
            handleChoose={(label) => {
              if (input.categories.includes(label)) {
                setInput((prev) => ({
                  ...prev,
                  categories: prev.categories.filter((c) => c !== label),
                }));
              } else {
                setInput((prev) => ({
                  ...prev,
                  categories: [...prev.categories, label],
                }));
              }
            }}
          />
        </div>

        <div style={{ marginBottom: theme.spacing(4) }}>
          <Grid
            container
            justify="space-between"
            alignItems="center"
            style={{ marginBottom: theme.spacing(2) }}
          >
            <Grid item>
              <Typography>Giới thiệu sự kiện</Typography>
            </Grid>
            <Grid item>
              <Typography variant="body2" color="textSecondary">
                {5000 - input.description.length} ký tự còn lại
              </Typography>
            </Grid>
          </Grid>

          <TextField
            onChange={handleInputChange}
            value={input.description}
            inputProps={{ maxLength: 5000 }}
            name="description"
            variant="outlined"
            label="Nhập văn bản"
            fullWidth
            rows={6}
            multiline
          />
        </div>

        <div style={{ marginBottom: theme.spacing(3) }}>
          <Grid
            container
            spacing={2}
            direction={isDevicePhone ? "column" : "row"}
          >
            <Grid item xs={isDevicePhone ? 12 : 6}>
              <Typography>Ngày tổ chức</Typography>
              <TextField
                onChange={handleInputChange}
                value={input.start_time}
                name="start_time"
                type="date"
                variant="outlined"
                fullWidth
              />
            </Grid>
            <Grid item xs={isDevicePhone ? 12 : 6}>
              <Typography>Địa điểm tổ chức</Typography>
              <Select
                fullWidth
                MenuProps={{
                  getContentAnchorEl: null,
                  anchorOrigin: {
                    vertical: "bottom",
                    horizontal: "left",
                  },
                  classes: { paper: classes.menuPaper },
                }}
                value={input.location}
                onChange={handleInputChange}
                variant="outlined"
                name="location"
              >
                {cities.map((city, index) => (
                  <MenuItem key={index} value={city}>
                    {city}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
          </Grid>
        </div>

        <Divider style={{ marginBottom: theme.spacing(3) }} />

        <Grid container alignItems="center" style={{ marginBottom: "80px" }}>
          <Grid
            item
            xs={isDevicePhone ? 12 : 5}
            style={{ marginBottom: isDevicePhone && "8px" }}
          >
            <PolicyCheckbox
              checked={checked}
              checkboxClick={() => setChecker(!checked)}
            />
          </Grid>
          <Grid item xs={isDevicePhone ? 12 : 7}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              fullWidth
              disabled={
                !(
                  input.name != "" &&
                  input.location != "" &&
                  input.categories.length <= 3 &&
                  input.categories.length >= 1 &&
                  input.description != "" &&
                  input.start_time &&
                  !locked &&
                  checked &&
                  !loadingCreateEvent &&
                  !data
                )
              }
            >
              {!loadingCreateEvent && !data ? (
                "Tạo sự kiện mới"
              ) : (
                <CircularProgress color="inherit" size={25} />
              )}
            </Button>
          </Grid>
        </Grid>
      </form>
      {isHeavyFile && (
        <div style={{ position: "fixed", top: 30, left: 30 }}>
          <Alert severity="error">
            Dung lượng ảnh được chọn đã vượt quá 2MB
          </Alert>
        </div>
      )}
    </Dialog>
  );
}

const CREATE_EVENT_MUTATION = gql`
  mutation (
    $name: String!
    $description: String!
    $start_time: DateTime
    $location: String
    $cover: Upload
    $register_link: String
    $categories: [String!]
  ) {
    create_event(
      data: {
        name: $name
        description: $description
        start_time: $start_time
        location: $location
        cover: $cover
        register_link: $register_link
        categories: $categories
      }
    ) {
      id
      register_link
    }
  }
`;
