import { gql, useMutation } from "@apollo/client";
import {
  Avatar,
  Button,
  Grid,
  IconButton,
  makeStyles,
  Menu,
  MenuItem,
  Typography,
} from "@material-ui/core";
import IEvent from "types/Event";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { ConfirmUpdate } from "../Confirmation";
import { CreateEventDialogs, ResponsiveSideEventCard } from "../eventCard";

import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";

interface IOrgProps {
  org: {
    id: string;
    name: string;
    description: string;
    categories: string[];
    avatarUrl: string;
    events: IEvent[];
  };
}

export function ProfileOrgCard(props: IOrgProps) {
  const classes = useStyles();
  const [viewalldes, setViewAllDes] = useState<boolean>(false);
  const [viewallevent, setViewAllEvent] = useState<boolean>(false);
  const [open, setOpen] = useState(false);
  const [open2, setOpen2] = useState(false);
  const router = useRouter();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const [deleteorg, { loading: loadingDeleteOrg }] = useMutation(DELETE_ORG);
  const edit = () => {
    router.push(`/orgs/${props.org.id}/edit`);
  };

  const handledelete = async () => {
    const { data, errors } = await deleteorg({
      variables: { org_id: props.org.id },
    });

    if (errors) {
    }
    if (data) {
      router.reload();
    }
  };

  const dropdown = (
    <Menu
      id="simple-menu"
      anchorEl={anchorEl}
      getContentAnchorEl={null}
      open={Boolean(anchorEl)}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      onClose={handleClose}
    >
      <MenuItem style={{ width: "210px" }} onClick={edit}>
        <Typography variant="h5">Chỉnh sửa</Typography>
      </MenuItem>
      <MenuItem onClick={() => setOpen2(true)}>
        <Typography variant="h5" color="error">
          Xóa tổ chức
        </Typography>
      </MenuItem>
    </Menu>
  );

  return (
    <>
      <Grid container className={classes.orgcard}>
        <Grid
          container
          style={{
            borderBottom: "1px solid #DEE4ED",
            paddingBottom: "16px",
            marginBottom: "16px",
          }}
        >
          <Typography color="primary">Cập nhật 3 tiếng trước</Typography>
        </Grid>

        <Grid container spacing={4}>
          <Grid item xs={1}>
            <Avatar src={props.org.avatarUrl}></Avatar>
          </Grid>
          <Grid item xs={11}>
            <Grid container justify="space-between">
              <Grid item>
                <Link href={`/orgs/${props.org.id}`}>
                  <a>
                    <Typography variant="h2">{props.org.name}</Typography>
                  </a>
                </Link>
                <Typography variant="body1" color="secondary">
                  to chuc kul ngau
                </Typography>
                <Grid container spacing={1}>
                  {props.org.categories.map((cate) => (
                    <Grid item key={cate}>
                      <div className={classes.categories}>
                        <Typography variant="body2" color="textPrimary">
                          {cate}
                        </Typography>
                      </div>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
              <Grid item>
                <IconButton onClick={handleClick} aria-haspopup="true">
                  <MoreHorizIcon />
                </IconButton>
                <ConfirmUpdate
                  Title="Bạn muốn xóa tổ chức?"
                  context={`Hành động này sẽ xóa vĩnh viễn các thông tin của "${props.org.name}"`}
                  button1ctn="Quay lại"
                  button2ctn="Lưu thay đổi"
                  open={open2}
                  handleBack={() => setOpen2(false)}
                  handleClose={() => setOpen2(false)}
                  handleConfirm={() => handledelete()}
                  loading={loadingDeleteOrg}
                  stylebtn1={{ color: "#532BDC" }}
                  stylebtn2={{ background: "#E4593B", color: "white" }}
                />
              </Grid>
            </Grid>
            <Grid
              container
              justify="space-between"
              alignItems="center"
              style={{ marginTop: "16px" }}
            >
              <Grid>
                <Typography variant="body1">
                  <b>Thông tin tổ chức</b>
                </Typography>
              </Grid>
              <Grid>
                <Button
                  onClick={() => setViewAllDes(!viewalldes)}
                  variant="text"
                  style={{ color: `${viewalldes ? "#1B2431" : "#8D9198"}` }}
                  endIcon={
                    viewalldes ? (
                      <KeyboardArrowDownIcon />
                    ) : (
                      <KeyboardArrowUpIcon />
                    )
                  }
                >
                  Xem tất cả
                </Button>
              </Grid>
            </Grid>
            <Typography variant="body1" color="secondary">
              {viewalldes
                ? props.org.description.slice(0, 100)
                : props.org.description.slice(0, 1000)}
            </Typography>
            <Grid container justify="space-between" alignItems="center">
              <Grid item style={{ marginTop: "16px" }}>
                <Typography variant="body1">
                  <b>Các sự kiện đã tổ chức</b>
                </Typography>
              </Grid>
              <Grid item>
                <Button
                  onClick={() => setViewAllEvent(!viewallevent)}
                  variant="text"
                  style={{ color: `${viewallevent ? "#1B2431" : "#8D9198"}` }}
                  endIcon={
                    viewallevent ? (
                      <KeyboardArrowDownIcon />
                    ) : (
                      <KeyboardArrowUpIcon />
                    )
                  }
                >
                  Xem tất cả
                </Button>
              </Grid>
            </Grid>
            <Grid container spacing={2} style={{ marginTop: "8px" }}>
              {props.org.events && viewallevent
                ? props.org.events.map((event) => (
                    <Grid item xs={12} key={event.id}>
                      <ResponsiveSideEventCard {...event} />
                    </Grid>
                  ))
                : ""}
            </Grid>
            <Grid>
              <Button
                onClick={() => setOpen(true)}
                fullWidth
                variant="outlined"
                color="primary"
                size="large"
                startIcon="+"
                style={{ marginTop: "16px" }}
              >
                <Typography variant="body1">Thêm sự kiện</Typography>
              </Button>
              <CreateEventDialogs
                open={open}
                handleClose={() => setOpen(false)}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {dropdown}
    </>
  );
}

const DELETE_ORG = gql`
  mutation ($org_id: String!) {
    delete_org(org_id: $org_id)
  }
`;

const useStyles = makeStyles((theme) => ({
  orgcard: {
    borderRadius: 8,
    border: "1px solid #DEE4ED",
    padding: theme.spacing(3, 2.25),
    marginTop: theme.spacing(4),
  },

  categories: {
    padding: theme.spacing(1, 1.25),
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
}));
