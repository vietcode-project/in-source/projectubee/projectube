import React, { ReactElement } from "react";
import Image from "next/image";
import Link from "next/link";
import { makeStyles, Typography, Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1.5),
    border: "1px solid #DEE4ED",
    borderRadius: 16,
    height: 179,
    width: "inherits",
  },
  avatar: {
    borderRadius: "100%",
  },
  cate: {
    padding: theme.spacing(0.5, 1, 0.5, 1),
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
}));

interface Props {
  id: string;
  avatarUrl?: string;
  name: string;
  categories?: string[];
}

export function LargeOrgCard({
  id,
  avatarUrl,
  name,
  categories,
}: Props): ReactElement {
  const classes = useStyles();

  return (
    <Link href={`/orgs/${id}`}>
      <a>
        <div className={classes.root}>
          <div
            style={{
              margin: "0 auto",
              width: "fit-content",
              marginBottom: "8px",
            }}
          >
            <Image
              width={60}
              height={60}
              src={
                avatarUrl || "https://pbs.twimg.com/media/DSTYzfUV4AAs_uP.jpg"
              }
              className={classes.avatar}
            />
          </div>

          <Typography variant="h3" gutterBottom align="center">
            {name && name.length > 15 ? name.slice(0, 15) + "..." : name}
          </Typography>

          <Typography
            variant="body2"
            color="textSecondary"
            align="center"
            gutterBottom
          >
            Tổ chức phi lợi nhuận
          </Typography>
          <Grid container spacing={1} justify="center" alignItems="center">
            <Grid item>
              <Grid container spacing={1}>
                {categories && categories.length ? (
                  categories.slice(0, 2).map((cate) => (
                    <Grid item key={cate}>
                      <div className={classes.cate}>
                        <Typography
                          variant="body2"
                          color="textPrimary"
                          style={{ fontSize: "1em" }}
                        >
                          {cate}
                        </Typography>
                      </div>
                    </Grid>
                  ))
                ) : (
                  <Typography variant="body2" align="center">
                    Chưa có
                  </Typography>
                )}
              </Grid>
            </Grid>
          </Grid>
        </div>
      </a>
    </Link>
  );
}
