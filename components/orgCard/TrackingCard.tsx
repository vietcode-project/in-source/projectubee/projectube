import React, { ReactElement, useState } from "react";
import {
  makeStyles,
  Typography,
  Grid,
  Avatar,
  IconButton,
  MenuItem,
  Menu,
} from "@material-ui/core";
import Link from "next/link";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import { useRouter } from "next/router";
import { ConfirmUpdate } from "../Confirmation";
import { gql, useMutation } from "@apollo/client";

const useStyles = makeStyles(() => ({
  root: {
    padding: 16,
    border: "1px solid #DEE4ED",
    borderRadius: 8,
    width: "100%",
    marginBottom: 48,
    position: "relative",
  },
  avatar: {
    borderRadius: "100%",
  },
  cate: {
    marginTop: 10,
    marginRight: 3,
    marginLeft: 3,
    padding: "8px 10px",
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
}));

export function OrgTrackingCard(props): ReactElement {
  const classes = useStyles();
  const [open2, setOpen2] = useState(false);

  const router = useRouter();
  const [deleteorg, { loading: loadingDelete }] = useMutation(DELETE_ORG);

  const edit = () => {
    router.push(`/orgs/${props.org.id}/edit`);
  };
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handledelete = async () => {
    const { data, errors } = await deleteorg({
      variables: { org_id: props.org.id },
    });

    if (errors) {
    }
    if (data) {
      router.reload();
    }
  };

  const dropdown = (
    <Menu
      id="simple-menu"
      anchorEl={anchorEl}
      getContentAnchorEl={null}
      open={Boolean(anchorEl)}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      onClose={handleClose}
    >
      <MenuItem style={{ width: "210px" }} onClick={edit}>
        <Typography variant="h5">Chỉnh sửa</Typography>
      </MenuItem>
      <MenuItem onClick={() => setOpen2(true)}>
        <Typography variant="h5" color="error">
          Xóa tổ chức
        </Typography>
      </MenuItem>
    </Menu>
  );

  return (
    <div className={classes.root}>
      <Grid container justify="space-between" style={{ position: "relative" }}>
        <Grid style={{ position: "absolute", top: 0, right: 0 }}>
          {router.pathname != "/" && (
            <IconButton onClick={handleClick} aria-haspopup="true">
              <MoreHorizIcon />
            </IconButton>
          )}
          <ConfirmUpdate
            Title="Bạn muốn xóa tổ chức?"
            context={`Hành động này sẽ xóa vĩnh viễn các thông tin của "${props.org.name}"`}
            button1ctn="Quay lại"
            button2ctn="Lưu thay đổi"
            open={open2}
            handleBack={() => setOpen2(false)}
            handleClose={() => setOpen2(false)}
            handleConfirm={() => handledelete()}
            loading={loadingDelete}
            stylebtn1={{ color: "#532BDC" }}
            stylebtn2={{ background: "#E4593B", color: "white" }}
          />
        </Grid>
        <Grid container justify="center">
          <Link href={"/orgs/" + props.org.id}>
            <a>
              <Grid container style={{ marginTop: 16 }}>
                <Avatar
                  style={{ margin: "auto", width: 145, height: 145 }}
                  src={
                    props.org.avatarUrl ||
                    "https://pbs.twimg.com/media/DSTYzfUV4AAs_uP.jpg"
                  }
                />
              </Grid>
              <Grid container justify="center">
                <Typography variant="h2">
                  <b>{props.org.name}</b>
                </Typography>
              </Grid>
              <Grid container justify="center">
                <Typography variant="body1" color="textSecondary">
                  Tổ chức phi lợi nhuận
                </Typography>
              </Grid>
              {props.org.categories.length == 0 && (
                <Link href={""} scroll={false}>
                  <a>
                    <Typography
                      style={{ marginTop: 8 }}
                      color="textSecondary"
                      variant="h5"
                      onClick={handleClick}
                    >
                      Hãy thêm phân loại cho tổ chức của bạn
                    </Typography>
                  </a>
                </Link>
              )}
              {props.org.categories && props.org.categories.length > 0 && (
                <Grid container justify="center">
                  {props.org.categories.map((cate) => (
                    <Grid item key={cate}>
                      <div className={classes.cate}>
                        <Typography
                          variant="body1"
                          style={{
                            fontSize: "12px",
                          }}
                        >
                          {cate}
                        </Typography>
                      </div>
                    </Grid>
                  ))}
                </Grid>
              )}
            </a>
          </Link>
        </Grid>
        <Grid container justify="center">
          <Grid
            item
            xs={4}
            style={{
              height: 16,
              borderBottom: "1px solid #DEE4ED",
              marginBottom: 16,
            }}
          />
        </Grid>
        <Grid container direction="column">
          <Typography variant="body1" color="textSecondary" gutterBottom>
            Thống kê
          </Typography>
          <Typography variant="body2" color="textSecondary" gutterBottom>
            Feature coming soon
          </Typography>
        </Grid>
        <Grid container direction="column" style={{ marginTop: 16 }}>
          <Typography variant="body1" color="textSecondary" gutterBottom>
            Lượt theo dõi
          </Typography>
          <Typography variant="body1">
            {(props.org && props.org.subscribers.length) || 0} lượt theo dõi
          </Typography>
        </Grid>
        <Grid container direction="column" style={{ marginTop: 16 }}>
          <Typography variant="body1" color="textSecondary" gutterBottom>
            Tổng số event
          </Typography>
          <Typography variant="body1">
            {(props.org && props.org.events.length) || 0} sự kiện
          </Typography>
        </Grid>
      </Grid>
      {dropdown}
    </div>
  );
}
const DELETE_ORG = gql`
  mutation {
    delete_org
  }
`;
