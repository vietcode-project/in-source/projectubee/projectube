import { gql, useMutation } from "@apollo/client";
import {
  Avatar,
  Button,
  Divider,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";

const useStyles = makeStyles((theme) => ({
  cardDetail: {
    borderRadius: 8,
    border: "1px solid #DEE4ED",
    padding: theme.spacing(4, 3),
    marginBottom: 16,
  },
  categories: {
    padding: theme.spacing(1, 1.25),
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
  unsubbtn: {
    borderRadius: 6,
    padding: "8px 24px",
    background: "#EDE9FB",
    color: "#532BDC",
    "&:hover": {
      background: "#EDE9FB",
      color: "#532BDC",
    },
  },
}));

export const SideSmallOrgCard = ({ data, user }) => {
  const classes = useStyles();
  const router = useRouter();
  const [subbed, setSubbed] = useState(false);

  const [subscribe] = useMutation(ORG_SUB, {
    variables: {
      org_id: data?.id,
    },
  });

  const [unsubscribe] = useMutation(ORG_UNSUB, {
    variables: {
      org_id: data?.id,
    },
  });

  React.useEffect(() => {
    if (!user) return setSubbed(() => false);
    if (!data?.id) return setSubbed(() => false);
    if (
      user &&
      user.type == "User" &&
      user?.subscribing_orgs?.some((o) => o.id === data?.id)
    )
      return setSubbed(() => true);
  }, [user, data?.id]);

  const handlesub = async (e: any) => {
    e.preventDefault();

    if (!user) {
      router.push("/signin");
      return;
    }

    if (!subbed) {
      try {
        await subscribe().then(() => setSubbed(true));
      } catch (err) {}
    }
  };

  const handleunsub = async (e: any) => {
    e.preventDefault();
    if (subbed) {
      try {
        await unsubscribe().then(() => setSubbed(false));
      } catch (err) {}
    }
  };

  return (
    <div className={classes.cardDetail}>
      <Grid container spacing={1} justify="center">
        <Grid item>
          <Link href={"/orgs/" + data?.id}>
            <a>
              <Avatar
                src={data && data.avatarUrl}
                style={{
                  height: "16vh",
                  width: "16vh",
                  marginBottom: "24px",
                }}
              ></Avatar>
            </a>
          </Link>
        </Grid>
        <Grid item xs={12}>
          <Link href={"/orgs/" + data?.id}>
            <a>
              <Typography align="center" variant="h2">
                {data && data.name}
              </Typography>
            </a>
          </Link>
        </Grid>
        <Grid item xs={12}>
          <Typography align="center" variant="body1" color="textSecondary">
            {(data && data.description.slice(0, 100) + "...") ||
              "Chua co mo ta"}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={1} justify="center">
            {data &&
              data.categories.map((cate, i) => (
                <Grid item key={i}>
                  <div className={classes.categories}>
                    <Typography variant="body2" color="textPrimary">
                      {cate}
                    </Typography>
                  </div>
                </Grid>
              ))}
          </Grid>
        </Grid>
      </Grid>
      <Divider style={{ margin: "16px auto", width: "120px" }} />
      <Grid container spacing={2}>
        <Grid item xs={6}>
          {subbed ? (
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.unsubbtn}
              onClick={handleunsub}
            >
              <Typography variant="body1">Đã theo dõi</Typography>
            </Button>
          ) : (
            <Button
              fullWidth
              variant="contained"
              color="primary"
              style={{ borderRadius: 6, padding: "8px 24px" }}
              disabled={user?.type == "Org"}
              onClick={
                user && user?.type == "User"
                  ? handlesub
                  : () => {
                      if (!user) {
                        router.push("/signin");
                      }
                    }
              }
            >
              <Typography variant="body1">Theo dõi</Typography>
            </Button>
          )}
        </Grid>
        <Grid item xs={6}>
          <Button
            fullWidth
            href={
              (data && data.contact.facebook_url) ||
              "http://fb.com/vietcode.org"
            }
            style={{ borderRadius: 6, padding: "6.5px 24px" }}
            target="_blank"
            variant="outlined"
            color="primary"
          >
            <Typography variant="body1">Liên hệ</Typography>
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

const ORG_SUB = gql`
  mutation ($org_id: String!) {
    subscribe_org(org_id: $org_id)
  }
`;
const ORG_UNSUB = gql`
  mutation ($org_id: String!) {
    unsubscribe_org(org_id: $org_id)
  }
`;
