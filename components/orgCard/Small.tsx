import {
  Avatar,
  Button,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

const useStyles = makeStyles(() => ({
  root: {
    padding: 16,
    border: "1px solid #DEE4ED",
    borderRadius: 8,
    width: "100%",
    marginBottom: 16,
  },
  avatar: {
    borderRadius: "100%",
  },
  cate: {
    marginRight: 6,
    marginTop: 10,
    padding: "6px 10px",
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
}));

export function SmallOrgCard(props) {
  const classes = useStyles();

  return (
    <Grid className={classes.root}>
      <Link href={"/orgs/" + props?.org?.id}>
        <a>
          <Grid container justify="space-between">
            <Grid item xs={2}>
              <Avatar
                style={{ margin: "auto", width: 47, height: 47 }}
                src={props?.org?.avatarUrl || "/temp-ava.jpg"}
              />
            </Grid>
            <Grid item xs={10} style={{ paddingLeft: 16, cursor: "pointer" }}>
              <Typography variant="h2">
                <b>{props?.org?.name}</b>
              </Typography>
              <Typography variant="body2" color="textSecondary">
                Follow for more info
              </Typography>
              {props?.org?.categories && props?.org?.categories?.length > 0 && (
                <Grid container>
                  {props?.org?.categories?.slice(0, 2).map((cate) => (
                    <Grid item key={cate}>
                      <div className={classes.cate}>
                        <Typography
                          variant="body1"
                          gutterBottom
                          style={{ fontSize: "9px" }}
                        >
                          {cate}
                        </Typography>
                      </div>
                    </Grid>
                  ))}
                  {props?.org?.categories?.length < 0 && (
                    <Typography
                      variant="body1"
                      gutterBottom
                      style={{ fontSize: "9px" }}
                    >
                      "Chưa có phân loại"
                    </Typography>
                  )}
                </Grid>
              )}
            </Grid>
          </Grid>
        </a>
      </Link>
    </Grid>
  );
}
