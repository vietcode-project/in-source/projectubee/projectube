import { gql, useMutation } from "@apollo/client";
import { Avatar, Button, Grid, Typography, makeStyles } from "@material-ui/core";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

interface IOrgProps {
  org: {
    id: string;
    name: string;
    description: string;
    categories: string[];
    avatarUrl: string;
  };
}

const useStyles = makeStyles(()=>({
  unfollowBtn: {
    border: "1px solid #FCECE9",
    '&:hover':{
      backgroundColor:"white",
      border: "1px solid #E4593B",
    }
  }
}))

export function SubbedOrgCard(props: IOrgProps) {
  const router = useRouter();
  const classes= useStyles()
  const [unsubcribe] = useMutation(ORG_UNSUB, {
    variables: {
      org_id: props.org.id,
    },
  });
  const handleunsub = async (e: any) => {
    e.preventDefault();

    try {
      await unsubcribe().then(() => router.reload());
    } catch (err) {}
  };

  return (
    <Grid
      container
      style={{
        padding: "16px",
        marginTop: "30px",
        border: "1px solid #DEE4ED",
        borderRadius: 8,
      }}>
      <Grid item xs={1}>
        <Grid
          style={{ height: "100%" }}
          container
          justify="center"
          alignItems="flex-start">
          <Avatar src={props.org.avatarUrl} style={{ width: 47, height: 47 }} />
        </Grid>
      </Grid>
      <Grid item xs={11} style={{ paddingLeft: 16 }}>
        <Grid container spacing={1} direction="column">
          <Link href={`/orgs/${props.org.id}`}>
            <a>
              <Grid>
                <Typography variant="h3" style={{ marginTop: "5px" }}>
                  <b>{props.org.name}</b>
                </Typography>
              </Grid>
              <Grid>
                <Typography
                  variant="body2"
                  color="textSecondary"
                  style={{ marginTop: "4px" }}>
                  {props.org.description.length > 50
                    ? props.org.description.slice(0, 50) + "..."
                    : props.org.description}
                </Typography>
              </Grid>
            </a>
          </Link>
          <Grid
            container
            justify="space-between"
            alignItems="center"
            style={{ marginTop: 10 }}>
            <Grid item>
              <Grid container spacing={1}>
                {props.org.categories.map((cate) => (
                  <Grid item key={cate}>
                    <div
                      style={{
                        padding: "4px 6px",
                        border: "1px solid #DEE4ED",
                        borderRadius: 4,
                      }}>
                      <Typography variant="body2" color="textPrimary">
                        {cate}
                      </Typography>
                    </div>
                  </Grid>
                ))}
              </Grid>
            </Grid>
            <Grid item>
              <Button
                onClick={handleunsub}
                className={classes.unfollowBtn}
               >
                <Typography style={{ color: "#E4593B" }} variant="body1" >
                  Hủy theo dõi
                </Typography>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

const ORG_UNSUB = gql`
  mutation ($org_id: String!) {
    unsubscribe_org(org_id: $org_id)
  }
`;
