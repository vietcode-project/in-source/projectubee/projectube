import React, { useEffect, useState } from "react";

import { Grid, Typography } from "@material-ui/core";
import { SmallOrgCard } from ".";
import IOrg from "../../types/Org";
import { ResponsiveSideEventCard } from "../eventCard";
import { makeStyles } from "@material-ui/styles";
import Link from "next/link";

const useStyle = makeStyles(() => ({
  feature: {
    marginBottom: 32,
    marginTop: 32,
    "&:after": {
      content: '""',
      left: 0,
      bottom: 0,
      height: "32px",
      width: "calc(100% - 32px)",
      borderBottom: "1px solid #DEE4ED",
      margin: "auto",
    },
    "&:before": {
      content: '""',
      left: 0,
      bottom: 0,
      height: "32px",
      width: "calc(100% - 32px)",
      borderTop: "1px solid #DEE4ED",
      margin: "auto",
    },
  },
}));

interface Props {
  orgList: IOrg[];
}

export function OrgSuggestCard(props: Props) {
  const [randomOrg, setRandomOrg] = useState<IOrg>();
  const classes = useStyle();

  useEffect(() => {
    const neworgwithevent = props.orgList.filter(
      (org) => org.events.length > 0
    );
    setRandomOrg(
      neworgwithevent[Math.floor(Math.random() * neworgwithevent.length)]
    );
  }, []);

  return (
    <Grid item>
      <Grid container className={classes.feature}>
        <Grid item xs={12} md={6}>
          <Link href={`/orgs/${randomOrg && randomOrg.id}`}>
            <a>
              <Grid container>
                <Grid item xs={12}>
                  <Typography style={{ fontSize: 32, marginBottom: 16 }}>
                    <b>
                      Tuyển tập những sự kiện của {randomOrg && randomOrg.name}
                    </b>
                  </Typography>
                </Grid>
                <Grid item xs={8}>
                  {randomOrg && <SmallOrgCard org={randomOrg} />}
                </Grid>
              </Grid>
            </a>
          </Link>
        </Grid>
        <Grid item xs={12} md={6}>
          <Grid
            container
            direction="column"
            justify="space-around"
            alignItems="center"
          >
            {randomOrg &&
              randomOrg.events
                .slice(0, 3)
                .map((event) => (
                  <ResponsiveSideEventCard key={event.id} {...event} />
                ))}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
