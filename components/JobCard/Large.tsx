import { Grid, makeStyles, Typography } from "@material-ui/core";
import Link from "next/link";
import React from "react";

const useStyles = makeStyles(() => ({
  root: {
    width: "100%",
    borderRadius: "15px",
    height: 309,
    display: "flex",
    alignItems: "flex-end",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
  },
  typoOrg: {
    fontSize: "16px",
    fontWeight: 400,
    marginBottom: "4px",
  },
  typoTitle: {
    fontSize: "24px",
    fontWeight: 700,
    marginBottom: "8px",
    lineHeight: "30px",
  },
  typoLocation: {
    fontSize: "12px",
    padding: "2px 6px",
    backgroundColor: "#532BDC",
    borderRadius: "2px",
    marginRight: "14px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  typoDeadline: {
    fontSize: "16px",
    fontWeight: 400,
  },
}));

export const LargeJobCard = ({ data }) => {
  const classes = useStyles();

  return (
    <Link href={"/recruit/" + data?.id}>
      <a>
        <Grid item style={{ width: "inherits" }}>
          <Grid
            item
            className={classes.root}
            style={{
              backgroundImage: `linear-gradient(180deg, rgba(27, 36, 49, 0) 43.23%, rgba(27, 36, 49, 0.3) 67.19%, #1B2431 100%),url(${
                data?.cover_url ? data.cover_url : "/temp-cover.jpg"
              })`,
            }}
          >
            <Grid style={{ padding: "20px 29px", color: "white" }}>
              <Typography className={classes.typoOrg}>
                {data?.org?.name}
              </Typography>
              <Typography className={classes.typoTitle}>
                {data?.title?.length > 25
                  ? data?.title?.slice(0, 25) + "..."
                  : data?.title}
              </Typography>
              <Grid container direction="row" style={{ padding: 0 }}>
                <Typography className={classes.typoLocation}>
                  {data?.location}
                </Typography>
                <Typography className={classes.typoDeadline}>
                  Hạn cuối:{" "}
                  {new Date(data?.deadline).toLocaleDateString("en-GB")}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </a>
    </Link>
  );
};
