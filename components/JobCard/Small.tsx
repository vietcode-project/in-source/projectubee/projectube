import { Grid, makeStyles, Typography } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles(() => ({
  root: {
    padding: 12,
    borderRadius: "15px",
    border: "1px solid #DEE4ED",
    height: "350px",
    width: "inherits",
  },
  coverImg: {
    borderRadius: "12px",
    width: "100%",
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    height: "141px",
  },
  typoTitle: {
    fontSize: "19px",
    color: "#1B2431",
    fontWeight: 700,
    marginTop: "8px",
  },
  typoDescription: {
    fontSize: "16px",
    color: "#8D9198",
    fontWeight: 400,
    marginTop: "8px",
  },
  typoDeadline: {
    fontSize: "12px",
    color: "#8D9198",
    fontWeight: 400,
    marginTop: "8px",
  },
  typoState: {
    fontSize: "12px",
    color: "#19C739",
    backgroundColor: "#EEFBF1",
    fontWeight: 400,
    marginTop: "8px",
    textAlign: "center",
    paddingTop: "6px",
    paddingBottom: "6px",
    borderRadius: "4px",
  },
}));

export const SmallJobCard = () => {
  const classes = useStyles();
  return (
    <Grid item xs={3} className={classes.root} container direction="column">
      <div
        style={{
          backgroundImage: `url(${"/temp-cover.jpg"})`,
        }}
        className={classes.coverImg}
      />
      <Typography className={classes.typoTitle}>Job Listing</Typography>
      <Typography className={classes.typoDescription}>
        Job Description: Contrary to popular belief, Lorem Ipsum is not
        simply...
      </Typography>
      <Typography className={classes.typoDeadline}>
        HẠN CUỐI: 30/7/2021
      </Typography>
      <Typography className={classes.typoState}>Đang mở đơn</Typography>
    </Grid>
  );
};
