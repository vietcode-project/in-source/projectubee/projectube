import React from "react";
import {
  Grid,
  makeStyles,
  Typography,
  IconButton,
  Menu,
  MenuItem,
  useMediaQuery,
  Theme,
} from "@material-ui/core";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import { gql, useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import Link from "next/link";
import Loading from "components/Loading";
import { useAuthState } from "contexts/auth";
import dynamic from "next/dynamic";
import { Props } from "components/Confirmation";

const ConfirmUpdateDynamic = dynamic<Props>(() =>
  import("components/Confirmation").then((mod) => mod.ConfirmUpdate)
);
const UpdateRecruitDialogDynamic = dynamic(() => import("./Update"));

const useStyles = makeStyles(() => ({
  root: {
    position: "relative",
    width: "inherit",
    cursor: "pointer",
  },
  coverImg: {
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    borderRadius: "12px",
  },
  typoJobState: {
    fontSize: "11px",
    color: "#19C739",
    fontWeight: 400,
    padding: "6px 10px",
    border: "1px solid #19C739",
    borderRadius: "4px",
    width: "93px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginRight: "8px",
  },
  typoTitle: {
    fontSize: "19px",
    fontWeight: 700,
  },
  typoDescription: {
    marginTop: "8px",
    fontSize: "16px",
    color: "#8D9198",
    fontWeight: 400,
  },
  typoDeadline: {
    fontSize: "12px",
    color: "#8D9198",
    fontWeight: 400,
  },
  typoLocation: {
    fontSize: 12,
    color: "#4621C5",
    padding: "6px 10px",
    backgroundColor: "#EDE9FB",
    borderRadius: "4px",
  },
}));

export const MediumJobCard = ({ data }) => {
  const classes = useStyles();
  const router = useRouter();
  const { user } = useAuthState();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [openEdit, setOpenEdit] = React.useState<boolean>(false);
  const [openConfirm, setOpenConfirm] = React.useState<boolean>(false);
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  const [
    deleteRecruitment,
    { loading: loadingDeleteRecruit, error: errorRecruit },
  ] = useMutation(DELETE_RECRUITMENT);

  //dropdown icon handle edit and delete job
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const dropdown = (
    <Menu
      id="simple-menu"
      anchorEl={anchorEl}
      getContentAnchorEl={null}
      open={Boolean(anchorEl)}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      onClose={handleClose}
    >
      <MenuItem style={{ width: "210px" }} onClick={() => setOpenEdit(true)}>
        <Typography variant="h5">Chỉnh sửa</Typography>
      </MenuItem>
      <MenuItem onClick={() => setOpenConfirm(true)}>
        <Typography variant="h5" color="error">
          Xóa tuyển dụng
        </Typography>
      </MenuItem>
    </Menu>
  );

  //delete comment
  const handleDeleteRecruitment = () => {
    deleteRecruitment({
      variables: {
        recruitment_id: data.id,
      },
    })
      .then(() => router.reload())
      .catch((err) => console.log(err));
  };

  if (!data) {
    return <Loading />;
  }

  return (
    <Grid
      className={classes.root}
      style={{
        padding: isDevicePhone ? "11px 15px" : "15px 16px",
        height: isDevicePhone ? "122px" : "161px",
      }}
    >
      <Grid style={{ position: "absolute", top: 13, right: 16 }}>
        {user &&
          ((data && data?.org?.id == user?.id) ||
            router.pathname == "/myorg") && (
            <IconButton
              aria-haspopup="true"
              style={{ padding: 0, zIndex: 10 }}
              onClick={handleClick}
            >
              <MoreHorizIcon />
            </IconButton>
          )}
        {dropdown}
        <ConfirmUpdateDynamic
          Title="Bạn muốn xóa tuyển dụng?"
          context={`Hành động này sẽ xóa tuyển dụng ${data.title} `}
          button1ctn="Quay lại"
          button2ctn="Lưu thay đổi"
          open={openConfirm}
          handleBack={() => setOpenConfirm(false)}
          handleClose={() => setOpenConfirm(false)}
          handleConfirm={handleDeleteRecruitment}
          loading={loadingDeleteRecruit}
          stylebtn1={{ color: "#532BDC" }}
          stylebtn2={{ background: "#E4593B", color: "white" }}
        />
        <UpdateRecruitDialogDynamic
          open={openEdit}
          handleClose={() => setOpenEdit(false)}
          data={data}
        />
      </Grid>
      <Link href={"/recruit/" + data?.id}>
        <a>
          <Grid
            item
            container
            direction="row"
            justify="flex-start"
            style={{ padding: 0 }}
          >
            <Grid
              item
              xs={isDevicePhone ? 5 : 4}
              style={{ paddingRight: isDevicePhone && "11px" }}
            >
              <div
                style={{
                  backgroundImage: `url(${
                    data.cover_url ? data.cover_url : "/temp-cover.jpg"
                  })`,
                  width: isDevicePhone ? "100%" : "92%",
                  height: isDevicePhone ? "99px" : "131px",
                }}
                className={classes.coverImg}
              />
            </Grid>
            <Grid
              item
              style={{ padding: 0 }}
              container
              xs={isDevicePhone ? 7 : 8}
            >
              <Grid container>
                <Grid item xs={3} style={{ height: "27px" }}>
                  <Typography className={classes.typoJobState}>
                    {new Date().getTime() < new Date(data?.deadline).getTime()
                      ? "Đang mở đơn"
                      : "Đã đóng đơn"}
                  </Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography
                    className={classes.typoTitle}
                    style={{ marginTop: isDevicePhone && "3px" }}
                  >
                    {data.title.length > 25
                      ? data.title.slice(0, 25) + "..."
                      : data.title}
                  </Typography>
                </Grid>

                {!isDevicePhone && (
                  <Grid item xs={12}>
                    <Typography className={classes.typoDescription}>
                      {data?.job_description?.length > 50
                        ? data?.job_description.slice(0, 100) + "..."
                        : data?.job_description}
                    </Typography>
                  </Grid>
                )}
              </Grid>
              <Grid
                item
                xs={12}
                container
                justify="space-between"
                direction="row"
                alignItems="flex-end"
                style={{ paddingTop: "8px" }}
              >
                <Typography className={classes.typoDeadline}>
                  HẠN CUỐI:
                  {new Date(data?.deadline).toLocaleDateString("en-GB")}
                </Typography>

                {!isDevicePhone && (
                  <Typography className={classes.typoLocation}>
                    {data?.location}
                  </Typography>
                )}
              </Grid>
            </Grid>
          </Grid>
        </a>
      </Link>
    </Grid>
  );
};

const DELETE_RECRUITMENT = gql`
  mutation ($recruitment_id: String!) {
    delete_recruitment(recruitment_id: $recruitment_id)
  }
`;
