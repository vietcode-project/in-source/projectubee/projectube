import {
  Button,
  Dialog,
  Divider,
  Grid,
  Select,
  TextField,
  Typography,
  useTheme,
  MenuItem,
  makeStyles,
  IconButton,
  CircularProgress,
  useMediaQuery, 
  Theme
} from "@material-ui/core";
import React, { ChangeEvent, useState } from "react";
import cities from "config/cities.json";
import CloseIcon from "@material-ui/icons/Close";
import PolicyCheckbox from "components/PolicyCheckbox";
import { gql, useMutation } from "@apollo/client";
import { useRouter } from "next/router";

interface IInput {
  cover_url: File;
  title: string;
  // name: string;
  job_description: string;
  deadline: Date | string;
  location: string;
}

const useStyles = makeStyles(() => ({
  menuPaper: {
    maxHeight: 200,
  },
}));

export const CreateJobDialog = ({ handleClose, open }) => {
  const theme = useTheme();
  const router = useRouter();
  const classes = useStyles();
  const [coverPreview, setCoverPreview] = useState("");
  const [locked, setLocked] = useState(false);
  const [checked, setChecker] = useState(false);
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  const [input, setInput] = useState<IInput>({
    cover_url: null,
    title: "",
    // name: "",
    job_description: "",
    deadline: new Date().toISOString().slice(0, 10),
    location: "",
  });

  const [
    createRecruitment,
    { loading: loadingRecruit, data: dataRecruit, error: errRecruit },
  ] = useMutation(CREATE_RECRUITMENT);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!locked) {
      setLocked(() => true);
      createRecruitment({
        variables: {
          title: input.title,
          job_description: input.job_description,
          deadline:
            typeof input.deadline == "string"
              ? new Date(input.deadline)
              : input.deadline,
          cover_url: input.cover_url,
          location: input.location,
        },
      })
        .then(({ data, errors }) => {
          setLocked(() => false);
          if (errors) console.error("Create error: " + errors);
          if (data) {
            router.reload();
          }
        })
        .catch((err) => console.error(err));
    }
  };

  const onSelectCover = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.persist();
    setInput((prev) => ({ ...prev, cover_url: e.target.files[0] }));
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setCoverPreview(objectUrl);
  };

  const handleInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    e.persist();
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };

  return (
    <Dialog
      maxWidth="md"
      onClose={handleClose}
      open={open}
      fullScreen={isDevicePhone}
      PaperProps={{
        style: {
          padding: theme.spacing(7, 6),
           width: isDevicePhone ? "100%" : "60%",
          minWidth: "400px",
        },
      }}
    >
      <form onSubmit={handleSubmit}>
        <Grid
          container
          alignItems="center"
          style={{ marginBottom: theme.spacing(3) }}
        >
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h2" display="inline">
            Đăng mục tuyển dụng
          </Typography>
        </Grid>

        <Typography variant="h2">Ảnh bìa mục tuyển dụng</Typography>
        <Typography color="textSecondary" gutterBottom>
          Ảnh bìa mục tuyển dụng sẽ giúp nhiều người chú ý hơn
        </Typography>

        <Grid
          container
          justify="center"
          style={{
            paddingTop: "28.125%",
            paddingBottom: "28.125%",
            border: `${
              coverPreview == undefined
                ? "1px solid transparent"
                : "1px solid #DEE4ED"
            }`,
            borderRadius: "8px",
            backgroundImage: `${
              coverPreview != undefined ? `url(${coverPreview})` : ""
            }`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
        >
          <Grid item>
            {!input.cover_url && (
              <Typography variant="h3">Kéo thả ảnh vào đây</Typography>
            )}
          </Grid>
        </Grid>

        <br />
        <Button variant="outlined" component="label">
          <Typography variant="button">Chọn ảnh từ thiết bị của bạn</Typography>
          <input type="file" accept="image/*" onChange={onSelectCover} hidden />
        </Button>

        <Divider style={{ margin: theme.spacing(4, 0) }} />

        <Typography variant="h2" style={{ marginBottom: theme.spacing(4) }}>
          Thông tin tuyển dụng
        </Typography>
        <div style={{ marginBottom: theme.spacing(4) }}>
          <Typography>Tiêu đề tuyển dụng</Typography>
          <TextField
            onChange={handleInputChange}
            value={input.title}
            name="title"
            placeholder="Tên tiêu đề"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            variant="outlined"
          />
        </div>

        {/* <div style={{ marginBottom: theme.spacing(4) }}>
          <Typography>Tên dự án/tổ chức tuyển dụng</Typography>
          <TextField
            onChange={handleInputChange}
            value={input.name}
            name="name"
            placeholder="Tên dự án/tổ chức"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            variant="outlined"
          />
        </div> */}

        <div style={{ marginBottom: theme.spacing(3) }}>
          <Grid container spacing={2}>
            <Grid item xs={isDevicePhone?12:6}>
              <Typography style={{ marginBottom: "16px" }}>
                Khu vực tuyển dụng{" "}
              </Typography>
              <Select
                fullWidth
                MenuProps={{
                  getContentAnchorEl: null,
                  anchorOrigin: {
                    vertical: "bottom",
                    horizontal: "left",
                  },
                  classes: { paper: classes.menuPaper },
                }}
                value={input.location}
                onChange={handleInputChange}
                variant="outlined"
                name="location"
              >
                {cities.map((city, index) => (
                  <MenuItem key={index} value={city}>
                    {city}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
            <Grid item xs={isDevicePhone?12:6}>
              <Typography style={{ marginBottom: "16px" }}>
                Hạn tuyển dụng
              </Typography>
              <TextField
                onChange={handleInputChange}
                value={input.deadline}
                name="deadline"
                type="date"
                variant="outlined"
                fullWidth
              />
            </Grid>
          </Grid>
        </div>

        <div style={{ marginBottom: theme.spacing(4) }}>
          <Grid
            container
            justify="space-between"
            alignItems="center"
            style={{ marginBottom: theme.spacing(2) }}
          >
            <Grid item>
              <Typography>Miêu tả chi tiết</Typography>
            </Grid>
            <Grid item>
              <Typography variant="body2" color="textSecondary">
                {5000 - input.job_description.length} ký tự còn lại
              </Typography>
            </Grid>
          </Grid>

          <TextField
            onChange={handleInputChange}
            value={input.job_description}
            inputProps={{ maxLength: 5000 }}
            name="job_description"
            variant="outlined"
            label="Nhập văn bản"
            fullWidth
            rows={8}
            multiline
          />
        </div>
        <Divider style={{ marginBottom: theme.spacing(3) }} />

        <Grid container alignItems="center" style={{ marginBottom: "80px" }} direction={isDevicePhone ? "column" : "row"}>
          <Grid item xs={isDevicePhone?12:5}>
            <PolicyCheckbox
              checked={checked}
              checkboxClick={() => setChecker(!checked)}
            />
          </Grid>
          <Grid item xs={isDevicePhone?12:7}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              fullWidth
              disabled={
                !(
                  input.title != "" &&
                  // input.name != "" &&
                  input.location != "" &&
                  input.job_description != "" &&
                  input.deadline &&
                  !locked &&
                  checked &&
                  !loadingRecruit &&
                  !dataRecruit
                )
              }
            >
              {!loadingRecruit && !dataRecruit ? (
                "Tạo tin tuyển dụng"
              ) : (
                <CircularProgress color="inherit" size={25} />
              )}
            </Button>
          </Grid>
        </Grid>
      </form>
    </Dialog>
  );
};

const CREATE_RECRUITMENT = gql`
  mutation (
    $job_description: String!
    $deadline: DateTime
    $location: String!
    $cover_url: Upload
    $title: String!
  ) {
    create_recruitment(
      data: {
        title: $title
        location: $location
        job_description: $job_description
        deadline: $deadline
        cover_url: $cover_url
      }
    ) {
      title
      location
      job_description
      deadline
      cover_url
    }
  }
`;
