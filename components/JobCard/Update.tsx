import {
  Button,
  Dialog,
  Divider,
  Grid,
  Select,
  TextField,
  Typography,
  useTheme,
  MenuItem,
  makeStyles,
  IconButton,
  CircularProgress,
  useMediaQuery,
  Theme
} from "@material-ui/core";
import React, { ChangeEvent, useState } from "react";
import cities from "config/cities.json";
import CloseIcon from "@material-ui/icons/Close";
import PolicyCheckbox from "components/PolicyCheckbox";
import { gql, useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import convert from "widgets/Convert";

interface IInput {
  cover_url: File;
  title: string;
  // name: string;
  job_description: string;
  deadline: Date | string;
  location: string;
  isOpen: boolean;
}

const useStyles = makeStyles(() => ({
  menuPaper: {
    maxHeight: 200,
  },
}));

export const UpdateRecruitDialog = ({ handleClose, open, data }) => {
  const theme = useTheme();
  const router = useRouter();
  const classes = useStyles();
  const [coverPreview, setCoverPreview] = useState(data.cover_url);

  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  const [input, setInput] = useState<IInput>({
    cover_url: data.cover_url,
    title: data.title,
    // name: "",
    job_description: data.job_description,
    deadline: convert(new Date(data.deadline)),
    location: data.location,
    isOpen: data.isOpen,
  });

  const [
    updateRecruitment,
    { loading: loadingRecruit, data: dataRecruit, error: errRecruit },
  ] = useMutation(UPDATE_RECRUITMENT);

  const resetChange = () => {
    setInput({
      cover_url: data.cover_url,
      title: data.title,
      // name: "",
      job_description: data.job_description,
      deadline: convert(new Date(data.deadline)),
      location: data.location,
      isOpen: data.isOpen,
    });
    setCoverPreview(data.cover_url);
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    updateRecruitment({
      variables: {
        recruitment_id: data.id,
        cover_url: input.cover_url,
        title: input.title,
        // name: "",
        job_description: input.job_description,
        deadline: convert(new Date(input.deadline)),
        location: input.location,
        isOpen: input.isOpen,
      },
    })
      .then(({ data, errors }) => {
        router.reload();
      })
      .catch((err) => console.error(err));
  };

  const onSelectCover = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.persist();
    setInput((prev) => ({ ...prev, cover_url: e.target.files[0] }));
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setCoverPreview(objectUrl);
  };

  const handleInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    e.persist();
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };

  return (
    <Dialog
      maxWidth="md"
      onClose={handleClose}
      open={open}
       fullScreen={isDevicePhone}
      PaperProps={{
        style: {
          padding: theme.spacing(7, 6),
        width: isDevicePhone ? "100%" : "60%",
          minWidth: "400px",
        },
      }}
    >
      <form onSubmit={handleSubmit}>
        <Grid
          container
          alignItems="center"
          style={{ marginBottom: theme.spacing(3) }}
        >
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h2" display="inline">
            Đăng mục tuyển dụng
          </Typography>
        </Grid>

        <Typography variant="h2">Ảnh bìa mục tuyển dụng</Typography>
        <Typography color="textSecondary" gutterBottom>
          Ảnh bìa mục tuyển dụng sẽ giúp nhiều người chú ý hơn
        </Typography>

        <Grid
          container
          justify="center"
          style={{
            paddingTop: "28.125%",
            paddingBottom: "28.125%",
            border: `${
              coverPreview == undefined
                ? "1px solid transparent"
                : "1px solid #DEE4ED"
            }`,
            borderRadius: "8px",
            backgroundImage: `${
              coverPreview != undefined ? `url(${coverPreview})` : ""
            }`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
        >
          <Grid item>
            {!coverPreview && (
              <Typography variant="h3">Kéo thả ảnh vào đây</Typography>
            )}
          </Grid>
        </Grid>

        <br />
        <Button variant="outlined" component="label">
          <Typography variant="button">Chọn ảnh từ thiết bị của bạn</Typography>
          <input type="file" accept="image/*" onChange={onSelectCover} hidden />
        </Button>

        <Divider style={{ margin: theme.spacing(4, 0) }} />

        <Typography variant="h2" style={{ marginBottom: theme.spacing(4) }}>
          Thông tin tuyển dụng
        </Typography>
        <div style={{ marginBottom: theme.spacing(4) }}>
          <Typography>Tiêu đề tuyển dụng</Typography>
          <TextField
            onChange={handleInputChange}
            value={input.title}
            name="title"
            placeholder="Tên tiêu đề"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            variant="outlined"
          />
        </div>

        {/* <div style={{ marginBottom: theme.spacing(4) }}>
            <Typography>Tên dự án/tổ chức tuyển dụng</Typography>
            <TextField
              onChange={handleInputChange}
              value={input.name}
              name="name"
              placeholder="Tên dự án/tổ chức"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
            />
          </div> */}

        <div style={{ marginBottom: theme.spacing(3) }}>
          <Grid container spacing={2}>
            <Grid item  xs={isDevicePhone?12:6}>
              <Typography style={{ marginBottom: "16px" }}>
                Khu vực tuyển dụng
              </Typography>
              <Select
                fullWidth
                MenuProps={{
                  getContentAnchorEl: null,
                  anchorOrigin: {
                    vertical: "bottom",
                    horizontal: "left",
                  },
                  classes: { paper: classes.menuPaper },
                }}
                value={input.location}
                onChange={handleInputChange}
                variant="outlined"
                name="location"
              >
                {cities.map((city, index) => (
                  <MenuItem key={index} value={city}>
                    {city}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
            <Grid item  xs={isDevicePhone?12:6}>
              <Typography style={{ marginBottom: "16px" }}>
                Hạn tuyển dụng
              </Typography>
              <TextField
                onChange={handleInputChange}
                value={input.deadline}
                name="deadline"
                type="date"
                variant="outlined"
                fullWidth
              />
            </Grid>
          </Grid>
        </div>

        <div style={{ marginBottom: theme.spacing(4) }}>
          <Grid
            container
            justify="space-between"
            alignItems="center"
            style={{ marginBottom: theme.spacing(2) }}
          >
            <Grid item>
              <Typography>Miêu tả chi tiết</Typography>
            </Grid>
            <Grid item>
              <Typography variant="body2" color="textSecondary">
                {5000 - input?.job_description?.length} ký tự còn lại
              </Typography>
            </Grid>
          </Grid>

          <TextField
            onChange={handleInputChange}
            value={input?.job_description}
            inputProps={{ maxLength: 5000 }}
            name="job_description"
            variant="outlined"
            label="Nhập văn bản"
            fullWidth
            rows={8}
            multiline
          />
        </div>
        <Divider style={{ marginBottom: theme.spacing(3) }} />

        <Grid container spacing={1} justify="flex-end" direction={isDevicePhone ? "column" : "row"}>
          <Grid item xs={isDevicePhone?12:5}>
            <Button
              fullWidth
              variant="outlined"
              color="primary"
              style={{
                padding: "9px 20px",
              }}
              onClick={resetChange}
              disabled={
                input.title === data.title &&
                coverPreview === data.cover_url &&
                input.job_description === data.job_description &&
                input.location === data.location &&
                input.deadline === convert(new Date(data.deadline))
              }
            >
              <Typography display="inline">Hoàn tác thay đổi</Typography>
            </Button>
          </Grid>
          <Grid item  xs={isDevicePhone?12:5}>
            <Button
              onClick={handleSubmit}
              variant="contained"
              color="primary"
              fullWidth
              disabled={
                input.title === data.title &&
                coverPreview === data.cover_url &&
                input.job_description === data.job_description &&
                input.location === data.location &&
                input.deadline === convert(new Date(data.deadline)) &&
                loadingRecruit &&
                dataRecruit
              }
            >
              {!loadingRecruit && !dataRecruit ? (
                "Lưu thay đổi"
              ) : (
                <CircularProgress color="inherit" size={25} />
              )}
            </Button>
          </Grid>
        </Grid>
      </form>
    </Dialog>
  );
};

const UPDATE_RECRUITMENT = gql`
  mutation (
    $job_description: String
    $deadline: DateTime
    $location: String
    $cover_url: Upload
    $title: String!
    $recruitment_id: String!
    $isOpen: Boolean!
  ) {
    update_recruitment(
      data: {
        title: $title
        location: $location
        job_description: $job_description
        deadline: $deadline
        cover_url: $cover_url
        isOpen: $isOpen
      }
      recruitment_id: $recruitment_id
    ) {
      title
      location
      job_description
      deadline
      cover_url
      isOpen
    }
  }
`;

export default UpdateRecruitDialog;
