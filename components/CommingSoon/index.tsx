import React, { ReactElement } from "react";
import { Typography } from "@material-ui/core";

export default function index(): ReactElement {
  return (
    <Typography variant="h5" gutterBottom color="primary">
      Tính năng này hiện đang được xây dựng
    </Typography>
  );
}
