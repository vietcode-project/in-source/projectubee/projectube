import React, { ReactElement } from "react";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  rotate: {
    animation: `$rotate 2s`,
    animationIterationCount:"infinite",
    transitionTimingFunction:"linear"
  },
  upDown:{
    animation:`$updown 2s`,
    animationIterationCount:"infinite",
    transitionTimingFunction:"linear"
  },
  "@global": {
    "@keyframes rotate": {
      "0%":{
        transform: "rotate(0deg)",
      },
      "37.5%":{
        transform: "rotate(0deg)",
      },
     
      "75%": {
        transform: "rotate(-180deg)",
      },
      "100%": {
        transform: "rotate(-180deg)",
      },
      
    },
    "@keyframes updown":{
        "0%":{
            transform:"translateY(0px)"
        },
        "25%":{
            transform:"translateY(15px)"
        },
        "100%":{
            transform:"translateY(15px)"
        },        
    }
  },
}));

export default function Loading(): ReactElement {
  const classes = useStyles();
  return (
    <div
      style={{
        width: "100%",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <div
        className={classes.rotate}
        style={{
          width: "68px",
          height: "70px",
          margin: "auto",
          backgroundColor: "#DEE4ED",
          borderRadius: "10px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <div
          style={{
            backgroundColor: "white",
            padding: "auto",
            width: "56px",
            height: "58px",
            textAlign: "center",
            borderRadius: "8px",
          }}>
          <div
            className={classes.upDown}
            style={{
              width: "45px",
              height: "32px",
              borderRadius: "5px",
              margin: "6px auto 20px auto",
              backgroundColor: "#DEE4ED",
            }}></div>
        </div>
      </div>
    </div>
  );
}
