import React from "react";
import {
  Button,
  Dialog,
  Grid,
  IconButton,
  Typography,
  Checkbox,
} from "@material-ui/core";

import CloseIcon from "@material-ui/icons/Close";

interface IProps {
  checked: boolean;
  checkboxClick: () => void;
}

export default function PolicyCheckbox({ checked, checkboxClick }: IProps) {
  const [openPolicy, setOpenPolicy] = React.useState(false);
  const handleClosePolicy = () => {
    setOpenPolicy(false);
  };

  const PolicyDialog = (
    <Dialog
      PaperProps={{
        style: {
          padding: "28px 24px",
          width: "60%",
          minWidth: "400px",
        },
      }}
      scroll="body"
      maxWidth="md"
      open={openPolicy}
      onClose={handleClosePolicy}
    >
      <IconButton
        edge="start"
        color="inherit"
        onClick={handleClosePolicy}
        aria-label="close"
      >
        <CloseIcon />
      </IconButton>
      <Grid>
        <Typography
          style={{ marginBottom: 40 }}
          variant="h1"
          color="primary"
          align="center"
          gutterBottom
        >
          Điều khoản và Bảo mật
        </Typography>
        <Grid>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Cám ơn người dùng đã truy cập vào Website Projectube. Chúng tôi tôn
            trọng và cam kết sẽ bảo mật những thông tin mang tính riêng tư của
            bạn. Xin vui lòng đọc bản Chính sách bảo mật dưới đây để hiểu hơn
            những cam kết mà chúng tôi thực hiện nhằm tôn trọng và bảo vệ quyền
            lợi của người truy cập.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chính sách bảo mật sẽ giải thích cách chúng tôi tiếp nhận, sử dụng
            và (trong trường hợp nào đó) tiết lộ thông tin cá nhân của bạn.
            Chính sách cũng sẽ giải thích các bước chúng tôi thực hiện để bảo
            mật thông tin cá nhân của người dùng. Cuối cùng, Chính sách bảo mật
            sẽ giải thích quyền lựa chọn của người dùng về việc thu thập, sử
            dụng và tiết lộ thông tin cá nhân của mình.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Bảo vệ dữ liệu cá nhân và gây dựng được niềm tin cho người dùng là
            vấn đề rất quan trọng với chúng tôi. Vì vậy, chúng tôi sẽ dùng tên
            và các thông tin khác liên quan đến người dùng tuân thủ theo nội
            dung của Chính sách bảo mật. Chúng tôi chỉ thu thập những thông tin
            cần thiết liên quan đến hỗ trợ người dùng.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chúng tôi sẽ giữ thông tin của người dùng trong thời gian luật pháp
            quy định hoặc cho mục đích nào đó. người dùng có thể truy cập vào
            website và trình duyệt mà không cần phải cung cấp chi tiết cá nhân.
          </Typography>
        </Grid>
        <Typography variant="h3" gutterBottom style={{ marginBottom: 16 }}>
          <b>1. Thu thập thông tin cá nhân</b>
        </Typography>
        <Grid>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chúng tôi sẽ thu thập nhiều thông tin khác nhau của người dùng khi
            bạn muốn truy cập Website Projectube nhằm mục đích tạo sự tiện nghi
            tới người dùng.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chúng tôi thu thập, lưu trữ và xử lý thông tin của bạn cho quá trình
            truy cập sử dụng tài khoản Projectube và cho những thông báo sau
            này, và để cung cấp dịch vụ. Chúng tôi không giới hạn thông tin cá
            nhân: danh hiệu, tên, giới tính, ngày sinh, email, địa chỉ, địa chỉ
            giao hàng, số điện thoại, fax, chi tiết thanh toán, chi tiết thanh
            toán bằng thẻ hoặc chi tiết tài khoản ngân hàng.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chúng tôi sẽ dùng thông tin người dùng đã cung cấp để cung cấp các
            dịch vụ và thông tin yêu cầu thông qua website và theo yêu cầu của
            bạn. Hơn nữa, chúng tôi sẽ sử dụng các thông tin đó để quản lý tài
            khoản của bạn; xác minh và thực hiện giao tương tác trực tuyến, kiểm
            toán việc tải dữ liệu từ web; cải thiện bố cục và nội dung trang web
            và điều chỉnh cho phù hợp với người dùng; nhận diện khách vào web,
            nghiên cứu nhân khẩu học, gửi thông tin bao gồm thông tin sản phẩm
            và dịch vụ, nếu bạn không có dấu hiệu từ chối. Nếu người dùng không
            muốn nhận bất cứ thông tin tiếp thị của chúng tôi thì có thể từ chối
            bất cứ lúc nào.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chúng tôi có thể chuyển tên và địa chỉ cho bên thứ ba để họ tiếp
            nhận nhu cầu của bạn (ví dụ cho bên nhà tư vấn và các tổ chức thành
            viên).
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Thông tin cá nhân của bạn được chúng tôi lưu giữ nhưng vì lý do bảo
            mật nên chúng tôi không công khai trực tiếp được. người dùng cam kết
            bảo mật dữ liệu cá nhân và không được phép tiết lộ cho bên thứ ba.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            <i>
              <b>Điều lệ khác về thông tin cá nhân</b>
            </i>
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chúng tôi có thể dùng thông tin cá nhân của bạn để nghiên cứu thị
            trường. chi tiết sẽ được ẩn và chỉ được dùng để thống kê. người dùng
            có thể từ chối không tham gia bất cứ lúc nào. Bất kỳ câu trả lời cho
            khảo sát hoặc thăm dò dư luận mà chúng tôi cần bạn làm sẽ không được
            chuyển cho bên thứ ba. Việc cần thiết duy nhất là tiết lộ email của
            bạn nếu bạn muốn tham gia. Câu trả lời sẽ được lưu tách riêng với
            email của bạn.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Bạn sẽ nhận được thông tin về chúng tôi, về website, các website
            khác, sản phẩm,, bản tin, bất cứ những gì liên quan đến các công ty
            nằm trong nhóm hoặc các đối tác liên kết. Nếu người dùng không muốn
            nhận những thông tin này, vui lòng nhấn vào link từ chối trong bất
            kỳ email chúng tôi gửi cho bạn. Trong vòng 7 ngày nhận chỉ dẫn của
            bạn, chúng tôi sẽ ngừng gửi thông tin. Nếu thấy không rõ, chúng tôi
            sẽ liên lạc với bạn.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chúng tôi sẽ tiếp tục giấu tên dữ liệu người dùng trên website và sử
            dụng cho nhiều mục đích khác nhau, bao gồm việc xác định vị trí của
            người dùng và cách sử dụng các khía cạnh nhất định của website hoặc
            đường link chứa trong email tới người dùng và cung cấp dữ liệu ẩn
            danh đó cho bên thứ 3 là nhà xuất bản. Tuy nhiên, dữ liệu này không
            có khả năng xác định cá nhân.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            <i>
              <b>Đối tác thứ ba và Liên kết</b>
            </i>
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chúng tôi có thể chuyển thông tin của người dùng cho các tổ chức
            khác trong nhóm. Chúng tôi có thể chuyển thông tin của người dùng
            cho các tổ chức thành viên trong khuôn khổ quy định của Chính sách
            bảo mật.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Ví dụ: chúng tôi sẽ nhờ bên thứ ba tiếp nhận nhu cầu đăng ký tham
            gia sự kiện, phân tích dữ liệu, tiếp thị và hỗ trợ dịch vụ người
            dùng. Chúng tôi có thể chuyển cơ sở dữ liệu gồm thông tin cá nhân
            của bạn nếu chúng tôi bán cả công ty hoặc chỉ một phần. Trong khuôn
            khổ Chính sách bảo mật, chúng tôi không bán hay tiết lộ dữ liệu cá
            nhân của bạn cho bên thứ ba mà không được đồng ý trước trừ khi điều
            này là cần thiết cho các điều khoản trong Chính sách bảo mật hoặc
            chúng tôi được yêu cầu phải làm như vậy theo quy định của Pháp luật.
            Website có thể bao gồm quảng cáo của bên thứ ba và các liên kết đến
            các trang web khác hoặc khung của các trang web khác.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Xin lưu ý rằng chúng tôi không có nhiệm vụ bảo mật thông tin hay nội
            dung của bên thứ ba hay các website khác, hay bất kỳ bên thứ ba nào
            mà chúng tôi chuyển giao dữ liệu cho phù hợp với Chính sách bảo mật.
          </Typography>
        </Grid>
        <Typography variant="h3" gutterBottom style={{ marginBottom: 16 }}>
          <b>2. Sử dụng Cookie</b>
        </Typography>
        <Grid>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Cookie là tập tin văn bản nhỏ có thể nhận dạng tên truy cập duy nhất
            từ máy tính của bạn đến máy chủ của chúng tôi khi bạn truy cập vào
            các trang nhất định trên website và sẽ được lưu bởi trình duyệt
            internet lên ổ cứng máy tính của bạn. Cookie được dùng để nhận dạng
            địa chỉ IP, lưu lại thời gian. Chúng tôi dùng cookie để tiện cho
            người dùng vào web (ví dụ: ghi nhớ tên truy cập khi bạn muốn vào
            thay đổi lại giỏ mua hàng mà không cần phải nhập lại địa chỉ email
            của mình) và không đòi hỏi bất kỳ thông tin nào về bạn (ví dụ: mục
            tiêu quảng cáo). Trình duyệt của bạn có thể được thiết lập không sử
            dụng cookie nhưng điều này sẽ hạn chế quyền sử dụng của bạn trên
            web. Xin vui lòng chấp nhận cam kết của chúng tôi là cookie không
            bao gồm bất cứ chi tiết cá nhân riêng tư nào và an toàn với virus.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Trình duyệt này sử dụng Google Analytics, một dịch vụ phân tích web
            được cung cấp bởi Google, Inc. (“Google”). Google Analytics dùng
            cookie, là những tập tin văn bản đặt trong máy tính để giúp website
            phân tích người dùng vào web như thế nào. Thông tin được tổng hợp từ
            cookie sẽ được truyền tới và lưu bởi Google trên các máy chủ tại Hoa
            Kỳ. Google sẽ dùng thông tin này để đánh giá cách dùng web của bạn,
            lập báo cáo về các hoạt động trên web cho các nhà khai thác web và
            cung cấp các dịch vụ khác liên quan đến các hoạt động internet và
            cách dùng internet. Google cũng có thể chuyển giao thông tin này cho
            bên thứ ba theo yêu cầu của pháp luật hoặc các bên thứ ba xử lý
            thông tin trên danh nghĩa của Google. Google sẽ không kết hợp địa
            chỉ IP của bạn với bất kỳ dữ liệu nào khác mà Google đang giữ. người
            dùng có thể từ chối dùng cookie bằng cách chọn các thiết lập thích
            hợp trên trình duyệt của mình, tuy nhiên lưu ý rằng điều này sẽ ngăn
            bạn sử dụng triệt để chức năng của website. Bằng cách sử dụng trang
            web này, bạn đã đồng ý cho Google xử lý dữ liệu về bạn theo cách
            thức và các mục đích nêu trên.
          </Typography>
        </Grid>
        <Typography variant="h3" gutterBottom style={{ marginBottom: 16 }}>
          <b>3. Bảo mật</b>
        </Typography>
        <Grid>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Chúng tôi có biện pháp thích hợp về kỹ thuật và an ninh để ngăn chặn
            truy cập trái phép hoặc trái pháp luật hoặc mất mát hoặc tiêu hủy
            hoặc thiệt hại cho thông tin của bạn. Khi thu thập dữ liệu trên web,
            chúng tôi thu thập chi tiết cá nhân của bạn trên máy chủ an toàn.
            Chúng tôi dùng tường lửa cho máy chủ. Khi thu thập chi tiết các thẻ
            thanh toán điện tử, chúng tôi dùng mã hóa bằng Secure Socket Layer
            (SSL). Khi chúng tôi không thể bảo đảm an ninh 100%, SSL sẽ gây khó
            khăn cho hacker muốn giải mã thông tin của người dùng. Bạn không nên
            gửi đầy đủ chi tiết của thẻ tín dụng hay thẻ ghi nợ khi chưa được mã
            hóa cho chúng tôi. Chúng tôi duy trì các biện pháp bảo vệ vật lý và
            điện tử trong mối liên kết với thu thập, lưu trữ và tiết lộ thông
            tin của bạn.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Các thủ tục an toàn của chúng tôi nghĩa là chúng tôi có thể đôi khi
            yêu cầu giấy tờ chứng minh trước khi tiết lộ thông tin cá nhân cho
            bạn. Chúng tôi khuyên người dùng rằng người dùng không nên đưa thông
            tin chi tiết về tài khoản cá nhân với bất kỳ ai bằng email, chúng
            tôi không chịu trách nhiệm về những mất mát người dùng có thể gánh
            chịu trong việc trao đổi thông tin của người dùng qua internet hoặc
            email.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Người dùng tuyệt đối không sử dụng bất kỳ chương trình, công cụ hay
            hình thức nào khác để can thiệp vào hệ thống hay làm thay đổi cấu
            trúc dữ liệu. Nghiêm cấm việc phát tán, truyền bá hay cổ vũ cho bất
            của hệ thống website. Mọi vi phạm sẽ bị tước bỏ mọi quyền lợi cũng
            như sẽ bị truy tố trước pháp luật nếu cần thiết.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Mọi thông tin giao dịch sẽ được bảo mật nhưng trong trường hợp cơ
            quan pháp luật yêu cầu, chúng tôi sẽ buộc phải cung cấp những thông
            tin này cho các cơ quan pháp luật.
          </Typography>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Các điều kiện, điều khoản và nội dung của trang web này được điều
            chỉnh bởi luật pháp Việt Nam và tòa án Việt Nam có thẩm quyền xem
            xét.
          </Typography>
        </Grid>
        <Typography variant="h3" gutterBottom style={{ marginBottom: 16 }}>
          <b>4. Quyền lợi người dùng</b>
        </Typography>
        <Grid>
          <Typography gutterBottom style={{ marginBottom: 16 }}>
            Người dùng có quyền yêu cầu truy cập vào dữ liệu cá nhân của mình,
            có quyền yêu cầu chúng tôi sửa lại những sai sót trong dữ liệu của
            bạn mà không mất phí. Bất cứ lúc nào bạn cũng có quyền yêu cầu chúng
            tôi ngưng sử dụng dữ liệu cá nhân của bạn cho mục đích tiếp thị.
          </Typography>
        </Grid>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          onClick={handleClosePolicy}
        >
          Đóng
        </Button>
      </Grid>
    </Dialog>
  );

  return (
    <Grid container alignItems="center" direction="row">
      {PolicyDialog}
      <Grid item xs={1}>
        <Checkbox
          onClick={checkboxClick}
          color="primary"
          icon={<div />}
          checkedIcon={<div />}
        />
      </Grid>
      <Grid item xs={11}>
        <Grid>
          <Typography variant="h5">
            Tôi chấp nhận mọi{" "}
            <Typography
              display="inline"
              style={{
                textDecoration: "underline",
                cursor: "pointer",
                color: "#532BDC",
              }}
              onClick={() => setOpenPolicy(true)}
            >
              Điều khoản và Bảo mật
            </Typography>{" "}
            của Projectube khi tạo sự kiện mới
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
}
