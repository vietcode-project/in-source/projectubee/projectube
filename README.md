# Front-end Onboarding Docs

Setup cho front end

## :gear: Cài đặt

- Clone repo về máy

```bash
git clone git@gitlab.com:vietcode-project/in-source/projectubee/projectube.git
```

#### Nhớ chuyển sang branch staging rồi tải các dependencies về

- Tải các dependencies

```bash
npm i
```

- Tải SSL HTTP proxy

```bash
npm install -g local-ssl-proxy
```

- mở cmd hoăc powershell chạy

```bash
Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted
```

## :runner: Sử dụng

-chuyển sang branch staging

### chạy https ở localhost:

- mở terminal

```bash
local-ssl-proxy --target 3001 --source 3000
```

### setup env

- Tạo file .env rồi thêm vào

```bash
NEXT_PUBLIC_SERVER_URI = https://projectube-server.herokuapp.com/api/gql
```


### chạy client

```bash
npm run dev
```

rồi mở link https://localhost:3000 chọn tùy chọn nâng cao rồi chọn cho phép

### Chúc mừng bạn đã vô được

## :book: Contributing

Các bạn có thể tạo issues về bug, tính năng, hoặc bất cứ ý tưởng gì của các bạn để đóng góp cho chúng tôi.

# Coding Guideline

## Cấu trúc các branch
- `master`: branch chứa các version đã deploy qua thời gian, dùng để backup
- `staging`: branch dùng để deploy và dev
- `hotfix`: branch để fix bug

## Dev
- Khi giải quyết 1 issue, tạo branch cá nhân theo cấu trúc dev/name/branch_name (VD:dev/Hoang/Navbar)
- Tất cả các branch mới của bất kì issue đều phải được tạo ra từ staging (checkout từ staging), trừ các trường hợp bắt buộc phải tạo từ 1 branch phụ nào đấy do các tính năng có liên kết
- Để đảm bảo các commit của mình sạch và được chia nhỏ đầy đủ, bạn có thể tạo một branch private rồi squash commit sang branch đang dev của bạn. Tham khảo tại [đây](https://www.youtube.com/watch?v=Hlp-9cdImSM)
- Thường xuyên cập nhật branch của mình với staging bằng cách pull staging xuống branch dev đang làm
- Nếu bạn làm 1 tính năng lớn, hãy tạo branch lớn cho nó rồi dev ở các branch nhỏ hơn rồi tạo merge request vào branch đấy (VD: `staging` <--- `dev/Name/featureA` <--- `dev/Name/componentsA` )

## Git commit message
- Cấu trúc message: `<type>[optional scope]: <description> [optional body]` (VD:feat: Add loading effect on homepage)
- Type thường dùng
  - fix: Fix bug, issue
  - feat: Thêm tính năng
  - refactor: refactor
  - build: Viết layout, add utils
  - docs: Viết giấy tờ, guideline
Tham khảo cách sử dụng tại [đây](https://www.conventionalcommits.org/en/v1.0.0/)

## Tạo một merge request
- Chỉ được tạo merge vào `staging` hoặc các branch cá nhân khác trừ `master`
- Nội dung cần hiển thị
  - Lý do
  - Thay đổi
  - Issue đi kèm (nếu có)
  - Nếu là tính năng mới thì cần demo (ảnh,vid...)
- File change không được quá 10
- Số thay đổi ở 1 commit < 250 dòng
- Sau khi hoàn thiện nội dung trên tag @hoanghohohaha vào làm Reviewer (Front) hoặc @duc0905 (Back)

# Issue Label
- High-prio: Cần làm nhanh, có ý nghĩa & hậu quả lớn
- Low-prio: Không có hậu quả quá lớn
- Good First Task: Issue dễ để làm quen với codebase
- Bug
- Feature
- Improvement

# Code structure

- Bạn có thể xem directory tree dưới đây để hiểu rõ hơn cấu trúc của file đang chạy

<details><summary>Click to expand</summary>

```bash
├───components #folder components
│   │   ...
│   │
│   ├───Calendar #thẻ lịch
│   │       ...
│   │
│   ├───CommingSoon
│   │       index.tsx
│   │
│   ├───eventCard #các thẻ đi với event
│   │       ...
│   │
│   ├───header #phân header hiện thị ở trang chính khi chưa đăng nhập
│   │       ...
│   │
│   ├───memberCard #các thể hiện thị thành viên
│   │       ...
│   │
│   ├───orgCard #các thẻ về tổ chức
│   │       ...
│   │
│   └───UserCard #các thẻ về user
│           ...
│
├───config
│       categories.json #file json chưa các catergories khi lọc trong trang events và orgs
│
├───contexts
│   └───auth #query khi đâng nhập đăng xuất
│           ...
│
├───graphql
│       EventFragment.ts
│       OrgFragment.ts
│
├───hocs #higher order components
│       withLayout.tsx #hocs để wrap layout
│       withLove.tsx
│
├───layouts
│   │   Auth.tsx #phần layout cho trang đăng nhập và đăng kí
│   │
│   ├───Guest
│   │   │   index.tsx
│   │   │
│   │   └───components
│   │       └───Navbar
│   │               index.tsx #layout thanh nav bar khi chưa đăng nhập
│   │
│   └───User
│       │   index.tsx
│       │
│       └───components
│           └───Navbar
│                   index.tsx #layout thanh nav bar khi đã đăng nhập
│
├───logos
├───pages #Folder các trang hiện thị
│   │   index.tsx #Trang chủ
│   │   search.tsx #trang khi dùng thanh search
│   │   signin.tsx #trang đăng nhập
│   │   signup.tsx #trang đăng kí
│   │   _app.tsx
│   │   _document.tsx
│   │
│   ├───confirm
│   │       [confirm_id].tsx #trang xác nhận
│   │
│   ├───events
│   │       index.tsx #trang hiện thị các sự kiện
│   │       [id].tsx # trang hiện thị chi tiết của sự kiện
│   │
│   ├───myorg
│   │       index.tsx #trang tổ chức cá nhân
│   │
│   ├───orgs
│   │   │   index.tsx #trang các tổ chức
│   │   │   new.tsx #trang tạo tổ chức mới
│   │   │
│   │   └───[org_id]
│   │           edit.tsx #trang chỉnh sửa tổ chức
│   │           index.tsx #trang chi tiết của một tổ chức
│   │
│   └───profile
│           edit.tsx #trang chỉnh sửa thông tin cá nhân
│           index.tsx #trang hiện thị thông tin cá nhân người dùng
│
├───public
│   └───fonts #folder chứa font design riêng
│
├───styles
│       common.css #file css chính (nhưng vãn có file đc add style dùng useStyles của material)
│
├───types #Định dạng
│       Event.tsx #định dạng cho events
│       Org.tsx #định dạng cho orgs
│       User.tsx #định dạng cho User
│
├───utils
│       apolloClient.ts #setup của apollo client, uri trong env sẽ đc thêm vào đây
│       theme.ts #setup theme
│       validators.ts #validate của form sign in sign up
│
└───widgets #các components để cập nhật thông tin khi edit
    ├──...
```

</details>

