import React, { ReactElement, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

import IOrg from "types/Org";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { TextField } from "@material-ui/core";
import { gql, useMutation } from "@apollo/client";
import { useAuthState } from "contexts/auth";

interface Props {
  prevContact: IOrg["contact"];
  handleClose: () => void;
}

const useStyles = makeStyles((theme) => ({
  sectionSpacer: {
    marginBottom: theme.spacing(4),
  },
}));

export default function index({
  prevContact,
  handleClose,
}: Props): ReactElement {
  const classes = useStyles();
  const [update, { loading: queryLoading }] = useMutation(
    UPDATE_CONTACT_MUTATION
  );
  const [input, setInput] = useState<Props["prevContact"]>({ ...prevContact });
  const { refetch, loading } = useAuthState();

  async function handleSave() {
    const result = await update({
      variables: {
        data: {
          email: input.email,
          phone_number: input.phone_number,
          location: input.location,
          website: input.website,
        },
      },
    });

    if (!result) return;

    await refetch();

    handleClose();
  }

  function handleChange(
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) {
    e.persist();

    setInput((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  }

  if (loading || queryLoading)
    return <Typography variant="h3">Loading...</Typography>;

  return (
    <div>
      <Typography variant="h2" gutterBottom>
        Chỉnh sửa liên hệ
      </Typography>

      <div className={classes.sectionSpacer}>
        <TextField
          fullWidth
          variant="outlined"
          label="Email"
          value={input.email || ""}
          name="email"
          onChange={handleChange}
        />
      </div>

      <div className={classes.sectionSpacer}>
        <TextField
          fullWidth
          variant="outlined"
          label="Số điện thoại"
          value={input.phone_number || ""}
          name="phone_number"
          onChange={handleChange}
        />
      </div>

      <div className={classes.sectionSpacer}>
        <TextField
          fullWidth
          variant="outlined"
          label="Địa chỉ"
          value={input.location || ""}
          name="location"
          onChange={handleChange}
        />
      </div>

      <div className={classes.sectionSpacer}>
        <TextField
          fullWidth
          variant="outlined"
          label="Website"
          value={input.website || ""}
          name="website"
          onChange={handleChange}
        />
      </div>

      <Button variant="contained" color="secondary" onClick={handleSave}>
        Lưu
      </Button>
    </div>
  );
}

const UPDATE_CONTACT_MUTATION = gql`
  mutation ($data: UpdateContactInput!) {
    update_contact(data: $data)
  }
`;
