import React, { ReactElement, useState } from "react";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { gql, useMutation } from "@apollo/client";
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  sectionSpacer: {
    marginBottom: theme.spacing(4),
  },
}));

interface Props {
  id: string;
  prevDescription: string;
  refetch: () => Promise<any>;
  handleClose: () => void;
}

export default function index({
  id,
  prevDescription,
  refetch,
  handleClose,
}: Props): ReactElement {
  const classes = useStyles();
  const [update, { loading }] = useMutation(UPDATE_EVENT_TITLE);
  const [description, setDescription] = useState(prevDescription);

  async function handleSave() {
    await update({
      variables: {
        data: {
          description,
        },
        id,
      },
    });

    await refetch();

    handleClose();
  }

  if (loading) return <Typography variant="h3">Loading...</Typography>;

  return (
    <div>
      <Typography variant="h2" gutterBottom>
        Cập nhật mô tả
      </Typography>

      <div className={classes.sectionSpacer}>
        <TextField
          fullWidth
          label="Mô tả"
          variant="outlined"
          multiline
          rows={3}
          value={description}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setDescription(e.target.value)
          }
        />
      </div>

      <Button variant="contained" color="secondary" onClick={handleSave}>
        Lưu
      </Button>
    </div>
  );
}

const UPDATE_EVENT_TITLE = gql`
  mutation($id: String!, $data: UpdateEventInput!) {
    update_event(id: $id, data: $data) {
      description
    }
  }
`;
