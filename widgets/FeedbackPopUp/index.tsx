import { Button, Snackbar, Typography } from "@material-ui/core";
import { FeedbackForm } from "components/Feedbackform";
import Image from "next/image";
import React, { useState } from "react";

const quesList = [
  {
    question: "Về độ hữu ích",
    context:
      "Projectube có giúp bạn tìm kiếm thông tin về các hoạt động hiệu quả hơn không?",
  },
  {
    question: "Về chất lượng thông tin",
    context:
      "Projectube có cung cấp cho bạn thông tin chính xác và đầy đủ hay không?",
  },
  {
    question: "Về số lượng thông tin",
    context: "Bạn cảm thấy thông tin mà Projectube cung cấp nhiều hay ít?",
  },
  {
    question: "Về chất lượng giao diện",
    context: "Bạn cảm thấy giao diện của Projectube có dễ sử dụng không?",
  },
];

export default function FeedbackPopUp({ scrollTop, user }) {
  const [openSuccess, setOpenSuccess] = useState(false);
  const [openForm, setOpenForm] = useState(false);
  const [open, setOpen] = useState(true);

  const handleCloseAlert = () => {
    setOpenSuccess(false);
  };

  const handleCloseFeedback = () => {
    setOpen(false);
  };
  return (
    <>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        open={openSuccess}
        autoHideDuration={5000}
        onClose={handleCloseAlert}
      >
        <Button
          style={{
            background: "#41D762",
            color: "white",
            borderRadius: "24px",
            padding: "4px 12px",
          }}
          onClick={handleCloseAlert}
        >
          <Typography>
            Projectube xin cám ơn phản hồi của bạn {"<"}3{" "}
          </Typography>
        </Button>
      </Snackbar>
      {user && !openForm && (
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          open={open}
        >
          <Button
            style={{ borderRadius: "24px", padding: "16px" }}
            color="primary"
            variant="contained"
            onClick={() => {
              setOpenForm(true);
            }}
            startIcon={<Image src="/star.svg" width={16} height={16} />}
          >
            <Typography variant="h5">Phản hồi</Typography>
          </Button>
        </Snackbar>
      )}
      {!user && scrollTop > 300 && !openForm && (
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          open={open}
        >
          <Button
            style={{ borderRadius: "24px", padding: "16px" }}
            color="primary"
            variant="contained"
            onClick={() => {
              setOpenForm(true);
            }}
            startIcon={<Image src="/star.svg" width={16} height={16} />}
          >
            <Typography variant="h5">Phản hồi</Typography>
          </Button>
        </Snackbar>
      )}
      <FeedbackForm
        email={(user && user.email) || ""}
        handleSubmit={() => {
          setOpenForm(false);
          handleCloseFeedback();
          setOpenSuccess(true);
        }}
        questions={quesList}
        open={openForm}
        handleClose={() => setOpenForm(false)}
      />
    </>
  );
}
