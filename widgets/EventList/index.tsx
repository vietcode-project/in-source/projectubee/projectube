import React from "react";
import { Button, Grid, useTheme } from "@material-ui/core";
import { LargeEventCard } from "components/eventCard";
import { IEventCard } from "types/Event";

interface Props {
  data: IEventCard[];
  loadMore: () => void;
}

export default function EventList({ data, loadMore }: Props) {
  const theme = useTheme();

  const eventCards = React.useMemo(
    () =>
      data.map((e) => (
        <Grid item md={3} xs={4} key={e.id}>
          <LargeEventCard {...e} />
        </Grid>
      )),
    [data]
  );

  return (
    <>
      <Grid container spacing={3}>
        {eventCards}
      </Grid>

      <div
        style={{ width: 300, margin: "0 auto", marginTop: theme.spacing(4) }}
      >
        <Button variant="outlined" color="primary" fullWidth onClick={loadMore}>
          Xem thêm
        </Button>
      </div>
    </>
  );
}
