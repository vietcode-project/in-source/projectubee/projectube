import { Grid } from "@material-ui/core";
import React from "react";

export default function AboutPageMobileImageDisplay() {
  return (
    <>
      <Grid container justify="center" style={{ width: "100%" }}>
        <Grid
          item
          style={{
            maxWidth: "120vw",
            display: "flex",
            overflowX: "scroll",
          }}
        >
          <div style={{ margin: "0 3px" }}>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwt9h7BL5HzGIaq5qfPLNaH502oB24bJFiww&usqp=CAU"
              height="250"
            />
          </div>
          <div style={{ margin: "0 3px" }}>
            <img
              src="https://lh3.googleusercontent.com/ZokGCSsn0836X4Dt0SBNQYzavqQcGL-WDeMBNOSGqe5MGK9dp_JATWJTyWk4eRIFW4xhVovtugcq0Ynssvejel3Nc_v7T8c61UWz2LAL9SbHaJ8kTs5KRc_pAvmuJut1eMNQmw-e"
              height="250"
            />
          </div>
          <div style={{ margin: "0 3px" }}>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTCT7I-N09WegrFCrN_vEJ2VhJhsAew_RqRjObVp0fNE_nGHTB9yCn9YZn_l4aTHWx6UP8&usqp=CAU"
              height="250"
            />
          </div>
          <div style={{ margin: "0 3px" }}>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVQAQ5QGJxwL6z9zggQ9AVorcN2XTh23lf9vBSeOJqNQav8kOyznfJj8Y3wQDWwyW0mOY&usqp=CAU"
              height="250"
            />
          </div>
          <div style={{ margin: "0 3px" }}>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjFQFAje8ZkHNy13Bh3kXB1IMQWOgrg7HRtdrWQ089EznWsjpVkmCRAvXp0W20XrVzavs&usqp=CAU"
              height="250"
            />
          </div>
          <div style={{ margin: "0 3px" }}>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4FF4smWdj4gBDb-WQL46L1D0bAgMqZjLdSQ&usqp=CAU"
              height="250"
            />
          </div>
          <div style={{ margin: "0 3px" }}>
            <img
              src="https://images.unsplash.com/photo-1579546929518-9e396f3cc809?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8MXw5ODE2MjE4fHxlbnwwfHx8fA%3D%3D&w=1000&q=80"
              height="250"
            />
          </div>
          <div style={{ margin: "0 3px" }}>
            <img
              src="https://www2.shutterstock.com/blog/wp-content/uploads/sites/5/2018/05/Gradient-Roundup-Illustrator-02.jpg"
              height="250"
            />
          </div>
        </Grid>
      </Grid>
    </>
  );
}
