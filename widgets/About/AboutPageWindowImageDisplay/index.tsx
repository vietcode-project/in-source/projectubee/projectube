import { Grid } from "@material-ui/core";
import React from "react";

export default function AboutPageImageDisplayWindow() {
  return (
    <Grid container spacing={2}>
      <Grid item xs={4}>
        <Grid container spacing={2} justify="flex-end">
          <Grid item xs={12}>
            <div
              style={{
                width: "100%",
                paddingTop: "70%",
                backgroundImage:
                  "url('https://99designs-blog.imgix.net/blog/wp-content/uploads/2018/12/Gradient_builder_2.jpg?auto=format&q=60&w=1815&h=1200&fit=crop&crop=faces')",

                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
              }}
            ></div>
          </Grid>
          <Grid item xs={6}>
            <div
              style={{
                width: "100%",
                paddingTop: "120%",
                background:
                  "url('https://lh3.googleusercontent.com/ZokGCSsn0836X4Dt0SBNQYzavqQcGL-WDeMBNOSGqe5MGK9dp_JATWJTyWk4eRIFW4xhVovtugcq0Ynssvejel3Nc_v7T8c61UWz2LAL9SbHaJ8kTs5KRc_pAvmuJut1eMNQmw-e')",
                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
              }}
            ></div>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={8}>
        <Grid container spacing={2} alignItems="flex-end">
          <Grid item xs={5}>
            <div
              style={{
                width: "100%",
                paddingTop: "62%",
                background:
                  "url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwt9h7BL5HzGIaq5qfPLNaH502oB24bJFiww&usqp=CAU')",
                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
              }}
            ></div>
          </Grid>
          <Grid item xs={5}>
            <div
              style={{
                width: "100%",
                paddingTop: "40%",
                background:
                  "url('https://99designs-blog.imgix.net/blog/wp-content/uploads/2018/12/Gradient_builder_2.jpg?auto=format&q=60&w=1815&h=1200&fit=crop&crop=faces')",

                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
              }}
            ></div>
          </Grid>
        </Grid>
        <Grid container spacing={2} style={{ marginTop: 8 }}>
          <Grid item xs={6}>
            <div
              style={{
                width: "100%",
                paddingTop: "100%",
                background:
                  "url('https://lh3.googleusercontent.com/ZokGCSsn0836X4Dt0SBNQYzavqQcGL-WDeMBNOSGqe5MGK9dp_JATWJTyWk4eRIFW4xhVovtugcq0Ynssvejel3Nc_v7T8c61UWz2LAL9SbHaJ8kTs5KRc_pAvmuJut1eMNQmw-e')",

                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
              }}
            ></div>
          </Grid>
          <Grid item xs={6}>
            <div
              style={{
                width: "100%",
                paddingTop: "60%",
                background:
                  "url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwt9h7BL5HzGIaq5qfPLNaH502oB24bJFiww&usqp=CAU')",

                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
              }}
            ></div>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
