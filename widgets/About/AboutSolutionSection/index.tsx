import { Grid, Typography, Theme, useMediaQuery } from "@material-ui/core";
import React from "react";
import Image from "next/image";

export default function AboutSolutionSection() {
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  return (
    <Grid>
      <Grid
        container
        direction={isDevicePhone ? "column" : "row"}
        style={{ paddingBottom: "60px" }}
      >
        <Grid item xs={isDevicePhone ? 10 : 6}>
          <Grid
            container
            style={{ height: "100%" }}
            direction="column"
            justify="center"
            alignContent="center"
          >
            <Grid
              container
              justify="flex-end"
              style={{ padding: "22px 31px", background: "#EDE9FB" }}
            >
              <Grid item xs={10}>
                <Typography
                  align={isDevicePhone ? "left" : "right"}
                  style={{ fontSize: "32px", fontWeight: 700 }}
                  color="primary"
                >
                  Projectube tự tin đem lại cho bạn trải nghiệm khác biệt so với
                  những nền tảng tương tự:
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={isDevicePhone ? 12 : 6} style={{ padding: "40px 89px" }}>
          <Grid
            container
            alignItems={isDevicePhone ? "center" : "flex-start"}
            direction="column"
            spacing={2}
          >
            <Grid item>
              <Image src="/Icon_search_purple.svg" height={70} width={70} />
            </Grid>
            <Grid item>
              <Typography
                style={{ fontSize: "19px", fontWeight: 700 }}
                color="primary"
              >
                Tính năng và "nguồn tài nguyên" đa dạng
              </Typography>
            </Grid>
            <Grid item>
              <Typography
                align={isDevicePhone ? "center" : "left"}
                color="textSecondary"
              >
                Projectube sở hữu những tính năng và "nguồn tài nguyên" đa dạng
                giúp bạn có thể tìm những thông tin phù hợp với nhu cầu cá nhân.
              </Typography>
            </Grid>
            <Grid
              container
              direction="column"
              alignItems={isDevicePhone ? "center" : "flex-start"}
              spacing={2}
            >
              <Grid item>
                <Image src="/Icon_user_purple.svg" height={70} width={70} />
              </Grid>
              <Grid item>
                <Typography
                  style={{ fontSize: "19px", fontWeight: 700 }}
                  color="primary"
                >
                  Thân thiện với người dùng
                </Typography>
              </Grid>
              <Grid item>
                <Typography
                  align={isDevicePhone ? "center" : "left"}
                  color="textSecondary"
                >
                  Projectube đem đến giao diện thân thiện với người dùng, chỉ
                  với một số thao tác đơn giản, bạn có thể tiếp cận được với
                  những tổ chức mình cần tìm kiếm.
                </Typography>
              </Grid>

              <Grid
                container
                direction="column"
                alignItems={isDevicePhone ? "center" : "flex-start"}
                spacing={2}
              >
                <Grid item>
                  <Image
                    src="/Icon_bookcheck_purple.svg"
                    height={70}
                    width={70}
                  />
                </Grid>
                <Grid item>
                  <Typography
                    style={{ fontSize: "19px", fontWeight: 700 }}
                    color="primary"
                  >
                    Nguồn thông tin chính xác, đầy đủ nhất
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography
                    align={isDevicePhone ? "center" : "left"}
                    color="textSecondary"
                  >
                    Projectube đảm bảo cung cấp nguồn thông tin chính xác, đầy
                    đủ nhất về các tổ chức và hoạt động ngoại khóa.
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
