import {
  Button,
  Grid,
  Typography,
  Theme,
  useMediaQuery,
} from "@material-ui/core";
import React from "react";
export default function AboutActionSection() {
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );
  return (
    <>
      <Grid
        style={{
          padding: isDevicePhone ? "66px 20px" : "66px 156px",
          backgroundColor: "#532BDC",
        }}
      >
        <Grid
          container
          alignContent="center"
          justify="center"
          direction="column"
          style={{
            padding: isDevicePhone ? "60px 24px" : "60px 95px",
            backgroundColor: "#EDE9FB",
          }}
        >
          <Grid item style={{ marginTop: 24 }}>
            <Typography align="center" variant="h1" color="primary">
              “Đừng chờ đợi”
            </Typography>
          </Grid>
          <Grid container justify="center" style={{ margin: "16px 0" }}>
            <Grid item xs={isDevicePhone ? 12 : 8}>
              <Typography align="center">
                Làm việc theo phương châm “Đừng chờ đợi”, Projectube tin rằng
                mỗi người không nên ngần ngại nắm bắt cơ hội để tìm được điều
                mình mong muốn. Hãy bắt tay vào việc ngay để thực hiện những dự
                định của bản thân.
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Typography align="center">
              Và nếu bạn cần sự giúp đỡ, hãy đến với Projectube -{" "}
              <Typography color="primary" display="inline">
                Nơi dự án của bạn không còn là dự định.
              </Typography>
            </Typography>
          </Grid>
          <Grid item>
            <Grid container justify="center" style={{ marginTop: 24 }}>
              <Button variant="contained" color="primary" href="/">
                <Typography align="center">
                  Bắt đầu hành trình của bạn với Projectube
                </Typography>
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
