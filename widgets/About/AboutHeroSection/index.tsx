import { Grid } from "@material-ui/core";
import React from "react";
import Image from "next/image";

export default function AboutHeroSection() {
  return (
    <Grid
      style={{
        height: "40vh",
        backgroundImage: `url(/about.svg)`,
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        backgroundSize: "cover",
      }}
    >
      <Grid
        container
        justify="center"
        alignItems="center"
        style={{
          height: "100%",
          width: "100%",
          background:
            "linear-gradient(180deg, rgba(120, 96, 203, 0.6) 0%, rgba(29, 21, 57, 0) 100%)",
        }}
      >
        <Grid item>
          <Image src="/Logo_Light.svg" height={61} width={353} />
        </Grid>
      </Grid>
    </Grid>
  );
}
