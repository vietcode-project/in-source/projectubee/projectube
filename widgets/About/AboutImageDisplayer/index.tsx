import { Grid, useMediaQuery, Theme } from "@material-ui/core";
import React from "react";
import AboutPageMobileImageDisplay from "../AboutPageMobileImageDisplay";
import AboutPageImageDisplayWindow from "../AboutPageWindowImageDisplay";

export default function AboutImageDisplay() {
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  return (
    <Grid container justify="center" style={{ marginBottom: "60px" }}>
      <Grid item xs={isDevicePhone ? 12 : 9}>
        {isDevicePhone ? (
          <AboutPageMobileImageDisplay />
        ) : (
          <AboutPageImageDisplayWindow />
        )}
      </Grid>
    </Grid>
  );
}
