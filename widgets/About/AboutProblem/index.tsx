import { Grid, Typography, Theme, useMediaQuery } from "@material-ui/core";
import { Container } from "next/app";
import React from "react";

export default function AboutProblemSection() {
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  const Section1 = () => (
    <Grid item xs={isDevicePhone ? 12 : 7}>
      <Grid
        container
        justify={isDevicePhone ? "space-between" : "center"}
        spacing={isDevicePhone ? 0 : 4}
      >
        <Grid item xs={9} style={{ marginBottom: `${isDevicePhone ? 24 : 0}` }}>
          <Typography variant="h3">
            Với sự chuyển biến không ngừng của xã hôi và sự phát triển mạnh mẽ
            của công nghệ thông tin, việc sống hết mình và tạo ra giá trị riêng
            của bản thân là tâm niệm của hầu hết các bạn trẻ trong thời đại ngày
            nay. Như một hệ quả tất yếu, các tổ chức hoạt động ngoại khóa được
            ra đời ngày một nhiều hơn, nhằm thỏa mãn nguyện vọng được cống hiến
            cho cộng đồng, giúp các bạn thể hiện cá tính riêng và phát huy những
            tiềm năng của mình.
          </Typography>
          <br />
        </Grid>
        <Grid item xs={9} style={{ marginBottom: `${isDevicePhone ? 24 : 0}` }}>
          <Typography variant="h3">
            Tuy nhiên, sự xuất hiện tràn lan của các tổ chức, gây ra quá tải
            thông tin cùng việc chưa có công cụ tổng hợp hiệu quả đã dẫn đến sự
            "mất kết nối" giữa các cá nhân và tổ chức. Mặt khác, các bạn trẻ
            cũng gặp nhiều khó khăn trong việc tìm ra những tổ chức phù hợp với
            cá tính, sở thích, và khả năng của mình.
          </Typography>
          <br />
        </Grid>
        <Grid item xs={9} style={{ marginBottom: `${isDevicePhone ? 24 : 0}` }}>
          <Typography variant="h3">
            Nhìn nhận và hiểu rõ thực trạng ấy, Projectube ra mắt nhằm giúp học
            sinh sinh viên Việt Nam cải thiện chất lượng làm hoạt động ngoại
            khoá. Projectube hướng đến một tương lai mà mọi học sinh, sinh viên
            Việt Nam đều có thể tìm được tổ chức phù hợp với bản thân mình, được
            hoạt động chuyên nghiệp, và tạo ra giá trị cho cộng đồng.
          </Typography>
          <br />
        </Grid>
      </Grid>
    </Grid>
  );

  const Section2 = () => (
    <Grid item xs={isDevicePhone ? 12 : 5}>
      <Grid
        container
        style={{
          marginBottom: `${isDevicePhone ? "60px" : 0}`,
          height: "100%",
        }}
        direction="column"
        justify="center"
        alignContent="center"
      >
        <Grid item>
          <Grid container style={{ background: "#4621C5" }}>
            <Grid item xs={isDevicePhone ? 12 : 8}>
              <Typography
                variant="h1"
                style={{ color: "white", padding: "20px 32px" }}
              >
                Vấn đề mà Projectube trăn trở
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <Container>
      <Grid
        direction={isDevicePhone ? "column" : "row"}
        container
        style={{
          background: "#EDE9FB",
          padding: "60px 0",
          paddingLeft: "30px",
        }}
      >
        {isDevicePhone ? (
          <>
            <Section2 />
            <Section1 />
          </>
        ) : (
          <>
            <Section1 />
            <Section2 />
          </>
        )}
      </Grid>
    </Container>
  );
}
