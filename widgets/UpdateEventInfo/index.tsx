import React, { ReactElement, useState } from "react";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { gql, useMutation } from "@apollo/client";
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  sectionSpacer: {
    marginBottom: theme.spacing(4),
  },
}));

interface Props {
  id: string;
  prevName: string;
  prevInfo: {
    start_time: Date;
    location: string;
    ticket_price: number;
  };
  refetch: () => Promise<any>;
  handleClose: () => void;
}

export default function index({
  id,
  prevName,
  prevInfo,
  refetch,
  handleClose,
}: Props): ReactElement {
  const classes = useStyles();
  const [update, { loading }] = useMutation(UPDATE_EVENT_TITLE);
  const [name, setName] = useState(prevName);
  const [info, setInfo] = useState({ ...prevInfo });

  async function handleSave() {
    await update({
      variables: {
        data: {
          name,
          ...info,
          ticket_price: Number(info.ticket_price),
        },
        id,
      },
    });

    await refetch();

    handleClose();
  }

  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();

    setInfo((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  }

  if (loading) return <Typography variant="h3">Loading...</Typography>;

  return (
    <div>
      <Typography variant="h2" gutterBottom>
        Cập nhật tên và mô tả
      </Typography>

      <div className={classes.sectionSpacer}>
        <TextField
          fullWidth
          label="Tên"
          value={name}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setName(e.target.value)
          }
        />
      </div>

      <div className={classes.sectionSpacer}>
        <TextField
          fullWidth
          label="Thời gian bắt đầu"
          name="start_time"
          value={info.start_time}
          onChange={handleChange}
        />
      </div>

      <div className={classes.sectionSpacer}>
        <TextField
          fullWidth
          label="Địa điểm"
          name="location"
          value={info.location}
          onChange={handleChange}
        />
      </div>

      <div className={classes.sectionSpacer}>
        <TextField
          fullWidth
          type="number"
          label="Giá vé (VND)"
          name="ticket_price"
          value={info.ticket_price}
          onChange={handleChange}
        />
      </div>

      <Button variant="contained" color="secondary" onClick={handleSave}>
        Lưu
      </Button>
    </div>
  );
}

const UPDATE_EVENT_TITLE = gql`
  mutation($id: String!, $data: UpdateEventInput!) {
    update_event(id: $id, data: $data) {
      name
      description
    }
  }
`;
