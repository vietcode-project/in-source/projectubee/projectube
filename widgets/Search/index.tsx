import React, { ChangeEvent, useEffect, useState } from "react";
import { BigSearchField } from "components/Search";
import { SearchMobileButton } from "components/Search";
import { Theme, useMediaQuery } from "@material-ui/core";

interface IProps {
  queryevent?: any;
  queryorg?: any;
  queryrecruitment?: any;
}

const ResponsiveSearchField = ({ queryevent, queryorg }: IProps) => {
  const [openSearch, setOpenSearch] = useState<boolean>(false);
  const isDeviceTablet = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down("sm")
  );
  return (
    <>
      <div>
        {isDeviceTablet ? (
          <div>
            <SearchMobileButton
              state={openSearch}
              onClick={() => setOpenSearch(true)}
            />
          </div>
        ) : (
          <BigSearchField queryevent={queryevent} queryorg={queryorg} />
        )}
      </div>
      {isDeviceTablet && (
        <div
          style={{
            display: openSearch ? "block" : "none",
            width: "100%",
          }}
        >
          <BigSearchField queryevent={queryevent} queryorg={queryorg} />
        </div>
      )}
    </>
  );
};

export default ResponsiveSearchField;
