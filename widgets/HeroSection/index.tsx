import { Button, makeStyles } from "@material-ui/core";
import { Content, Title } from "components/header";
import React from "react";
import Link from "next/link";

const useStyles = makeStyles((theme) => ({
  headerTitle: {
    display: "flex",
    justifyContent: "center",
    textAlign: "center",
  },
  heroImage: {
    paddingTop: "65%",
    width: "100%",
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundImage: `url("/Hero Image.svg")`,
    zIndex: -1,
  },
}));

export default function HeroSection({ user }) {
  const classes = useStyles();
  return (
    <>
      {!user && (
        <>
          <div
            className={classes.headerTitle}
            style={{
              marginBottom: "16px",
            }}
          >
            <Title />
          </div>
          {/* <div className={classes.headerTitle}>
            <Content />
          </div> */}
          <div
            style={{
              marginTop: "24px",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <>
              <Link href="/signup">
                <a>
                  <Button
                    variant="outlined"
                    color="primary"
                    style={{ marginRight: "8px" }}
                  >
                    Đăng ký
                  </Button>
                </a>
              </Link>
              <Link href="/signin">
                <a>
                  <Button variant="contained" color="primary">
                    Đăng nhập
                  </Button>
                </a>
              </Link>
            </>
          </div>
          <div className={classes.heroImage}></div>
        </>
      )}
    </>
  );
}
