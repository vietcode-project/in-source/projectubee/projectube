import React, { ReactElement } from "react";
import {
  Button,
  Grid,
  makeStyles,
  Menu,
  TextField,
  Typography,
  Select,
  MenuItem,
  useMediaQuery,
  Theme
} from "@material-ui/core";

import cates from "config/categories.json";
import CateList from "components/Catelist";
import cities from "config/cities.json";

const useStyles = makeStyles(() => ({
  root: { padding: "24px 32px 24px 24px" },
  adddelbtn: {
    border: "1px solid #DEE4ED",
    borderRadius: 4,
    height: 24,
    width: 24,
    padding: 8,
  },
  menuPaper:{
    maxHeight:200
  }
}));

interface IFilter {
  location: string;
  start_time: string;
  categories: string[];
}

interface Props {
  anchorEl: HTMLElement;
  filter: IFilter;

  handleClose: () => void;
  handleInputChange: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  handleChooseCategories: (label: string) => void;
  handleRemoveFilter: () => void;
}

export default function EventFilter({
  anchorEl,
  filter,
  handleClose,
  handleInputChange,
  handleChooseCategories,
  handleRemoveFilter,
}: Props): ReactElement {
  const classes = useStyles();

  const disableClearButton = React.useMemo(() => {
    if (filter.location) return false;
    if (filter.categories.length) return false;
    if (filter.start_time) return false;
    return true;
  }, [filter]);
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down("xs")
  );
  return (
    <Menu
      id="simple-menu"
      anchorEl={anchorEl}
      getContentAnchorEl={null}
      open={Boolean(anchorEl)}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      onClose={handleClose}
    >
      <Grid className={classes.root} style={{width: !isDevicePhone && 784 }} container>
        <Grid container spacing={4} direction="row">
          <Grid item xs={12} md={6}>
            <Typography style={{ marginBottom: 16 }}>
              Địa điểm tổ chức
            </Typography>
            <Select
              fullWidth
              MenuProps={{
                getContentAnchorEl: null,
                anchorOrigin: {
                  vertical: "bottom",
                  horizontal: "left",
                },
                classes:{ paper: classes.menuPaper }
              }}
              value={filter.location}
              onChange={handleInputChange}
              variant="outlined"
              name="location"
              >
              {cities.map((city, index) => (
                <MenuItem key={index} value={city}>
                  {city}
                </MenuItem>
              ))}
            </Select>
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography style={{ marginBottom: 16 }}>Ngày tổ chức</Typography>
            <TextField
              onChange={handleInputChange}
              value={filter.start_time}
              name="start_time"
              type="date"
              variant="outlined"
              fullWidth
            />
          </Grid>
        </Grid>

        <Grid container direction="column" style={{ marginTop: 16 }}>
          <Typography variant="h5">Lọc theo chủ đề</Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            style={{ marginBottom: 16 }}
          >
            Vui lòng chọn tối đa 3 chủ đề
          </Typography>
          <CateList
            cates={cates}
            selected={filter.categories}
            handleChoose={handleChooseCategories}
          />
        </Grid>
        <Button
          style={{ marginTop: 16, width: "100%" }}
          color="primary"
          variant="contained"
          onClick={handleRemoveFilter}
          disabled={disableClearButton}
        >
          Xóa bộ lọc
        </Button>
      </Grid>
    </Menu>
  );
}
