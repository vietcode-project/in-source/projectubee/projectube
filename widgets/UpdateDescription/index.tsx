import React, { useState } from "react";
import { useMutation, gql } from "@apollo/client";
import {
  Button,
  Grid,
  IconButton,
  makeStyles,
  TextField,
  Typography,
  useTheme,
} from "@material-ui/core";
import { DeleteOutlined } from "@material-ui/icons";

import { useAuthState } from "contexts/auth";
import IOrg from "types/Org";

const useStyles = makeStyles((theme) => ({
  sectionSpacer: {
    marginBottom: theme.spacing(4),
  },
}));

export default function UpdateDescription({
  prevDesc,
  handleClose,
}: {
  prevDesc: IOrg["description"];
  handleClose: () => void;
}) {
  const classes = useStyles();
  const theme = useTheme();
  const [update, { data, loading: queryLoading, error }] =
    useMutation(UPDATE_DESC_MUTATION);
  const { loading, refetch } = useAuthState();
  const [desc, setDesc] = useState<{ title: string; content: string }[]>(
    JSON.parse(JSON.stringify(prevDesc)) || []
  );

  function handleAddDesc() {
    if (desc.length === 3) return;
    setDesc((prev) => [...prev, { title: "", content: "" }]);
  }

  function handleRemoveDesc(index: number) {
    if (0 > index || index > desc.length) return;

    setDesc((prev) => {
      return [...prev.slice(0, index), ...prev.slice(index + 1, prev.length)];
    });
  }

  function handleChange(
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    index: number,
    field: "title" | "content"
  ) {
    e.persist();

    setDesc((prev) => {
      const temp = [...prev];

      temp[index][field] = e.target.value;

      return temp;
    });
  }

  async function handleSave() {
    await update({
      variables: {
        new_desc: desc.map((d) => ({ title: d.title, content: d.content })),
      },
    });

    await refetch();

    handleClose();
  }

  if (loading || queryLoading) {
    return <Typography variant="h3">Đang cập nhật</Typography>;
  }

  return (
    <div>
      <Typography variant="h2" gutterBottom>
        Chỉnh sửa mô tả
      </Typography>

      <div className={classes.sectionSpacer}>
        {Array(desc.length)
          .fill(null)
          .map((_, i) => (
            <Grid container wrap="nowrap" alignItems="center" key={i}>
              <Grid item style={{ flexGrow: 1, flexShrink: 1 }}>
                <div className={classes.sectionSpacer}>
                  <TextField
                    label="Tiêu đề"
                    fullWidth
                    value={desc[i].title}
                    onChange={(e) => handleChange(e, i, "title")}
                  />
                </div>
                <div>
                  <TextField
                    variant="outlined"
                    label="Nội dung"
                    fullWidth
                    multiline
                    rows={3}
                    value={desc[i].content}
                    onChange={(e) => handleChange(e, i, "content")}
                  />
                </div>
              </Grid>

              <Grid item>
                <IconButton onClick={() => handleRemoveDesc(i)}>
                  <DeleteOutlined />
                </IconButton>
              </Grid>
            </Grid>
          ))}
      </div>

      <Button variant="text" color="secondary" onClick={handleAddDesc}>
        Thêm mô tả
      </Button>

      <Button variant="contained" color="secondary" onClick={handleSave}>
        Lưu
      </Button>
    </div>
  );
}

const UPDATE_DESC_MUTATION = gql`
  mutation ($new_desc: [UpdateDescriptionInput!]!) {
    result: update_description(new_description: $new_desc) {
      description {
        title
        content
      }
    }
  }
`;
