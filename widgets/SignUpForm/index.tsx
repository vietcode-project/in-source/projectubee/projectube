import React, { useEffect, useMemo, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { gql, useMutation } from "@apollo/client";
import {
  Button,
  TextField,
  FormHelperText,
  makeStyles,
  InputAdornment,
  Typography,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import CheckIcon from "@material-ui/icons/Check";

import {
  validateName,
  validateEmail,
  validatePhoneNumber,
  validatePassword,
} from "utils/validators";

const useStyles = makeStyles((theme) => ({
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
  },
  submit: {
    margin: theme.spacing(2, 0),
    borderRadius: "6px",
  },
  tick: {
    fontSize: "15px",
    color: "#532BDC",
  },
  label: {
    marginBottom: "8px",
    marginTop: "16px",
  },
  alert: {
    animation: `$popup 500ms`,
  },
  "@global": {
    "@keyframes popup": {
      from: {
        transform: "translateY(-10px)",
      },
      to: {
        transform: "translateY(30)",
      },
    },
  },
}));

interface IUserData {
  name: string;
  type: string;
  email: string;
  password: string;
  phone_number: string;
}

interface IProps {
  userData: IUserData;
  setUserData: (IUserData) => void;
}

export default function SignUpForm(props: IProps) {
  const classes = useStyles();
  const router = useRouter();
  const { userData, setUserData } = props;
  const [signup_, { data, loading, error }] = useMutation(SIGNUP_MUTATION, {
    errorPolicy: "all",
  });

  const [retypePassword, setRetypePass] = useState("");
  const [typed, setTyped] = useState({
    name: false,
    email: false,
    password: false,
    phone_number: false,
    retypePassword: false,
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setUserData((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    setTyped((prev) => ({ ...prev, [name]: true }));
  };

  const valid = useMemo(() => {
    const name = validateName(userData.name);
    const email = validateEmail(userData.email);
    const password = validatePassword(userData.password);
    const phone_number = validatePhoneNumber(userData.phone_number);
    const retypePassword_ = userData.password === retypePassword;

    return {
      name,
      email,
      password,
      phone_number,
      retypePassword: retypePassword_,
    };
  }, [userData, retypePassword]);

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    await signup_({
      variables: userData,
    });
  };

  useEffect(() => {
    if (data) {
      router.push(`/signup/verify?email=${userData.email}`);
    }
    if (error) {
      console.log(error.message);
    }
  }, [data, error]);

  return (
    <>
      <Typography variant="h2" style={{ marginTop: "16px" }}>
        Đăng ký
      </Typography>

      <form className={classes.form} onSubmit={handleSubmit}>
        <Typography variant="h5" className={classes.label}>
          {userData.type === "org" ? "Tên tổ chức" : "Họ và tên"}
        </Typography>
        <TextField
          id="outlined-required"
          required={true}
          variant="outlined"
          placeholder={
            userData.type === "org"
              ? "VD: Vietcode, Developh, ..."
              : "VD: Nguyen Van A,..."
          }
          name="name"
          error={!valid.name && typed.name}
          value={userData.name}
          onChange={handleChange}
          InputProps={
            valid.name && {
              endAdornment: (
                <InputAdornment position="start">
                  <CheckIcon className={classes.tick} />
                </InputAdornment>
              ),
            }
          }
          fullWidth
        />
        <Typography variant="h5" className={classes.label}>
          Số điện thoại
        </Typography>
        <TextField
          id="outlined-required"
          name="phone_number"
          variant="outlined"
          placeholder="VD: 0123 456 789,..."
          required={true}
          fullWidth
          value={userData.phone_number}
          error={!valid.phone_number && typed.phone_number}
          InputProps={
            valid.phone_number && {
              endAdornment: (
                <InputAdornment position="start">
                  <CheckIcon className={classes.tick} />
                </InputAdornment>
              ),
            }
          }
          onChange={handleChange}
        />
        <Typography variant="h5" className={classes.label}>
          Email
        </Typography>
        <TextField
          id="outlined-required"
          name="email"
          variant="outlined"
          placeholder={
            userData.type === "org"
              ? "VD: director@vietcode.org..."
              : "VD: abcde@xyz.com,..."
          }
          fullWidth
          required={true}
          error={!valid.email && typed.email}
          value={userData.email}
          onChange={handleChange}
          InputProps={
            valid.email && {
              endAdornment: (
                <InputAdornment position="start">
                  <CheckIcon className={classes.tick} />
                </InputAdornment>
              ),
            }
          }
        />
        <Typography variant="h5" className={classes.label}>
          Mật khẩu
        </Typography>
        <TextField
          id="outlined-required"
          name="password"
          variant="outlined"
          placeholder="Nhập mật khẩu mới"
          type="password"
          value={userData.password}
          onChange={handleChange}
          required={true}
          fullWidth
          error={!valid.password && typed.password}
        />
        <FormHelperText style={{ marginTop: "8px" }}>
          {!valid.password && "Mật khẩu tối thiểu 6 ký tự"}
        </FormHelperText>
        <Typography variant="h5" className={classes.label}>
          Nhập lại mật khẩu
        </Typography>
        <TextField
          id="outlined-required"
          type="password"
          variant="outlined"
          placeholder="Nhập lại khẩu"
          required={true}
          fullWidth
          onChange={(e) => {
            setRetypePass(e.target.value);
            setTyped((prev) => ({ ...prev, retypePassword: true }));
          }}
          error={!valid.retypePassword && typed.retypePassword}
          InputProps={
            valid.password &&
            valid.retypePassword && {
              endAdornment: (
                <InputAdornment position="start">
                  <CheckIcon className={classes.tick} />
                </InputAdornment>
              ),
            }
          }
        />
        <FormHelperText style={{ marginTop: "8px" }}>
          {!valid.retypePassword && "Mật khẩu không khớp với mật khẩu đã tạo"}
        </FormHelperText>

        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          disabled={
            !Object.values(valid).reduce((acc, cur) => acc && cur, true) ||
            loading ||
            data
          }
          size="large"
        >
          {loading || data ? "Loading please wait" : "Tiếp tục"}
        </Button>
      </form>
      {error &&
        (error.graphQLErrors[0].message === "User already exists" ||
          error.message.includes("duplicate key value")) && (
          <div style={{ position: "fixed", top: 30, right: 30 }}>
            <Alert severity="error" className={classes.alert}>
              Tài khoản gmail của bạn đã tồn tại
            </Alert>
          </div>
        )}
    </>
  );
}

const SIGNUP_MUTATION = gql`
  mutation (
    $email: String!
    $password: String!
    $name: String!
    $phone_number: String!
    $type: String!
  ) {
    register(
      data: {
        name: $name
        email: $email
        password: $password
        phone_number: $phone_number
      }
      type: $type
    ) {
      email
    }
  }
`;
