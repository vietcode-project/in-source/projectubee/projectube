import { gql, NetworkStatus, useQuery } from "@apollo/client";
import React, { useEffect, useReducer } from "react";
import {
  initialState,
  AuthReducer,
  AuthStateType,
  ActionType,
} from "./reducer";

import { DetailedData as UDetData } from "../../graphql/UserFragment";
import { DetailedData as ODetData } from "../../graphql/OrgFragment";

const AuthStateContext = React.createContext<AuthStateType>(initialState);
const AuthDispatchContext = React.createContext<React.Dispatch<ActionType>>(
  () => {}
);

export function useAuthState() {
  const context = React.useContext(AuthStateContext);
  if (context === undefined) {
    throw new Error("useAuthState must be used within a AuthProvider");
  }

  return context;
}

export function useAuthDispatch(): React.Dispatch<ActionType> {
  const context = React.useContext(AuthDispatchContext);
  if (context === undefined) {
    throw new Error("useAuthDispatch must be used within a AuthProvider");
  }

  return context;
}

export const AuthProvider = ({ children }) => {
  const { data, error, loading, refetch, networkStatus } = useQuery(ME_QUERY, {
    notifyOnNetworkStatusChange: true,
  });
  const [authState, dispatch] = useReducer(AuthReducer, {
    ...initialState,
    refetch,
  });

  useEffect(() => {
    const key = setInterval(() => {
      refetch();
    }, 1000 * 60 * 5);

    return () => {
      clearInterval(key);
    };
  }, []);

  useEffect(() => {
    if (!loading) {
      dispatch({ type: "UNSET_LOADING" });
      if (networkStatus !== NetworkStatus.refetch && data) {
        dispatch({
          type: "SET_USER",
          payload: data.me,
        });
      }
    }
  }, [loading, networkStatus, data]);

  return (
    <AuthStateContext.Provider value={authState}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  );
};

const ME_QUERY = gql`
  query {
    me {
      ...UserDetailedFragment
      ...OrgDetailedFragment
    }
  }
  ${UDetData()}
  ${ODetData()}
`;
