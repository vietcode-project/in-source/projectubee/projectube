import IUser from "../../types/User";
import IOrg from "../../types/Org";
import { ObservableQuery } from "@apollo/client";

export type AuthStateType<
  TUser extends IUser = IUser,
  TOrg extends IOrg = IOrg
> = {
  // user: TUser | TOrg | null;
  user: any;
  loading: boolean;
  errorMessage: string;
  refetch: () => Promise<any>;
};

export type ActionType = {
  type: string;
  payload?: AuthStateType["user"];
  error?: string;
};

export const initialState: AuthStateType = {
  user: null,
  loading: true,
  errorMessage: "",
  refetch: async () => {},
};

export const AuthReducer = (
  state: AuthStateType,
  action: ActionType
): AuthStateType => {
  switch (action.type) {
    case "SET_LOADING":
      return { ...state, loading: true };
    case "UNSET_LOADING":
      return { ...state, loading: false };
    case "SET_USER":
      return {
        ...state,
        user: action.payload,
        errorMessage: "",
      };
    case "UNSET_USER":
      return {
        ...state,
        user: null,
      };
    default:
      return state;
  }
};
