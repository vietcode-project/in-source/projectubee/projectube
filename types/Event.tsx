import IOrg from "./Org";
import IUser from "./User";

export default interface IEvent {
  id: string;
  name: string;
  avatarUrl?: string;
  coverUrl?: string;
  description?: string;
  start_time?: Date;
  location: string;
  organizer: IOrg;
  subscribers: IUser[];
  categories: string[];
  register_link?: string;
}

export type IEventCard = Pick<
  IEvent,
  | "id"
  | "name"
  | "description"
  | "start_time"
  | "location"
  | "organizer"
  | "subscribers"
  | "categories"
>;
