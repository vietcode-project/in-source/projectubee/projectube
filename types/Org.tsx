import IEvent from "./Event";
import IUser from "./User";
import IAssessment from "./Assessment";
import IRecruitment from "./Recruitment";

export default interface IOrg {
  id: string;
  name: string;
  avatarUrl: string;
  coverUrl: string;
  categories: string[];
  members?: Omit<IUser, "email">[];
  type: string;
  description: string;
  contact: {
    email?: string;
    phone_number?: string;
    location?: string;
    website?: string;
    facebook_url: string;
  };
  events: IEvent[];
  subscribers: IUser[];
  assessments: IAssessment[];
  recruitments: IRecruitment[];
}

export type IOrgCard = Pick<
  IOrg,
  | "id"
  | "name"
  | "avatarUrl"
  | "categories"
  | "description"
  | "events"
  | "subscribers"
>;
