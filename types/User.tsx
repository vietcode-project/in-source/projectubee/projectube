import IOrg from "./Org";
import IEvent from "./Event";

export default interface IUser {
  id: string;
  name: string;
  email?: string;
  interests: string[];
  avatarUrl?: string;
  phone_number: string;
  subscribing_events: IEvent[];
  subscribing_orgs: IOrg[];
  type: string;
}
