import IOrg from "./Org";

export default interface IRecruitment {
  id: string;
  title: number;
  deadline: Date;
  cover_url: string;
  positions: string[];
  screen_questions: string[];
  job_description: string;
  location: string;
  isOpen: boolean;
  org: IOrg;
}
