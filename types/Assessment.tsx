import IUser from "./User";

export default interface IAssesment {
  id: string;
  rate: number;
  comment: string;
  user: IUser;
}
