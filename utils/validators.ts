export function validateName(name: string): boolean {
  return name.length > 4;
}

export function validateEmail(email: string): boolean {
  const pattern = /^[a-zA-Z][a-zA-Z0-9\._\-\+]+@[a-z0-9]+\.[a-z]{1,4}/;
  return pattern.test(email);
}

export function validatePhoneNumber(phoneNumber: string): boolean {
  const pattern = /^(0|\+)[0-9]{9,12}/;
  return pattern.test(phoneNumber);
}

export function validatePassword(password: string): boolean {
  return password.length >= 6;
}
