export function averageArr(arr: number[]) {
  const sum = arr.reduce((a, c) => a + c);

  return Number((sum / arr.length).toFixed(2));
}

export function averageParam(objArray: Array<Object>, param: string) {
  if (!objArray) return 0;

  const filterArr = objArray.map((e) => e[param]);

  return averageArr(filterArr);
}
