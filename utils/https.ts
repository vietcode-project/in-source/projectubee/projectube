function validateURL(link) {
  if (link.indexOf("http://") == 0 || link.indexOf("https://") == 0) {
    return true;
  } else {
    return false;
  }
}

function httpsautoadd(link) {
  if (validateURL(link)) return link;
  else {
    return "https://" + link;
  }
}

export { validateURL, httpsautoadd };
