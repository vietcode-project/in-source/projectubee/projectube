import { Typography } from "@material-ui/core";
import React from "react";

const URL_REGEX =
  /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;

export const renderText = (txt) =>
  txt.split(" ").map((part, i) =>
    URL_REGEX.test(part) ? (
      <a key={part + i} href={part}>
        <Typography
          style={{ color: "#532BDC", textDecoration: "underline" }}
          display="inline"
        >
          {part}{" "}
        </Typography>
      </a>
    ) : (
      part + " "
    )
  );
