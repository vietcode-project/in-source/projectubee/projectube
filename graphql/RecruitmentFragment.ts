import { gql } from "@apollo/client";
import { DetailedData as OrgDetailData } from "./OrgFragment";

export const EssentialData = () => gql`
  fragment RecruitmentEssentialFragment on Recruitment {
    id
    isOpen
    title
    job_description
    deadline
    cover_url
    location
  }
`;

export const DetailedData = () => gql`
  fragment RecruitmentDetailedFragment on Recruitment {
    positions
    job_description
    ...RecruitmentEssentialFragment
    org {
      ...OrgDetailedFragment
    }
  }
  ${OrgDetailData()}
`;
