import { gql } from "@apollo/client";
import { DetailedData as UserDetailedData } from "./UserFragment";
import { DetailedData as RecruitDetailedData } from "./RecruitmentFragment";

export const ApplicationFragment = () => gql`
  fragment ApplicationFragment on Application {
    id
    screen_answers
    user {
      ...UserDetailedFragment
    }
    recruitment {
      ...RecruitmentDetailedFragment
    }
  }
  ${UserDetailedData()}
  ${RecruitDetailedData()}
`;
