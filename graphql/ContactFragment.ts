import { gql } from "@apollo/client";

export const ContactData = () => gql`
  fragment ContactFragment on Contact {
    phone_number
    email
    website
    location
    facebook_url
    instagram_url
  }
`;
