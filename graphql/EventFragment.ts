import { gql } from "@apollo/client";
import { EssentialData as OEssData } from "./OrgFragment";
import { EssentialData as UEssData } from "./UserFragment";
import { AssessmentData } from "./AssessmentFragment";

export const EssentialData = () => gql`
  fragment EventEssentialFragment on Event {
    id
    name
    categories
    description
    start_time
    coverUrl
    location
    register_link
  }
`;

export const DetailedData = () => gql`
  fragment EventDetailedFragment on Event {
    subscribers {
      ...UserEssentialFragment
    }
    attendees {
      ...UserEssentialFragment
    }
    organizer {
      ...OrgEssentialFragment
    }
    assessments {
      ...AssessmentFragment
    }
  }
  ${UEssData()}
  ${OEssData()}
  ${AssessmentData()}
`;
