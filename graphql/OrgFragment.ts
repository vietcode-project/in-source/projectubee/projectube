import { gql } from "@apollo/client";
import { AssessmentData } from "./AssessmentFragment";
import { ContactData } from "./ContactFragment";
import { EssentialData as EEssData } from "./EventFragment";
import { EssentialData as RecruitEssData } from "./RecruitmentFragment";
import {
  EssentialData as UEssData,
  DetailedData as UDetData,
} from "./UserFragment";

export const EssentialData = () => gql`
  fragment OrgEssentialFragment on Org {
    id
    email
    name
    categories
    description
    avatarUrl
    coverUrl
    type
    contact {
      ...ContactFragment
    }
  }
  ${ContactData()}
`;

export const DetailedData = () => gql`
  fragment OrgDetailedFragment on Org {
    ...OrgEssentialFragment
    members {
      ...UserEssentialFragment
    }
    subscribers {
      ...UserEssentialFragment
    }
    events {
      ...EventEssentialFragment
    }
    assessments {
      ...AssessmentFragment
    }
    recruitments {
      ...RecruitmentEssentialFragment
    }
  }
  ${UEssData()}
  ${UDetData()}
  ${EEssData()}
  ${AssessmentData()}
  ${RecruitEssData()}
`;
