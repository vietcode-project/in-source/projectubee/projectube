import { gql } from "@apollo/client";
import { DetailedData as UDetData } from "./UserFragment";

export const AssessmentData = () => gql`
  fragment AssessmentFragment on Assessment {
    id
    rate
    comment
    user {
      ...UserDetailedFragment
    }
  }
  ${UDetData()}
`;
