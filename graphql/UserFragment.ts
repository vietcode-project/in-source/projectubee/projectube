import { gql } from "@apollo/client";
import { EssentialData as EEssData } from "./EventFragment";
import { EssentialData as OEssData } from "./OrgFragment";

export const EssentialData = () => gql`
  fragment UserEssentialFragment on User {
    id
    name
    email
    phone_number
    avatarUrl
    coverUrl
    interests
    type
    dob
  }
`;

export const DetailedData = () => gql`
  fragment UserDetailedFragment on User {
    ...UserEssentialFragment
    member_of {
      ...OrgEssentialFragment
    }
    subscribing_orgs {
      ...OrgEssentialFragment
    }
    subscribing_events {
      ...EventEssentialFragment
    }
    attend_events {
      ...EventEssentialFragment
    }
    followers {
      ...UserEssentialFragment
    }
    followings {
      ...UserEssentialFragment
    }
    related_org_by_categories {
      ...OrgEssentialFragment
    }
    related_event_by_categories {
      ...EventEssentialFragment
    }
  }
  ${EssentialData()}
  ${OEssData()}
  ${EEssData()}
`;
