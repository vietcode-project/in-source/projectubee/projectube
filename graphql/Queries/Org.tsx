import { gql } from "@apollo/client";

export const ORG_QUERY = gql`
  query ($id: String!) {
    org(id: $id) {
      id
      name
      avatarUrl
      coverUrl
      description
      type
      categories
      contact {
        email
        phone_number
        website
        facebook_url
      }
      members {
        name
        avatarUrl
      }
      events {
        id
        name
        start_time
        categories
        location
        coverUrl
        subscribers {
          avatarUrl
        }
        organizer {
          id
          name
        }
      }
      recruitments {
        id
        title
        job_description
        deadline
        location
        isOpen
        cover_url
      }
      assessments {
        id
        rate
        comment
        user {
          id
          avatarUrl
          name
        }
      }
    }
  }
`;
