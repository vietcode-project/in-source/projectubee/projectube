import { gql } from "@apollo/client";
import { DetailedData as UDetData } from "graphql/UserFragment";
import { DetailedData as ODetData } from "graphql/OrgFragment";

export const ME_QUERY = gql`
  query {
    me {
      ...UserDetailedFragment
      ...OrgDetailedFragment
    }
  }
  ${UDetData()}
  ${ODetData()}
`;
