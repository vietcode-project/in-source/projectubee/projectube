import React from "react";

interface Props {}

export default function withLove<P extends object>(
  Comp: React.ComponentType<P>
): (props: Props) => JSX.Element {
  return (props: Props) => {
    return (
      <>
        <h1>With love</h1>
        <Comp {...(props as P)} />
      </>
    );
  };
}
