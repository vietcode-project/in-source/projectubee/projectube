import React, { useEffect, useState } from "react";
import Image from "next/image";
import {
  Grid,
  AppBar,
  Toolbar,
  Button,
  useTheme,
  Container,
  Typography,
  makeStyles,
  useMediaQuery,
  Theme,
  IconButton,
  Menu,
  MenuItem,
  Fade,
} from "@material-ui/core";
import Link from "next/link";
import { useRouter } from "next/router";

const useStyle = makeStyles((theme) => ({
  navitem: {
    marginLeft: "16px",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
}));

export default function Navbar() {
  const theme = useTheme();
  const router = useRouter();
  const classes = useStyle();
  const [showSignIn, setShowSignIn] = useState(true);

  useEffect(() => {
    if (router.pathname == "/") {
      setShowSignIn(false);
      const onScroll = (e) => {
        {
          e.target.documentElement.scrollTop >= 350
            ? setShowSignIn(true)
            : setShowSignIn(false);
        }
      };
      window.addEventListener("scroll", onScroll);
      return () => window.removeEventListener("scroll", onScroll);
    }
  }, []);

  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down("xs")
  );

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  if (isDevicePhone) {
    return (
      <AppBar
        position="fixed"
        style={{
          zIndex: theme.zIndex.drawer + 1,
          color: "black",
          padding: 0,
          boxShadow: "none",
          background: "white",
        }}
      >
        <Toolbar disableGutters>
          <Container maxWidth="lg">
            <Grid container justify="space-between" alignItems="center">
              <Grid item>
                <Grid container direction="row" alignItems="center">
                  <Grid item>
                    <Link href="/">
                      <a>
                        <Image src="/Logo.svg" width={140} height={22} />
                      </a>
                    </Link>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid container spacing={2}>
                  <IconButton
                    edge="end"
                    className={classes.menuButton}
                    color="inherit"
                    aria-label="menu"
                    onClick={handleClick}
                  >
                    <Image src="/MenuIcon.svg" layout="fill" />
                  </IconButton>
                  <Menu
                    id="fade-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={open}
                    onClose={handleClose}
                    TransitionComponent={Fade}
                  >
                    <MenuItem>
                      <Link href="/orgs">
                        <a>
                          <Typography
                            variant="body1"
                            style={{
                              color:
                                router.pathname === "orgs"
                                  ? "#1B2431"
                                  : "#8D9198",
                            }}
                          >
                            Các tổ chức
                          </Typography>
                        </a>
                      </Link>
                    </MenuItem>
                    <MenuItem>
                      <Link href="/recruit">
                        <a>
                          <Typography variant="body1" color="textSecondary">
                            Tuyển dụng
                          </Typography>
                        </a>
                      </Link>
                    </MenuItem>
                    <MenuItem>
                      <Link href="/about">
                        <a>
                          <Typography variant="body1" color="textSecondary">
                            Về chúng tôi
                          </Typography>
                        </a>
                      </Link>
                    </MenuItem>
                  </Menu>
                </Grid>
              </Grid>
            </Grid>
          </Container>
        </Toolbar>
      </AppBar>
    );
  }

  return (
    <AppBar
      position="fixed"
      style={{
        zIndex: theme.zIndex.drawer + 1,
        color: "black",
        padding: 0,
        boxShadow: "none",
        background: "white",
      }}
    >
      <Toolbar disableGutters>
        <Container maxWidth="lg">
          <Grid container justify="space-between" alignItems="center">
            <Grid item>
              <Grid container direction="row" alignItems="center">
                <Grid item>
                  <Link href="/">
                    <a>
                      <Image src="/Logo.svg" width={140} height={22} />
                    </a>
                  </Link>
                </Grid>

                <Grid item>
                  <Grid container spacing={2}>
                    <Grid item className={classes.navitem}>
                      <svg
                        width="2"
                        height="16"
                        viewBox="0 0 2 16"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path d="M1 0V16" stroke="#DEE4ED" />
                      </svg>
                    </Grid>
                    <Grid item style={{ marginLeft: 8 }}>
                      <Link href="/orgs">
                        <a>
                          <Typography
                            variant="body1"
                            style={{
                              color:
                                router.pathname === "/orgs/[org_id]" ||
                                router.pathname === "/orgs"
                                  ? "#1B2431"
                                  : "#8D9198",
                            }}
                          >
                            Các tổ chức
                          </Typography>
                        </a>
                      </Link>
                    </Grid>
                    <Grid item className={classes.navitem}>
                      <Link href="/recruit">
                        <a>
                          <Typography
                            variant="body1"
                            style={{
                              color:
                                router.pathname === "/recruit/[recruit_id]" ||
                                router.pathname === "/recruit"
                                  ? "#1B2431"
                                  : "#8D9198",
                            }}
                          >
                            Tuyển dụng
                          </Typography>
                        </a>
                      </Link>
                    </Grid>
                    <Grid item className={classes.navitem}>
                      <Link href="/about">
                        <a>
                          <Typography
                            variant="body1"
                            style={{
                              color:
                                router.pathname === "/about"
                                  ? "#1B2431"
                                  : "#8D9198",
                            }}
                          >
                            Về chúng tôi
                          </Typography>
                        </a>
                      </Link>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid container spacing={2}>
                {showSignIn && (
                  <>
                    <Grid item>
                      <Link href="/signup">
                        <a>
                          <Button variant="outlined" color="primary">
                            Đăng ký
                          </Button>
                        </a>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/signin">
                        <a>
                          <Button variant="contained" color="primary">
                            Đăng nhập
                          </Button>
                        </a>
                      </Link>
                    </Grid>
                  </>
                )}
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </Toolbar>
    </AppBar>
  );
}
