import React, { ReactElement } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import {
  AppBar,
  Container,
  Grid,
  makeStyles,
  Toolbar,
  Theme,
  useMediaQuery,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "11% 17%",
  },
  content: { flexGrow: 1 },
  navbarSpacer: theme.mixins.toolbar,
}));

export default function Auth({
  children,
}: React.PropsWithChildren<{}>): ReactElement {
  const classes = useStyles();
  const router = useRouter();

  return (
    <>
      <Navbar />
      <div className={classes.root}>
        <div className={classes.navbarSpacer} />
        <main className={classes.content}>{children}</main>
      </div>
    </>
  );
}

function Navbar() {
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down("xs")
  );

  return (
    <AppBar
      color="transparent"
      position="fixed"
      style={{ boxShadow: "none", width: "30%", left: "10%" }}
    >
      <Toolbar disableGutters>
        <Container maxWidth="lg">
          <Grid container alignItems="center">
            <Grid item>
              <Grid container alignItems="center">
                <Link href="/">
                  <a>
                    <Image
                      src={isDevicePhone ? "/Logo.svg" : "/Logo_Light.svg"}
                      width={140}
                      height={22}
                    />
                  </a>
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </Toolbar>
    </AppBar>
  );
}
