import React, { ReactElement } from "react";
import { makeStyles, Toolbar } from "@material-ui/core";

import Navbar from "./components/Navbar";
import { useRouter } from "next/router";

interface Props extends React.PropsWithChildren<{}> {}

const useStyles = makeStyles((theme) => ({
  root: { display: "flex" },
  content: { flexGrow: 1 },
  sidebar: { flexShrink: 0 },
  navbarSpacer: theme.mixins.toolbar,
}));

export default function User({ children }: Props): ReactElement {
  const classes = useStyles();
  const router = useRouter();

  return (
    <>
      <Navbar />
      <div className={classes.root}>
        <main className={classes.content}>
          <Toolbar />
          {children}
          <Toolbar
            style={{ display: `${router.pathname == "/about" ? "none" : ""}` }}
          />
        </main>
      </div>
    </>
  );
}
