import React, { ReactElement, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { gql, useQuery } from "@apollo/client";
import {
  Button,
  Container,
  Divider,
  Grid,
  makeStyles,
  Typography,
  useTheme,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import withLayout from "hocs/withLayout";
import { useAuthState } from "contexts/auth/index";
import Loading from "components/Loading";
import RateCard from "components/AssessmentCard/RateCard";
import DialogAssess from "components/AssessmentCard/DialogAssess";
import CommentCard from "components/AssessmentCard/CommentCard";
import { SmallOrgCard } from "components/orgCard";
import { renderText } from "utils/renderTextwithLink";
import SideSmallEventCard from "components/eventCard/SideSmall";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(6),
  },
  cover: {
    borderRadius: 8,
  },
}));

function index(): ReactElement {
  const theme = useTheme();
  const classes = useStyles();
  const router = useRouter();
  const { user, loading: loadingUser } = useAuthState();
  const [rated, setRated] = useState<boolean>(false);
  const [OpenAssess, setOpenAssess] = useState(false);

  const { data, loading, error } = useQuery(EVENT_QUERY, {
    variables: {
      id: router.query.event_id,
    },
  });

  useEffect(() => {
    if (
      data &&
      data?.event?.assessments?.some((ass) => ass.user.id === user?.id)
    ) {
      setRated(true);
    }
  }, [data, user]);

  if (loading || loadingUser) return <Loading />;

  return (
    <div className={classes.root}>
      <Container>
        <Grid container alignItems="center">
          <Button
            onClick={() => router.back()}
            variant="outlined"
            startIcon={<ArrowBackIcon />}
          >
            Quay lại
          </Button>
          <Typography
            display="inline"
            variant="h3"
            style={{ marginLeft: theme.spacing(2) }}
          >
            Sự kiện
          </Typography>
        </Grid>

        <Grid
          container
          spacing={3}
          style={{
            marginTop: theme.spacing(3),
            marginBottom: theme.spacing(5),
            position: "relative",
          }}
        >
          <Grid item xs={12} md={8}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <div
                  className={classes.cover}
                  style={{
                    width: "100%",
                    paddingTop: "60%",
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                    backgroundRepeat: "no-repeat",
                    backgroundImage: `url(${
                      data && data?.event?.coverUrl
                        ? data?.event?.coverUrl
                        : "/temp-cover.jpg"
                    })`,
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography variant="h3" gutterBottom>
                  Thông tin sự kiện
                </Typography>

                {data &&
                  data?.event?.description.split("\n").map((d) => (
                    <Typography
                      variant="body1"
                      color="textSecondary"
                      gutterBottom
                      key={d}
                    >
                      {renderText(d)}
                    </Typography>
                  ))}

                <Grid
                  container
                  style={{
                    marginTop: theme.spacing(5),
                    marginBottom: theme.spacing(1),
                  }}
                >
                  <Typography variant="h3" gutterBottom>
                    Nhận xét
                  </Typography>
                  <br />
                  {data &&
                    data?.event?.assessments?.length > 0 &&
                    data?.event?.assessments?.map((assessment) => (
                      <CommentCard
                        key={assessment.id}
                        data={assessment}
                        type={data.event.__typename}
                        iconState={user && assessment.user.id === user.id}
                      />
                    ))}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid
            item
            xs={12}
            md={4}
            style={{
              position: "sticky",
              top: 88,
            }}
          >
            <SideSmallEventCard data={data && data.event} user={user} />
            <SmallOrgCard org={data?.event?.organizer} />
            <RateCard
              user={user}
              openAssess={() => setOpenAssess(true)}
              assessments={data && data?.event?.assessments}
              rated={rated}
            />
            <DialogAssess
              name={data?.event?.name}
              openAssess={OpenAssess}
              closeAssess={() => setOpenAssess(false)}
              type={data && data?.event?.__typename}
              id={data && data?.event?.id}
              initialRate={0}
              initialComment=""
            />
          </Grid>
        </Grid>

        <Divider />

        <div style={{ marginTop: theme.spacing(5) }}>
          <Typography variant="h3" color="textPrimary" gutterBottom>
            Các sự kiện khác
          </Typography>
        </div>
      </Container>
    </div>
  );
}

const EVENT_QUERY = gql`
  query ($id: String!) {
    event(id: $id) {
      id
      name
      coverUrl
      start_time
      location
      description
      categories
      register_link
      subscribers {
        avatarUrl
      }
      organizer {
        id
        name
        avatarUrl
        description
        categories
        subscribers {
          id
          avatarUrl
        }
      }
      assessments {
        id
        rate
        comment
        user {
          name
          avatarUrl
          id
        }
      }
    }
  }
`;

export default withLayout(index);
