import React, { ReactElement, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";

import {
  Button,
  Container,
  Grid,
  Menu,
  Select,
  MenuItem,
  Typography,
  useTheme,
  InputBase,
  IconButton,
  useMediaQuery,
  Theme,
} from "@material-ui/core";

import { gql, useQuery } from "@apollo/client";
import { Alert } from "@material-ui/lab";
import Loading from "components/Loading";
import { IOrgCard } from "types/Org";
import { DetailedData, EssentialData } from "graphql/OrgFragment";
import withLayout from "hocs/withLayout";
import { LargeOrgCard } from "components/orgCard";
import CateList from "../../components/Catelist";
import { makeStyles } from "@material-ui/styles";
import { useAuthState } from "contexts/auth";
import cates from "config/categories.json";
import Search from "widgets/Search";

const useStyle = makeStyles(() => ({
  root: { width: 784 },
  adddelbtn: {
    border: "1px solid #DEE4ED",
    borderRadius: 4,
    height: 24,
    width: 24,
    padding: 8,
  },
}));

const PAGI_LIMIT = 16;

interface Ifilter {
  event_number?: number;
  type?: string;
  categories: string[];
}

function Orgs(): ReactElement {
  const theme = useTheme();
  const classes = useStyle();
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );
  const { user } = useAuthState();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [raworgs, setRaworgs] = useState<IOrgCard[]>([]);
  const [orgs, setOrgs] = useState<IOrgCard[]>([]);
  const [noMoreOrg, setNoMoreOrg] = useState(false);
  const [filter, setFilter] = useState<Ifilter>({
    categories: [],
    event_number: 0,
    type: "",
  });

  const { data, loading, error, fetchMore } = useQuery<{ orgs: IOrgCard[] }>(
    ORGS_QUERY,
    {
      variables: {
        offset: 0,
      },
    }
  );

  useEffect(() => {
    if (!loading && data && data.orgs && data.orgs.length) {
      setOrgs(filterOrg(raworgs));
    }
  }, [filter]);

  useEffect(() => {
    if (!loading && data && data.orgs && data.orgs.length && !orgs.length) {
      setRaworgs(data.orgs);
      setOrgs(data.orgs);
    }
  }, [data, loading]);

  useEffect(() => {
    if (filter.categories.length || filter.event_number > 0) {
      setOrgs(filterOrg(raworgs));
    } else {
      setOrgs(raworgs);
    }
  }, [raworgs]);

  const loadMore = () => {
    fetchMore({
      variables: { offset: raworgs.length },
    }).then(({ data }) => {
      if (data && data.orgs.length === 0) {
        setNoMoreOrg(true);
        setTimeout(() => {
          setNoMoreOrg(false);
        }, 5000);
      }
      setRaworgs((prev) => [...prev, ...data.orgs]);
    });
  };

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  function handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setFilter((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  }

  function changeeventnumber(type) {
    if (type == "+") {
      setFilter((prev) => ({
        ...prev,
        event_number: Number(prev.event_number) + 1,
      }));
    }
    if (type == "-") {
      if (filter.event_number > 0) {
        setFilter((prev) => ({
          ...prev,
          event_number: Number(prev.event_number) - 1,
        }));
      }
    }
  }

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleRemoveFilter = () => {
    setFilter({ categories: [], event_number: 0, type: "" });
  };

  function filterOrg(obj: IOrgCard[]) {
    if (filter.categories.length <= 0) {
      return obj.filter(({ events }) => events.length >= filter.event_number);
    }
    return obj
      .filter(({ categories }) =>
        filter.categories.every((e) => categories.includes(e))
      )
      .filter(({ events }) => events.length >= filter.event_number);
  }

  if (loading) return <Loading />;

  if (error) {
    return <div>Errorrrrr :(</div>;
  }

  const dropdown = (
    <Menu
      id="simple-menu"
      anchorEl={anchorEl}
      getContentAnchorEl={null}
      open={Boolean(anchorEl)}
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      onClose={handleClose}
    >
      <Grid
        className={classes.root}
        container
        style={{ padding: "24px 32px 24px 24px", maxWidth: 784, width: "100%" }}
      >
        <Grid container spacing={4} direction="row">
          <Grid item xs={6}>
            <Typography style={{ marginBottom: 16 }} variant="h5">
              Lọc theo loại tổ chức
            </Typography>
            <Select
              fullWidth
              MenuProps={{
                getContentAnchorEl: null,
                anchorOrigin: {
                  vertical: "bottom",
                  horizontal: "left",
                },
              }}
              variant="outlined"
            >
              <MenuItem value={1}>Tổ chức lợi nhuận</MenuItem>
              <MenuItem value={2}>Tổ chức phi lợi nhuận</MenuItem>
            </Select>
          </Grid>
          <Grid item xs={6}>
            <Grid>
              <Typography style={{ marginBottom: 16 }} variant="h5">
                Lọc theo số sự kiện ít nhất
              </Typography>
            </Grid>
            <Grid
              container
              style={{
                display: "flex",
                alignItems: "center",
                width: "100%",
                height: 57,
                border: " 1px solid #DEE4ED",
                borderRadius: 6,
                paddingLeft: 16,
                paddingRight: 16,
              }}
            >
              <Grid item xs={6}>
                <InputBase
                  type="number"
                  name="event_number"
                  value={filter.event_number}
                  onChange={handleInputChange}
                />
              </Grid>

              <Grid item xs={6}>
                <Grid
                  container
                  justify={isDevicePhone ? "space-around" : "flex-end"}
                >
                  <Grid item xs={isDevicePhone ? 12 : 4}>
                    <Grid
                      container
                      justify={isDevicePhone ? "center" : "flex-end"}
                    >
                      <Grid item xs={6}>
                        <IconButton
                          style={{ marginRight: isDevicePhone ? 0 : 8 }}
                          className={classes.adddelbtn}
                          onClick={() => changeeventnumber("+")}
                        >
                          <Image src="/Icon_add.svg" width={8} height={8} />
                        </IconButton>
                      </Grid>
                      <Grid item xs={6}>
                        <IconButton
                          className={classes.adddelbtn}
                          onClick={() => changeeventnumber("-")}
                        >
                          <Image src="/Icon_remove.svg" width={8} height={8} />
                        </IconButton>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid container direction="column" style={{ marginTop: 16 }}>
          <Typography variant="h5">Lọc theo chủ đề</Typography>
          {filter.categories.length <= 3 ? (
            <Typography
              variant="body2"
              color="textSecondary"
              style={{ marginBottom: 16 }}
            >
              Vui lòng chọn tối đa 3 chủ đề
            </Typography>
          ) : (
            <Typography
              variant="body2"
              color="error"
              style={{ marginBottom: 16 }}
            >
              Vui lòng chọn lại
            </Typography>
          )}

          <CateList
            cates={cates}
            selected={filter.categories}
            handleChoose={(label) => {
              if (filter.categories.includes(label)) {
                setFilter((prev) => ({
                  ...prev,
                  categories: prev.categories.filter((c) => c !== label),
                }));
              } else {
                setFilter((prev) => ({
                  ...prev,
                  categories: [...prev.categories, label],
                }));
              }
            }}
          />
        </Grid>
        <Button
          style={{ marginTop: 16, width: "100%" }}
          color="primary"
          variant="contained"
          onClick={handleRemoveFilter}
          disabled={filter.categories.length == 0 && filter.event_number == 0}
        >
          Xóa bộ lọc
        </Button>
      </Grid>
    </Menu>
  );
 console.log("A", user?.related_org_by_categories)
  return (
    <Container maxWidth="lg">
      <Grid
        container
        alignItems="center"
        justify="space-between"
        direction="row"
        style={{ marginTop: theme.spacing(2) }}
      >
        <Grid item>
          <Grid container alignItems="center">
            <Typography
              align="center"
              display="inline"
              style={{
                marginLeft: theme.spacing(1),
                fontSize: "19px",
                fontWeight: "bold",
              }}
            >
              Các tổ chức
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={5}>
          <Search queryorg={ORG_SEARCH_QUERY} />
        </Grid>
        <Grid item>
          <Button variant="outlined" onClick={handleClick} aria-haspopup="true">
            {isDevicePhone ? (
              <Image src="/Icon_filter.svg" width={20} height={30} />
            ) : (
              <Typography variant="button">Bộ lọc tổ chức</Typography>
            )}
          </Button>
        </Grid>
      </Grid>
      {dropdown}
      <div style={{ height: theme.spacing(5) }} />

      {!orgs.length ? (
        loading ? (
          <Loading />
        ) : raworgs.length ? (
          <>
            <Typography
              variant="h4"
              color="textSecondary"
              align="center"
              gutterBottom
            >
              Không có tổ chức phù hợp :(((
            </Typography>
            <OrgList data={orgs} loadMore={loadMore} type="all org" />
          </>
        ) : (
          <Typography variant="h3" gutterBottom>
            Error :(((
          </Typography>
        )
      ) : (
        <>
          {user && user.type === "User" && (
            <Grid container alignItems="center">
              <Typography
                align="center"
                display="inline"
                style={{
                  marginLeft: theme.spacing(1),
                  fontSize: "24px",
                  fontWeight: "bold",
                  marginBottom: "24px",
                }}>
                Các tổ chức thuộc lĩnh vực bạn yêu thích
              </Typography>
            </Grid>
          )}
         
          {user &&
            user.type === "User" &&
            (user?.related_org_by_categories.length > 0 ? (
              <OrgList
                data={user?.related_org_by_categories}
                loadMore={loadMore}
                type="love orgs"
              />
            ) : (
              <Typography
                style={{
                  marginLeft: theme.spacing(1),
                }}>
                Hãy yêu thích một chủ đề nào đó đi nào =)))
              </Typography>
            ))}
          {user &&
            user.type === "User" &&
            <Grid container alignItems="center">
            <Typography
              align="center"
              display="inline"
              style={{
                marginLeft: theme.spacing(1),
                fontSize: "24px",
                fontWeight: "bold",
                marginBottom: "24px",
                marginTop: "48px",
              }}>
              Tất cả tổ chức
            </Typography>
          </Grid>
          }
          <OrgList data={orgs} loadMore={loadMore} type="all orgs"/>
        </>
      )}
      {noMoreOrg && (
        <div style={{ position: "fixed", bottom: 30, left: 30 }}>
          <Alert severity="info">Hết tổ chức rồi :((</Alert>
        </div>
      )}
    </Container>
  );
}

function OrgList({
  data,
  loadMore,
  type
}: {
  data: IOrgCard[];
  loadMore: () => void;
  type:string
}) {
  const theme = useTheme();

  return (
    <>
      <Grid container spacing={2}>
        {data.map((org) => (
          <Grid item md={3} sm={6} xs={12} key={org.id}>
            <LargeOrgCard {...org} />
          </Grid>
        ))}
      </Grid>

      {
        type==="all orgs" && <div
        style={{ width: 300, margin: "0 auto", marginTop: theme.spacing(4) }}
      >
        <Button variant="outlined" color="primary" fullWidth onClick={loadMore}>
          Xem thêm
        </Button>
      </div>
      }
    </>
  );
}

export default withLayout(Orgs);

const ORGS_QUERY = gql`
  query($offset: Float!) {
    orgs(
      offset: $offset,
      limit: ${PAGI_LIMIT}
    ) {
      ...OrgEssentialFragment,
      ...OrgDetailedFragment,
    }
  }
  ${DetailedData()}
  ${EssentialData()}
`;

const ORG_SEARCH_QUERY = gql`
  query ($filterOrg: OrgFilterInput) {
    orgs(filter: $filterOrg) {
      id
      name
      avatarUrl
      categories
    }
  }
`;
