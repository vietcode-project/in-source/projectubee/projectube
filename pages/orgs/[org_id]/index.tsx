import React, { ReactElement, useState } from "react";
import { useRouter } from "next/router";
import { gql, useMutation, useApolloClient } from "@apollo/client";
import {
  Avatar,
  Button,
  Container,
  Divider,
  Grid,
  makeStyles,
  Typography,
  useTheme,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import withLayout from "hocs/withLayout";
import { SideEventCard } from "components/eventCard";
import { useAuthState } from "contexts/auth";
import IOrg from "types/Org";
import Loading from "components/Loading";
import { MediumJobCard } from "components/JobCard";

import RateCard from "components/AssessmentCard/RateCard";
import DialogAssess from "components/AssessmentCard/DialogAssess";
import CommentCard from "components/AssessmentCard/CommentCard";
import { SideSmallOrgCard } from "components/orgCard";
import { renderText } from "utils/renderTextwithLink";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(6),
  },
  cover: {
    borderRadius: 8,
    width: "100%",
    marginBottom: 40,
    paddingTop: "67%",
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
  },
  categories: {
    padding: theme.spacing(1, 1.25),
    border: "1px solid #DEE4ED",
    borderRadius: 4,
  },
  userList: {
    display: "flex",
    flexFlow: "wrap row",
  },
}));

function OrgPage(): ReactElement {
  const theme = useTheme();
  const classes = useStyles();
  const router = useRouter();
  const client = useApolloClient();
  const { user } = useAuthState();
  const [rated, setRated] = useState<boolean>(false);
  const [OpenAssess, setOpenAssess] = useState(false);
  const [loading, setLoading] = useState(true);
  const [orgData, setOrgData] = useState<IOrg>(null);
  const [preview, setPreview] = useState("event");

  React.useEffect(() => {
    if (
      orgData &&
      orgData?.assessments?.some((ass) => ass.user.id === user?.id)
    ) {
      setRated(true);
    }
  }, [orgData, user]);

  React.useEffect(() => {
    if (!router.query.org_id) return;

    client
      .query({
        query: ORG_QUERY,
        variables: { id: router.query.org_id },
      })
      .then(({ data, loading }) => {
        if (!loading && data) {
          setLoading(false);
          setOrgData(data.org);
        }
      });
  }, [router.query.org_id]);

  if (loading) return <Loading />;

  return (
    <div className={classes.root}>
      <Container>
        <Grid container alignItems="center">
          <Button
            onClick={() => router.back()}
            variant="outlined"
            startIcon={<ArrowBackIcon />}
          >
            Quay lại
          </Button>
          <Typography
            display="inline"
            variant="h3"
            style={{ marginLeft: theme.spacing(2), fontWeight: "bold" }}
          >
            Chi tiết tổ chức
          </Typography>
        </Grid>

        <Grid
          container
          spacing={3}
          style={{
            marginTop: theme.spacing(3),
            marginBottom: theme.spacing(5),
            position: "relative",
          }}
        >
          <Grid item xs={12} md={8}>
            <Grid item>
              {orgData && (
                <div
                  style={{
                    backgroundImage: `url(${
                      orgData.coverUrl ? orgData.coverUrl : "/temp-cover.jpg"
                    })`,
                  }}
                  className={classes.cover}
                />
              )}
            </Grid>
            <Grid item>
              <Typography
                variant="h3"
                gutterBottom
                style={{ fontWeight: "bold" }}
              >
                Giới thiệu
              </Typography>

              {orgData &&
                orgData.description.split("\n").map((d) => (
                  <Typography
                    variant="body1"
                    color="textSecondary"
                    gutterBottom
                    key={d}
                  >
                    {renderText(d)}
                  </Typography>
                ))}
            </Grid>
            <Grid
              container
              direction="row"
              spacing={3}
              item
              xs={8}
              style={{ marginTop: "8px" }}
            >
              <Grid item xs>
                <Typography
                  variant="h3"
                  color="textPrimary"
                  gutterBottom
                  style={{
                    opacity: preview === "recruit" && 0.24,
                    cursor: "pointer",
                    fontWeight: "bold",
                    borderRight: "1px solid #DEE4ED",
                  }}
                  onClick={() => setPreview("event")}
                >
                  Các sự kiện của tổ chức
                </Typography>
              </Grid>
              <Grid item xs style={{ paddingLeft: "0px" }}>
                <Typography
                  variant="h3"
                  color="textPrimary"
                  gutterBottom
                  style={{
                    opacity: preview === "event" && 0.24,
                    fontWeight: "bold",
                    cursor: "pointer",
                  }}
                  onClick={() => setPreview("recruit")}
                >
                  Tin tuyển dụng
                </Typography>
              </Grid>
            </Grid>
            {preview === "event" && (
              <Grid item xs={12} style={{ marginTop: 24 }}>
                {orgData && orgData?.events?.length > 0 ? (
                  orgData?.events.map((event) => (
                    <SideEventCard {...event} key={event.id} />
                  ))
                ) : (
                  <Typography>Tổ chức chưa có sự kiện nào</Typography>
                )}
              </Grid>
            )}
            {preview === "recruit" && (
              <Grid item xs={12} style={{ marginTop: 24 }}>
                {orgData && orgData?.recruitments?.length > 0 ? (
                  orgData?.recruitments?.map((recruitment) => (
                    <MediumJobCard data={recruitment} key={recruitment.id} />
                  ))
                ) : (
                  <Typography>Tổ chức chưa có tin tuyển dụng nào</Typography>
                )}
              </Grid>
            )}
            <Grid
              container
              style={{
                marginTop: theme.spacing(5),
                marginBottom: theme.spacing(1),
              }}
              direction="column"
            >
              <Typography
                variant="h3"
                gutterBottom
                style={{ fontWeight: "bold" }}
              >
                Nhận xét
              </Typography>
              {/* <br /> */}
              {orgData && orgData?.assessments?.length === 0 && (
                <Typography style={{ fontSize: "16px" }}>
                  Hiện tại chưa có nhận xét nào
                </Typography>
              )}
              {orgData &&
                orgData.assessments?.length > 0 &&
                orgData.assessments?.map((assessment) => (
                  <CommentCard
                    key={assessment.id}
                    data={assessment}
                    type={orgData?.type}
                    iconState={user && assessment.user.id === user.id}
                  />
                ))}
            </Grid>
          </Grid>

          <Grid item xs={12} md={4}>
            <SideSmallOrgCard data={orgData} user={user} key={111} />
            <RateCard
              user={user}
              openAssess={() => setOpenAssess(true)}
              assessments={orgData && orgData.assessments}
              rated={rated}
            />
            <DialogAssess
              name={orgData?.name}
              openAssess={OpenAssess}
              closeAssess={() => setOpenAssess(false)}
              type={orgData && orgData?.type}
              id={orgData && orgData.id}
              initialRate={0}
              initialComment=""
            />
          </Grid>
        </Grid>

        <Divider />

        <div style={{ marginTop: theme.spacing(5) }}>
          <Typography variant="h3" color="textPrimary" gutterBottom>
            Các tổ chức khác
          </Typography>
        </div>
      </Container>
    </div>
  );
}

const ORG_QUERY = gql`
  query ($id: String!) {
    org(id: $id) {
      id
      name
      avatarUrl
      coverUrl
      description
      type
      categories
      contact {
        email
        phone_number
        website
        facebook_url
      }
      members {
        name
        avatarUrl
      }
      events {
        id
        name
        start_time
        categories
        location
        coverUrl
        subscribers {
          avatarUrl
        }
        organizer {
          id
          name
        }
      }
      recruitments {
        id
        title
        job_description
        deadline
        location
        isOpen
        cover_url
      }
      assessments {
        id
        rate
        comment
        user {
          id
          avatarUrl
          name
        }
      }
    }
  }
`;

export default withLayout(OrgPage);
