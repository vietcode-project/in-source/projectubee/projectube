import React, { ReactElement, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { gql, useMutation } from "@apollo/client";
import {
  Button,
  Container,
  Grid,
  makeStyles,
  useTheme,
  TextField,
  Divider,
  Typography,
  Avatar,
  Switch,
  Select,
  MenuItem,
  Theme,
  useMediaQuery,
  CircularProgress,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import { useAuthState } from "contexts/auth";
import withLayout from "hocs/withLayout";
import CateList from "components/Catelist";
import IOrg from "types/Org";
import { ConfirmUpdate } from "components/Confirmation";
import cates from "config/categories.json";

const useStyles = makeStyles((theme) => ({
  form: {},
  divider: {
    margin: theme.spacing(4, 0),
    backgroundColor: "#DEE4ED",
    height: "2px",
    width: "100%",
  },
  list: {
    display: "flex",
    justifyContent: "flex-start",
    flexWrap: "wrap",
    listStyle: "none",
    padding: theme.spacing(0.5),
    margin: 0,
  },
  root: {
    margin: theme.spacing(0.5, 0.5, 0.5, 0),
    borderRadius: "5%/10%",
    padding: "5px 10px",
    border: "1px solid #DEE4ED",
    color: "black",
    "&$selected": {
      background: "#EDE9FB",
      borderColor: "#532BDC",
      color: "#532BDC",
    },
  },
  selected: {},
  dropZone: {
    width: "100%",
    height: "500px",
    border: "1px solid #DEE4ED",
    borderRadius: "2%",
  },
}));

interface IInput {
  name: string;
  description: string;
  categories: string[];
  facebook_url: string;
  avatar: File;
  cover: File;
}

function newOrg(): ReactElement {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  const [org, setOrg] = useState<IOrg>();

  const [avatarPreview, setAvatarPreview] = useState("");
  const [coverPreview, setCoverPreview] = useState("");

  const [input, setInput] = useState<IInput>({
    name: "",
    categories: [],
    description: "",
    avatar: null,
    cover: null,
    facebook_url: "",
  });
  const { user, loading } = useAuthState();
  const [locked, setLocked] = useState(false);

  const [open, setOpen] = useState<boolean>(false);
  const [updateOrg, { data, loading: loadingUpdateOrg, error }] =
    useMutation(UPDATE_ORG_MUTATION);

  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  useEffect(() => {
    if (!loading && !user) {
      router.push("/signin");
    }
    if (user?.id != router.query.org_id) {
      if (user == null) {
        router.push("/404");
      }
    }
    if (user) setOrg(user);
  }, [loading, user, router.query.org_id]);

  useEffect(() => {
    org &&
      setInput({
        name: org.name,
        categories: org.categories,
        description: org.description,
        facebook_url: org.contact.facebook_url,
        avatar: null,
        cover: null,
      });
    org && setAvatarPreview(org.avatarUrl);
    org && setCoverPreview(org.coverUrl);
  }, [org, user]);

  function setorgdata(org) {
    setInput({
      name: org.name,
      categories: org.categories,
      description: org.description,
      facebook_url: org.contact.facebook_url,
      avatar: null,
      cover: null,
    });
    setAvatarPreview(org.avatarUrl);
    setCoverPreview(org.coverUrl);
  }

  function onSelectCover(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setInput((prev) => ({ ...prev, cover: e.target.files[0] }));
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setCoverPreview(objectUrl);
  }

  function handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  }

  function onSelectAvatar(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setInput((prev) => ({ ...prev, avatar: e.target.files[0] }));
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setAvatarPreview(objectUrl);
  }

  const resetChange = () => {
    setorgdata(org);
    if (org.coverUrl) {
      setCoverPreview(org.coverUrl);
    } else {
      setCoverPreview("");
    }
    if (org.avatarUrl) {
      setAvatarPreview(org.avatarUrl);
    } else {
      setAvatarPreview("");
    }
  };

  const handleChange = () => {};

  const removeNullPoint = (obj: Object) => {
    return Object.keys(obj)
      .filter((k) => obj[k] != null)
      .reduce((a, k) => ({ ...a, [k]: obj[k] }), {});
  };

  function handleSubmit(e: React.FormEvent) {
    e.preventDefault();

    if (!locked) {
      updateOrg({
        variables: {
          ...removeNullPoint(input),
        },
      }).catch((err) => console.error(err));
      setLocked(true);
    }
  }

  if (data) {
    if (locked) {
      router.push("/myorg").then(() => router.reload());
    }
  }

  if (error && locked) {
    setLocked(() => false);
  }

  return (
    <Container maxWidth="lg" style={{ marginTop: 58 }}>
      <Grid container alignItems="center">
        <Button
          onClick={() => setOpen(true)}
          variant="outlined"
          startIcon={<ArrowBackIcon />}
        >
          <Typography variant="button">Quay lại</Typography>
        </Button>
        <ConfirmUpdate
          Title="Bạn có muốn quay lại?"
          context="Hành động sẽ không cập nhật và lưu trữ thông tin mới nhất của tổ chức này."
          button1ctn="Quay lại"
          button2ctn="Lưu thay đổi"
          open={open}
          handleBack={() => router.back()}
          handleClose={() => setOpen(false)}
          handleConfirm={handleSubmit}
          loading={loadingUpdateOrg}
          stylebtn1={{ color: "#E4593B", borderColor: "#E4593B" }}
        />
        <Typography
          align="center"
          display="inline"
          style={{
            marginLeft: theme.spacing(2),
            fontSize: isDevicePhone ? "1.5em" : "24px",
          }}
        >
          Chỉnh sửa thông tin tổ chức
        </Typography>
      </Grid>
      <br />

      <Grid container>
        <form onSubmit={handleSubmit}>
          <Grid item xs={12}>
            <Grid
              container
              direction={isDevicePhone ? "column" : "row"}
              alignItems="flex-start"
              justify="space-between"
            >
              <Grid item xs={isDevicePhone ? 12 : 4}>
                <Typography variant="h2" gutterBottom>
                  Ảnh cover
                </Typography>
                <Typography variant="body1" color="secondary">
                  Ảnh bìa sẽ giúp tổ chức của bạn tạo ấn tượng tốt với mọi người
                </Typography>
              </Grid>
              <Grid item xs={isDevicePhone ? 12 : 7}>
                <Grid
                  container
                  justify="center"
                  style={{
                    width: isDevicePhone && "100vw",
                    marginTop: isDevicePhone && "10px",
                    paddingTop: "28.125%",
                    paddingBottom: "28.125%",
                    border: `${
                      coverPreview === undefined
                        ? "1px solid transparent"
                        : "1px solid #DEE4ED"
                    }`,
                    borderRadius: "8px",
                    backgroundImage: `${
                      coverPreview != undefined ? `url(${coverPreview})` : ""
                    }`,
                    backgroundPosition: "center",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                  }}
                ></Grid>
                <br />
                <Button variant="outlined" component="label">
                  <Typography variant="button">
                    Chọn ảnh từ thiết bị của bạn
                  </Typography>
                  <input
                    type="file"
                    accept="image/*"
                    onChange={onSelectCover}
                    hidden
                  />
                </Button>
              </Grid>
            </Grid>
          </Grid>

          <Divider className={classes.divider} />
          <Grid item xs={12}>
            <Grid
              container
              direction={isDevicePhone ? "column" : "row"}
              alignItems="flex-start"
              justify="space-between"
            >
              <Grid item xs={isDevicePhone ? 12 : 4}>
                <Typography variant="h2" gutterBottom>
                  Thông tin cơ bản
                </Typography>
                <Typography variant="body1" color="secondary">
                  Thông tin sẽ được hiển thị công khai nhằm giúp mọi người tiếp
                  cận dễ dàng hơn
                </Typography>
              </Grid>
              <Grid
                item
                xs={isDevicePhone ? 12 : 7}
                style={{ width: isDevicePhone && "100%" }}
              >
                <Typography variant="button" gutterBottom>
                  {" "}
                  Avatar tổ chức{" "}
                </Typography>
                <Typography variant="body2" color="secondary" gutterBottom>
                  {" "}
                  Dung lượng ảnh tối đa là 2MB{" "}
                </Typography>
                <Grid
                  container
                  justify="flex-start"
                  direction={isDevicePhone ? "row" : "column"}
                  alignItems={isDevicePhone ? "center" : "flex-start"}
                >
                  <Avatar
                    src={avatarPreview}
                    style={{
                      width: theme.spacing(7),
                      height: theme.spacing(7),
                      margin: theme.spacing(1, 0),
                    }}
                  />
                  <Button
                    variant="outlined"
                    component="label"
                    style={{ marginLeft: isDevicePhone && "8px" }}
                  >
                    <Typography variant="button">
                      Chọn ảnh từ thiết bị của bạn
                    </Typography>
                    <input
                      type="file"
                      name="avatarUrl"
                      accept="image/png, image/jpeg"
                      hidden
                      onChange={onSelectAvatar}
                    />
                  </Button>
                </Grid>
                <br />
                <br />
                <Grid container>
                  <Grid
                    item
                    xs={12}
                    justify="space-between"
                    direction={isDevicePhone ? "column" : "row"}
                  >
                    <Grid container justify="space-between">
                      <Grid
                        item
                        xs={isDevicePhone ? 12 : 5}
                        style={{
                          width: isDevicePhone && "100%",
                          marginBottom: isDevicePhone && "8px",
                        }}
                      >
                        <Typography variant="button" gutterBottom>
                          {" "}
                          Tên tổ chức{" "}
                        </Typography>
                        <br />
                        <br />
                        <TextField
                          name="name"
                          placeholder="Nhập tên tổ chức của bạn"
                          autoFocus
                          fullWidth
                          value={input.name}
                          onChange={handleInputChange}
                        />
                      </Grid>

                      <Grid
                        item
                        xs={isDevicePhone ? 12 : 5}
                        style={{
                          width: isDevicePhone && "100%",
                          marginBottom: isDevicePhone && "8px",
                        }}
                      >
                        <Typography variant="button">
                          {" "}
                          Phân loại tổ chức{" "}
                        </Typography>
                        <br />
                        {/* PHẦN PHÂN LOẠI NÀY CHƯA CÓ */}
                        <Select
                          fullWidth
                          MenuProps={{
                            getContentAnchorEl: null,
                            anchorOrigin: {
                              vertical: "bottom",
                              horizontal: "left",
                            },
                          }}
                          variant="outlined"
                        >
                          <MenuItem value={1}>Tổ chức lợi nhuận</MenuItem>
                          <MenuItem value={2}>Tổ chức phi lợi nhuận</MenuItem>
                        </Select>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid>
                  <Typography variant="button">Chủ đề của tổ chức</Typography>
                  {(input.categories.length > 3 && (
                    <Typography variant="body2" color="error">
                      Bạn đã chọn quá giới hạn, vui lòng chọn lại
                    </Typography>
                  )) || (
                    <Typography variant="body2" color="secondary">
                      Vui lòng chọn tối đa 3 chủ đề
                    </Typography>
                  )}
                  <Grid component="ul" className={classes.list}>
                    <CateList
                      cates={cates}
                      selected={input.categories}
                      handleChoose={(label) => {
                        if (input.categories.includes(label)) {
                          setInput((prev) => ({
                            ...prev,
                            categories: prev.categories.filter(
                              (c) => c !== label
                            ),
                          }));
                        } else {
                          setInput((prev) => ({
                            ...prev,
                            categories: [...prev.categories, label],
                          }));
                        }
                      }}
                    />
                  </Grid>
                </Grid>
                <br />
                <Grid container justify="space-between" alignItems="center">
                  <Grid item>
                    <Typography variant="button" gutterBottom>
                      Giới thiệu tổ chức
                    </Typography>
                  </Grid>
                  <Grid item>
                    {input.description ? (
                      <Typography variant="body2" color="primary">
                        {5000 - input.description.length} kí tự còn lại
                      </Typography>
                    ) : (
                      <Typography variant="body2" color="secondary">
                        {5000 - input.description.length} kí tự còn lại
                      </Typography>
                    )}
                  </Grid>
                </Grid>

                <TextField
                  multiline
                  fullWidth
                  rows={6}
                  placeholder="Nhập văn bản"
                  variant="outlined"
                  name="description"
                  value={input.description}
                  onChange={handleInputChange}
                  inputProps={{ maxLength: 5000 }}
                />

                <Typography variant="button">Đường dẫn liên hệ</Typography>
                <Typography variant="body2" color="secondary" gutterBottom>
                  Mọi người sẽ liên hệ với tổ chức của bạn dễ dàng hơn
                </Typography>
                <TextField
                  name="facebook_url"
                  value={input.facebook_url}
                  variant="outlined"
                  fullWidth
                  placeholder="facebook.com, instagram,com,..."
                  onChange={handleInputChange}
                />
              </Grid>
            </Grid>
          </Grid>

          <Divider className={classes.divider} />

          <Grid item xs={12}>
            <Grid
              container
              direction={isDevicePhone ? "column" : "row"}
              alignItems="flex-start"
              justify="space-between"
            >
              <Grid item xs={isDevicePhone ? 12 : 4}>
                <Typography variant="h2" gutterBottom>
                  Cài đặt nâng cao
                </Typography>
                <Typography variant="body1" color="secondary">
                  Cài đặt nâng cao giúp bạn quản lý tổ chức dễ dàng hơn
                </Typography>
              </Grid>

              <Grid
                item
                xs={isDevicePhone ? 12 : 7}
                style={{
                  marginTop: isDevicePhone && "8px",
                  width: isDevicePhone && "100%",
                }}
              >
                <Grid>
                  <Typography variant="h3" gutterBottom>
                    Về tổ chức
                  </Typography>

                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="space-around"
                  >
                    <Grid item xs={11}>
                      <Typography variant="button">
                        Nhận thông báo mỗi lượt liên lạc
                      </Typography>
                      <Typography
                        variant="body2"
                        color="secondary"
                        gutterBottom
                      >
                        Thông báo sẽ được gửi trực tiếp đến Email của bạn khi
                        người dùng ấn nút "Liên lạc với tổ chức"
                      </Typography>
                    </Grid>
                    <Grid item xs={1}>
                      <Switch
                        disabled
                        onChange={handleChange}
                        name="checkedB"
                        color="primary"
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="space-between"
                >
                  <Grid item xs={11}>
                    <Typography variant="button">Thêm quản trị viên</Typography>
                    <Typography variant="body2" color="secondary" gutterBottom>
                      Những thành viên trong danh sách quản trị có thể chỉnh sửa
                      thông tin tổ chức
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <Switch
                      disabled
                      onChange={handleChange}
                      name="checkedB"
                      color="primary"
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="space-between"
                >
                  <Grid item xs={11}>
                    <Typography variant="button">Ưu tiên hiển thị</Typography>
                    <Typography variant="body2" color="secondary" gutterBottom>
                      Tổ chức của bạn sẽ được ưu tiên hiển thị đầu tiên so với
                      các tổ chức khác
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <Switch
                      disabled
                      onChange={handleChange}
                      name="checkedB"
                      color="primary"
                    />
                  </Grid>
                </Grid>

                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="space-between"
                >
                  <Grid item xs={11}>
                    <Typography variant="button">
                      Tạo thêm tổ chức mới
                    </Typography>
                    <Typography variant="body2" color="secondary" gutterBottom>
                      Bạn có thể tạo tối đa 3 thay vì 1 tổ chức
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <Switch
                      disabled
                      onChange={handleChange}
                      name="checkedB"
                      color="primary"
                    />
                  </Grid>
                </Grid>
                <br />
                <Grid>
                  <Typography variant="h3" gutterBottom>
                    Về sự kiện
                  </Typography>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="space-between"
                  >
                    <Grid item xs={11}>
                      <Typography variant="button">
                        Nhận thông báo mỗi lượt đăng ký
                      </Typography>
                      <Typography
                        variant="body2"
                        color="secondary"
                        gutterBottom
                      >
                        Thông báo sẽ được gửi trực tiếp đến Email của bạn khi
                        người dùng ấn nút "Đăng ký thông báo"
                      </Typography>
                    </Grid>
                    <Grid item xs={1}>
                      <Switch
                        disabled
                        onChange={handleChange}
                        name="checkedB"
                        color="primary"
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="space-between"
                  >
                    <Grid item xs={11}>
                      <Typography variant="button">
                        Không giới hạn sự kiện
                      </Typography>
                      <Typography
                        variant="body2"
                        color="secondary"
                        gutterBottom
                      >
                        Tổ chức của bạn có thể tạo không giới hạn sự kiện thay
                        vì tối đa 10 sự kiện
                      </Typography>
                    </Grid>
                    <Grid item xs={1}>
                      <Switch
                        disabled
                        onChange={handleChange}
                        name="checkedB"
                        color="primary"
                      />
                    </Grid>
                  </Grid>
                </Grid>

                <Divider className={classes.divider} />

                <Grid container justify="flex-end" spacing={1}>
                  <Grid item xs={isDevicePhone ? 6 : 4}>
                    <Button
                      fullWidth
                      variant="outlined"
                      color="primary"
                      style={{
                        padding: "9px 20px",
                      }}
                      disabled={
                        org &&
                        input.name == org.name &&
                        input.facebook_url == org.contact.facebook_url &&
                        input.categories == org.categories &&
                        input.description == org.description &&
                        !input.cover &&
                        !input.avatar
                      }
                      onClick={resetChange}
                    >
                      <Typography variant="h5">Hoàn tác thay đổi</Typography>
                    </Button>
                  </Grid>
                  <Grid item xs={!data ? 4 : 8}>
                    <Button
                      onClick={handleSubmit}
                      fullWidth
                      variant="contained"
                      color="primary"
                      style={{
                        padding: "9px 20px",
                      }}
                      disabled={
                        !(
                          input.name != "" &&
                          input.facebook_url != "" &&
                          input.categories.length <= 3 &&
                          input.categories.length >= 1 &&
                          input.description != "" &&
                          !locked &&
                          !loadingUpdateOrg &&
                          !data
                        )
                      }
                    >
                      {!loadingUpdateOrg && !data ? (
                        <Typography variant="h5">Lưu thay đổi</Typography>
                      ) : (
                        <CircularProgress color="inherit" size={25} />
                      )}
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Container>
  );
}

const UPDATE_ORG_MUTATION = gql`
  mutation (
    $name: String!
    $facebook_url: String
    $description: String!
    $categories: [String!]!
    $avatar: Upload
    $cover: Upload
  ) {
    update_org(
      data: {
        name: $name
        categories: $categories
        contact: { facebook_url: $facebook_url }
        description: $description
        avatar: $avatar
        cover: $cover
      }
    ) {
      name
    }
  }
`;

export default withLayout(newOrg);
