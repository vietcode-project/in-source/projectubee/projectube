import React, { ReactElement, useEffect, useState } from "react";

import {
  Button,
  Container,
  Grid,
  makeStyles,
  useTheme,
  TextField,
  Divider,
  Typography,
  Avatar,
  Switch,
  Select,
  MenuItem,
  Dialog,
  Theme,
  useMediaQuery,
  IconButton,
  CircularProgress,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import withLayout from "hocs/withLayout";
import { useAuthState } from "contexts/auth";
import { useRouter } from "next/router";
import { gql, useMutation } from "@apollo/client";

import CateList from "../../components/Catelist";
import cates from "config/categories.json";
import PolicyCheckbox from "components/PolicyCheckbox";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";
const useStyles = makeStyles((theme) => ({
  form: {},
  submit: {},
  divider: {
    margin: theme.spacing(4, 0),
    backgroundColor: "#DEE4ED",
    height: "2px",
    width: "100%",
  },
  list: {
    display: "flex",
    justifyContent: "flex-start",
    flexWrap: "wrap",
    listStyle: "none",
    padding: theme.spacing(0.5),
    margin: 0,
  },
  root: {
    margin: theme.spacing(0.5, 0.5, 0.5, 0),
    borderRadius: "5%/10%",
    padding: "5px 10px",
    border: "1px solid #DEE4ED",
    color: "black",
    "&$selected": {
      background: "#EDE9FB",
      borderColor: "#532BDC",
      color: "#532BDC",
    },
  },
  selected: {},
  dropZone: {
    width: "100%",
    height: "500px",
    border: "1px solid #DEE4ED",
    borderRadius: "2%",
  },
  textGutterBottom: {
    display: "inline-block",
    lineHeight: "0",
    marginBottom: "4px",
  },
  groupGutterBottom: {
    marginTop: "0",
    marginBottom: "16px",
  },
}));

interface IInput {
  name: string;
  description: string;
  categories: string[];
  facebook_url: string;
  avatar: File;
  cover: File;
}

function newOrg(): ReactElement {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  const [avatarPreview, setAvatarPreview] = useState("");
  const [coverPreview, setCoverPreview] = useState("");

  const [input, setInput] = useState<IInput>({
    name: "",
    categories: [],
    description: "",
    avatar: null,
    cover: null,
    facebook_url: "",
  });

  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  const { user, loading } = useAuthState();
  const [checked, setChecker] = useState(false);
  const [locked, setLocked] = useState(false);
  const [createOrg, { data, loading: loadingCreateOrg, error }] =
    useMutation(CREATE_ORG_MUTATION);

  useEffect(() => {
    if (!loading && !user) {
      router.push("/signin");
    }
  }, [loading, user]);

  function handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  }

  function onSelectAvatar(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setInput((prev) => ({ ...prev, avatar: e.target.files[0] }));
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setAvatarPreview(objectUrl);
  }

  function onSelectCover(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setInput((prev) => ({ ...prev, cover: e.target.files[0] }));
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setCoverPreview(objectUrl);
  }

  const handleChange = () => {};

  function handleSubmit(e: React.FormEvent) {
    e.preventDefault();
    if (!locked) {
      createOrg({
        variables: {
          name: input.name,
          description: input.description,
          categories: input.categories,
          avatar: input.avatar,
          cover: input.cover,
          facebook_url: input.facebook_url,
        },
      }).catch((err) => console.error(err));
      setLocked(true);
    }
  }

  if (data) {
    if (locked) setLocked(() => false);
    router.push("/myorg").then(() => router.reload());
  }

  if (error && locked) {
    setLocked(() => false);
  }

  return (
    <Container maxWidth="lg">
      <div style={{ height: theme.spacing(6) }} />
      <Grid container alignItems="center">
        <Button
          onClick={() => router.back()}
          variant="outlined"
          startIcon={<ArrowBackIcon />}
        >
          <Typography variant="button">Quay lại</Typography>
        </Button>
        <Typography
          variant="h2"
          align="center"
          display="inline"
          style={{ marginLeft: theme.spacing(2) }}
        >
          Tạo tổ chức mới
        </Typography>
      </Grid>
      <br />

      <Grid container>
        <form onSubmit={handleSubmit}>
          <Grid item xs={12}>
            <Grid
              container
              direction={isDevicePhone ? "column" : "row"}
              alignItems="flex-start"
              justify="space-between"
            >
              <Grid item xs={isDevicePhone ? 12 : 4}>
                <Typography variant="h2" gutterBottom>
                  Ảnh cover
                </Typography>
                <Typography variant="body1" color="secondary">
                  Ảnh bìa sẽ giúp tổ chức của bạn tạo ấn tượng tốt với mọi người
                </Typography>
              </Grid>
              <Grid item xs={isDevicePhone ? 12 : 7}>
                <Grid
                  container
                  justify="center"
                  style={{
                    width: isDevicePhone && "100vw",
                    marginTop: isDevicePhone && "10px",
                    paddingTop: "28.125%",
                    paddingBottom: "28.125%",
                    border: `${
                      coverPreview === undefined
                        ? "1px solid transparent"
                        : "1px solid #DEE4ED"
                    }`,
                    borderRadius: "8px",
                    backgroundImage: `${
                      coverPreview != undefined ? `url(${coverPreview})` : ""
                    }`,
                    backgroundPosition: "center",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                  }}
                >
                  <Grid item>
                    {!input.cover && (
                      <Typography variant="h3">Kéo thả ảnh vào đây</Typography>
                    )}
                  </Grid>
                </Grid>
                <br />
                <Button variant="outlined" component="label">
                  <Typography variant="button">
                    Chọn ảnh từ thiết bị của bạn
                  </Typography>
                  <input
                    type="file"
                    accept="image/*"
                    onChange={onSelectCover}
                    hidden
                  />
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Divider className={classes.divider} />
          <Grid item xs={12}>
            <Grid
              container
              direction={isDevicePhone ? "column" : "row"}
              alignItems="flex-start"
              justify="space-between"
            >
              <Grid item xs={isDevicePhone ? 12 : 4}>
                <Typography variant="h2" gutterBottom>
                  Thông tin cơ bản
                </Typography>
                <Typography variant="body1" color="secondary">
                  Thông tin sẽ được hiển thị công khai nhằm giúp mọi người tiếp
                  cận dễ dàng hơn
                </Typography>
              </Grid>
              <Grid
                item
                xs={isDevicePhone ? 12 : 7}
                style={{ width: isDevicePhone && "100%" }}
              >
                <Typography
                  variant="button"
                  gutterBottom
                  style={{ marginTop: isDevicePhone && "10px" }}
                >
                  {" "}
                  Avatar tổ chức{" "}
                </Typography>
                <Typography variant="body2" color="secondary" gutterBottom>
                  {" "}
                  Dung lượng ảnh tối đa là 2MB{" "}
                </Typography>
                <Grid
                  container
                  justify="flex-start"
                  direction={isDevicePhone ? "row" : "column"}
                  alignItems={isDevicePhone ? "center" : "flex-start"}
                >
                  <Avatar
                    src={avatarPreview}
                    style={{
                      width: theme.spacing(7),
                      height: theme.spacing(7),
                      margin: theme.spacing(1, 0),
                    }}
                  />
                  <Button
                    variant="outlined"
                    component="label"
                    style={{ marginLeft: isDevicePhone && "8px" }}
                  >
                    <Typography variant="button">
                      Chọn ảnh từ thiết bị của bạn
                    </Typography>
                    <input
                      type="file"
                      name="avatarUrl"
                      accept="image/png, image/jpeg"
                      hidden
                      onChange={onSelectAvatar}
                    />
                  </Button>
                </Grid>
                <br />
                <br />
                <Grid container>
                  <Grid
                    item
                    xs={12}
                    justify="space-between"
                    direction={isDevicePhone ? "column" : "row"}
                  >
                    <Grid container>
                      <Grid
                        item
                        xs={isDevicePhone ? 12 : 5}
                        style={{
                          width: isDevicePhone && "100%",
                          marginBottom: isDevicePhone && "8px",
                        }}
                      >
                        <Typography variant="button" gutterBottom>
                          {" "}
                          Tên tổ chức{" "}
                        </Typography>
                        <br />
                        <TextField
                          name="name"
                          placeholder="Nhập tên tổ chức của bạn"
                          autoFocus
                          fullWidth
                          value={input.name}
                          onChange={handleInputChange}
                        />
                      </Grid>

                      <Grid
                        item
                        xs={isDevicePhone ? 12 : 5}
                        style={{
                          width: isDevicePhone && "100%",
                          marginBottom: isDevicePhone && "8px",
                        }}
                      >
                        <Typography variant="button">
                          {" "}
                          Phân loại tổ chức{" "}
                        </Typography>
                        <br />
                        {/* PHẦN PHÂN LOẠI NÀY CHƯA CÓ */}
                        <Select
                          fullWidth
                          MenuProps={{
                            getContentAnchorEl: null,
                            anchorOrigin: {
                              vertical: "bottom",
                              horizontal: "left",
                            },
                          }}
                          variant="outlined"
                        >
                          <MenuItem value={1}>Tổ chức lợi nhuận</MenuItem>
                          <MenuItem value={2}>Tổ chức phi lợi nhuận</MenuItem>
                        </Select>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid>
                  <Typography variant="button">Chủ đề của tổ chức</Typography>
                  {(input.categories.length > 3 && (
                    <Typography variant="body2" color="error">
                      Bạn đã chọn quá giới hạn, vui lòng chọn lại
                    </Typography>
                  )) || (
                    <Typography variant="body2" color="secondary">
                      Vui lòng chọn tối đa 3 chủ đề
                    </Typography>
                  )}
                  <Grid component="ul" className={classes.list}>
                    <CateList
                      cates={cates}
                      selected={input.categories}
                      handleChoose={(label) => {
                        if (input.categories.includes(label)) {
                          setInput((prev) => ({
                            ...prev,
                            categories: prev.categories.filter(
                              (c) => c !== label
                            ),
                          }));
                        } else {
                          setInput((prev) => ({
                            ...prev,
                            categories: [...prev.categories, label],
                          }));
                        }
                      }}
                    />
                  </Grid>
                </Grid>
                <br />
                <Grid container justify="space-between" alignItems="center">
                  <Grid item>
                    <Typography variant="button" gutterBottom>
                      Giới thiệu tổ chức
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="secondary">
                      {5000 - input.description.length} kí tự còn lại
                    </Typography>
                  </Grid>
                </Grid>

                <TextField
                  multiline
                  fullWidth
                  rows={6}
                  placeholder="Nhập văn bản"
                  variant="outlined"
                  name="description"
                  value={input.description}
                  onChange={handleInputChange}
                  inputProps={{ maxLength: 5000 }}
                />

                <Typography variant="button">Đường dẫn liên hệ</Typography>
                <Typography variant="body2" color="secondary" gutterBottom>
                  Mọi người sẽ liên hệ với tổ chức của bạn dễ dàng hơn
                </Typography>
                <TextField
                  name="facebook_url"
                  value={input.facebook_url}
                  variant="outlined"
                  fullWidth
                  placeholder="facebook.com, instagram,com,..."
                  onChange={handleInputChange}
                />
              </Grid>
            </Grid>
          </Grid>
          <Divider className={classes.divider} />
          <Grid item xs={12}>
            <Grid
              container
              direction={isDevicePhone ? "column" : "row"}
              alignItems="flex-start"
              justify="space-between"
            >
              <Grid item xs={isDevicePhone ? 12 : 4}>
                <Typography variant="h2" gutterBottom>
                  Cài đặt nâng cao
                </Typography>
                <Typography variant="body1" color="secondary">
                  Cài đặt nâng cao giúp bạn quản lý tổ chức dễ dàng hơn (tính
                  năng đang xây dựng)
                </Typography>
              </Grid>

              <Grid
                item
                xs={isDevicePhone ? 12 : 7}
                style={{
                  marginTop: isDevicePhone && "8px",
                  width: isDevicePhone && "100%",
                }}
              >
                <Grid className={classes.groupGutterBottom}>
                  <Typography
                    variant="h3"
                    className={classes.groupGutterBottom}
                  >
                    Về tổ chức
                  </Typography>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="space-around"
                  >
                    <Grid item xs={11}>
                      <Typography
                        variant="button"
                        className={classes.textGutterBottom}
                      >
                        Nhận thông báo mỗi lượt liên lạc
                      </Typography>
                      <Typography variant="body2" color="secondary">
                        Thông báo sẽ được gửi trực tiếp đến Email của bạn khi
                        người dùng ấn nút "Liên lạc với tổ chức"
                      </Typography>
                    </Grid>

                    <Grid item xs={1}>
                      <Switch
                        disabled
                        onChange={handleChange}
                        name="checkedB"
                        color="primary"
                      />
                    </Grid>
                  </Grid>
                </Grid>

                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="space-between"
                  className={classes.groupGutterBottom}
                >
                  <Grid item xs={11}>
                    <Typography
                      variant="button"
                      className={classes.textGutterBottom}
                    >
                      Thêm quản trị viên
                    </Typography>
                    <Typography variant="body2" color="secondary" gutterBottom>
                      Những thành viên trong danh sách quản trị có thể chỉnh sửa
                      thông tin tổ chức
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <Switch
                      disabled
                      onChange={handleChange}
                      name="checkedB"
                      color="primary"
                    />
                  </Grid>
                </Grid>

                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="space-between"
                  className={classes.groupGutterBottom}
                >
                  <Grid item xs={11}>
                    <Typography
                      variant="button"
                      className={classes.textGutterBottom}
                    >
                      Ưu tiên hiển thị
                    </Typography>
                    <Typography variant="body2" color="secondary" gutterBottom>
                      Tổ chức của bạn sẽ được ưu tiên hiển thị đầu tiên so với
                      các tổ chức khác
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <Switch
                      disabled
                      onChange={handleChange}
                      name="checkedB"
                      color="primary"
                    />
                  </Grid>
                </Grid>

                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="space-between"
                >
                  <Grid item xs={11}>
                    <Typography
                      variant="button"
                      className={classes.textGutterBottom}
                    >
                      Tạo thêm tổ chức mới
                    </Typography>
                    <Typography variant="body2" color="secondary" gutterBottom>
                      Bạn có thể tạo tối đa 3 thay vì 1 tổ chức
                    </Typography>
                  </Grid>
                  <Grid item xs={1}>
                    <Switch
                      disabled
                      onChange={handleChange}
                      name="checkedB"
                      color="primary"
                    />
                  </Grid>
                </Grid>
                <br />
                <Grid>
                  <Typography
                    variant="h3"
                    className={classes.groupGutterBottom}
                  >
                    Về sự kiện
                  </Typography>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="space-between"
                    className={classes.groupGutterBottom}
                  >
                    <Grid item xs={11}>
                      <Typography
                        variant="button"
                        className={classes.textGutterBottom}
                      >
                        Nhận thông báo mỗi lượt đăng ký
                      </Typography>
                      <Typography
                        variant="body2"
                        color="secondary"
                        gutterBottom
                      >
                        Thông báo sẽ được gửi trực tiếp đến Email của bạn khi
                        người dùng ấn nút "Đăng ký thông báo"
                      </Typography>
                    </Grid>
                    <Grid item xs={1}>
                      <Switch
                        disabled
                        onChange={handleChange}
                        name="checkedB"
                        color="primary"
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="space-between"
                  >
                    <Grid item xs={11}>
                      <Typography
                        variant="button"
                        className={classes.textGutterBottom}
                      >
                        Không giới hạn sự kiện
                      </Typography>
                      <Typography
                        variant="body2"
                        color="secondary"
                        gutterBottom
                      >
                        Tổ chức của bạn có thể tạo không giới hạn sự kiện thay
                        vì tối đa 10 sự kiện
                      </Typography>
                    </Grid>
                    <Grid item xs={1}>
                      <Switch
                        disabled
                        onChange={handleChange}
                        name="checkedB"
                        color="primary"
                      />
                    </Grid>
                  </Grid>
                </Grid>

                <Divider className={classes.divider} />

                <Grid container direction={isDevicePhone ? "column" : "row"}>
                  <Grid item xs={isDevicePhone ? 12 : 6}>
                    <PolicyCheckbox
                      checked={checked}
                      checkboxClick={() => {
                        setChecker(!checked);
                      }}
                    />
                  </Grid>
                  <Grid
                    item
                    xs={isDevicePhone ? 12 : 6}
                    style={{ marginTop: isDevicePhone && "8px" }}
                  >
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      className={classes.submit}
                      size="large"
                      disabled={
                        !(
                          input.name != "" &&
                          input.facebook_url != "" &&
                          input.categories.length <= 3 &&
                          input.categories.length >= 1 &&
                          input.description != "" &&
                          !locked &&
                          checked &&
                          !loadingCreateOrg &&
                          !data
                        )
                      }
                    >
                      {!loadingCreateOrg && !data ? (
                        "Tạo tổ chức mới"
                      ) : (
                        <CircularProgress color="inherit" size={25} />
                      )}
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </Grid>
    </Container>
  );
}

const CREATE_ORG_MUTATION = gql`
  mutation (
    $name: String!
    $facebook_url: String!
    $description: String!
    $categories: [String!]!
    $avatar: Upload
    $cover: Upload
  ) {
    create_org(
      data: {
        name: $name
        categories: $categories
        contact: { facebook_url: $facebook_url }
        description: $description
        avatar: $avatar
        cover: $cover
      }
    ) {
      name
    }
  }
`;

export default withLayout(newOrg);
