import React, { useState } from "react";
import { useRouter } from "next/router";
import { gql, useQuery } from "@apollo/client";
import { useTheme } from "@material-ui/core/styles";
import { Grid, Typography, Button, Container } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import withLayout from "hocs/withLayout";
import { EssentialData as EventCardData } from "graphql/EventFragment";
import Org from "types/Org";
import Event from "types/Event";
import { LargeOrgCard } from "components/orgCard";
import { LargeEventCard } from "components/eventCard";
import Loading from "components/Loading";

function SearchResult() {
  const router = useRouter();
  const [eventlimit, seteventlimit] = useState(4);
  const [orglimit, setorglimit] = useState(4);
  const theme = useTheme();
  const { data, loading, error } = useQuery<{
    orgs: Org[];
    events: Event[];
  }>(SEARCH_QUERY, {
    variables: {
      filterEvent: {
        name: {
          re: router.query.keyword,
        },
      },
      filterOrg: {
        name: {
          re: router.query.keyword,
        },
      },
    },
  });

  if (loading) return <Loading />;

  if (error) {
    return <div>Errorrrrr :(</div>;
  }

  const loadMore = (e) => {
    if (e.currentTarget.id == "event") {
      seteventlimit(eventlimit + 4);
    }
    if (e.currentTarget.id == "org") {
      setorglimit(orglimit + 4);
    }
  };

  return (
    <>
      <Container maxWidth="lg">
        <Grid
          container
          alignItems="center"
          justify="space-between"
          direction="row"
          style={{ marginTop: theme.spacing(5) }}
        >
          <Grid item>
            <Grid container alignItems="center">
              <Button
                onClick={() => router.back()}
                variant="outlined"
                startIcon={<ArrowBackIcon />}
              >
                <Typography variant="button">Quay lại</Typography>
              </Button>
              <Typography
                variant="h2"
                align="center"
                display="inline"
                style={{ marginLeft: theme.spacing(2) }}
              >
                Kết quả tìm kiếm
              </Typography>
            </Grid>
          </Grid>
        </Grid>

        <div style={{ height: theme.spacing(4) }} />

        {/* Event list */}
        <div style={{ marginBottom: theme.spacing(16) }}>
          <Grid container justify="space-between">
            <Grid item>
              <Typography
                variant="h2"
                style={{ marginBottom: theme.spacing(4) }}
              >
                Sự kiện
              </Typography>
            </Grid>
          </Grid>
          {(router.query.keyword &&
            data.events.some(({ name }) =>
              name
                .toLowerCase()
                .includes((router.query.keyword as string).toLowerCase())
            ) && (
              <Grid container spacing={2}>
                {data.events &&
                  router.query.keyword &&
                  data.events
                    .filter(({ name }) =>
                      name
                        .toLowerCase()
                        .includes(
                          (router.query.keyword as string).toLowerCase()
                        )
                    )
                    .slice()
                    .slice(0, eventlimit)
                    .map((event) => (
                      <Grid item xs={4} md={3} key={event.id}>
                        <LargeEventCard
                          {...event}
                          start_time={new Date()}
                          categories={event.categories}
                          location={event.location || "Online"}
                        />
                      </Grid>
                    ))}
              </Grid>
            )) ||
            "Không có sự kiện phù hợp :("}
        </div>
        {router.query.keyword &&
          data.events.some(({ name }) =>
            name
              .toLowerCase()
              .includes((router.query.keyword as string).toLowerCase())
          ) && (
            <div
              style={{
                width: 300,
                margin: "0 auto",
                marginTop: theme.spacing(4),
              }}
            >
              <Button
                variant="outlined"
                color="primary"
                fullWidth
                onClick={loadMore}
                id="event"
              >
                Xem thêm
              </Button>
            </div>
          )}
        {/* Org list */}
        <div>
          <Grid container justify="space-between">
            <Grid item>
              <Typography
                variant="h2"
                style={{ marginBottom: theme.spacing(4) }}
              >
                Tổ chức
              </Typography>
            </Grid>
          </Grid>

          {(router.query.keyword &&
            data.orgs.some(({ name }) =>
              name
                .toLowerCase()
                .includes((router.query.keyword as string).toLowerCase())
            ) && (
              <Grid container spacing={2}>
                {data.orgs &&
                  router.query.keyword &&
                  data.orgs
                    .filter(({ name }) =>
                      name
                        .toLowerCase()
                        .includes(
                          (router.query.keyword as string).toLowerCase()
                        )
                    )
                    .slice()
                    .slice(0, orglimit)
                    .map((org) => (
                      <Grid item xs={4} md={3} key={org.id}>
                        <LargeOrgCard {...org} />
                      </Grid>
                    ))}
              </Grid>
            )) || (
            <Typography
              variant="h4"
              style={{
                fontSize: "12px",
                fontWeight: 400,
                lineHeight: "16px",

                color: "#8D9198",
              }}
            >
              Không có kết quả tìm kiếm phù hợp
            </Typography>
          )}
        </div>
        {router.query.keyword &&
          data.orgs.some(({ name }) =>
            name
              .toLowerCase()
              .includes((router.query.keyword as string).toLowerCase())
          ) && (
            <div
              style={{
                width: 300,
                margin: "0 auto",
                marginTop: theme.spacing(4),
              }}
            >
              <Button
                variant="outlined"
                color="primary"
                fullWidth
                onClick={loadMore}
                id="org"
              >
                Xem thêm
              </Button>
            </div>
          )}
      </Container>
    </>
  );
}

const SEARCH_QUERY = gql`
  query ($filterEvent: EventFilterInput, $filterOrg: OrgFilterInput) {
    orgs(filter: $filterOrg) {
      id
      name
      avatarUrl
      categories
    }
    events(filter: $filterEvent) {
      coverUrl
      categories
      ...EventEssentialFragment
    }
  }
  ${EventCardData()}
`;

export default withLayout(SearchResult);
