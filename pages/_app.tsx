import React from "react";
import { AppProps } from "next/app";
import Head from "next/head";
import { ApolloProvider } from "@apollo/client";
import { ThemeProvider } from "@material-ui/core/styles";
import { CssBaseline } from "@material-ui/core";

import { AuthProvider } from "contexts/auth";
import { mainTheme } from "utils/theme";
import { client } from "utils/apolloClient";

import { useEffect } from "react";
import { useRouter } from "next/router";
import * as gtag from "../lib/gtag";

import "styles/common.css";
import { SelectedDateProvider } from "components/Calendar";

export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url);
    };
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);

  return (
    <ApolloProvider client={client}>
      <AuthProvider>
        <SelectedDateProvider>
          <ThemeProvider theme={mainTheme}>
            <Head>
              <title>Projectube</title>
              <link rel="shortcut icon" href="/favicon.svg" />
              <link
                rel="preload"
                href="/fonts/CircularStd-Black.otf"
                as="font"
                crossOrigin=""
              />
              <link
                rel="preload"
                href="/fonts/CircularStd-BlackItalic.otf"
                as="font"
                crossOrigin=""
              />
              <link
                rel="preload"
                href="/fonts/CircularStd-Bold.otf"
                as="font"
                crossOrigin=""
              />
              <link
                rel="preload"
                href="/fonts/CircularStd-BoldItalic.otf"
                as="font"
                crossOrigin=""
              />
              <link
                rel="preload"
                href="/fonts/CircularStd-Book.otf"
                as="font"
                crossOrigin=""
              />
              <link
                rel="preload"
                href="/fonts/CircularStd-BookItalic.otf"
                as="font"
                crossOrigin=""
              />
              <link
                rel="preload"
                href="/fonts/CircularStd-Medium.otf"
                as="font"
                crossOrigin=""
              />
              <link
                rel="preload"
                href="/fonts/CircularStd-MediumItalic.otf"
                as="font"
                crossOrigin=""
              />
            </Head>
            <CssBaseline />
            <Component {...pageProps} />
          </ThemeProvider>
        </SelectedDateProvider>
      </AuthProvider>
    </ApolloProvider>
  );
}
