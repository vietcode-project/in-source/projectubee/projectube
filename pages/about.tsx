import {
  Grid,
  Typography,
  useMediaQuery,
  Theme,
  Divider,
} from "@material-ui/core";
import React from "react";
import withLayout from "hocs/withLayout";
import { Container } from "next/app";
import AboutHeroSection from "widgets/About/AboutHeroSection";
import AboutProblemSection from "widgets/About/AboutProblem";
import AboutImageDisplay from "widgets/About/AboutImageDisplayer";
import AboutSolutionSection from "widgets/About/AboutSolutionSection";
import AboutActionSection from "widgets/About/AboutActionSection";

function AboutUs() {
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  return (
    <>
      <AboutHeroSection />
      <Container>
        <Grid container style={{ paddingTop: 76 }}>
          <Grid item xs={12}>
            <Typography color="primary" align="center" variant="h1">
              Khởi đầu từ đam mê
            </Typography>
          </Grid>
        </Grid>
        <Grid container justify="center">
          <Grid item xs={9}>
            <Typography align="center" style={{ padding: "60px 0" }}>
              Được thành lập vào tháng 2/2021 bởi 9 bạn trẻ (từ 18-21 tuổi) khát
              khao cống hiến cho cộng đồng, dự án Projectube ra đời với sứ mệnh
              giúp các bạn học sinh cải thiện chất lượng làm hoạt động ngoại
              khóa.
            </Typography>
          </Grid>
        </Grid>
        <AboutImageDisplay />
      </Container>
      <AboutProblemSection />
      <Container>
        <Grid container justify="center" style={{ marginTop: 60 }}>
          <Grid item xs={isDevicePhone ? 10 : 5}>
            {isDevicePhone ? (
              <Typography
                align="center"
                style={{ fontSize: 64 }}
                color="primary"
              >
                <b>Giải pháp từ Projectube</b>
              </Typography>
            ) : (
              <>
                <Typography align="center" variant="h1" color="primary">
                  Giải pháp
                </Typography>
                <Typography align="center" variant="h1" color="primary">
                  từ Projectube
                </Typography>
              </>
            )}
          </Grid>
        </Grid>
        <Grid container justify="center">
          <Grid item xs={9}>
            <Typography
              variant="h3"
              align="center"
              style={{ padding: "60px 0" }}
            >
              Projectube là một nền tảng đa chiều kết nối các tổ chức, sự kiện
              với học sinh, sinh viên trên địa bàn cả nước. Tại đây, những thông
              tin được tổng hợp một cách khoa học, khách quan và chính xác.
              Projectube cung cấp giải pháp tối ưu hóa quy trình hoạt động, giúp
              người dùng tiết kiệm được nguồn lực.
            </Typography>
            <Divider style={{}} />
          </Grid>
        </Grid>
      </Container>
      <AboutSolutionSection />
      <AboutActionSection />
    </>
  );
}

export default withLayout(AboutUs);
