import React, { useEffect, useState } from "react";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { gql, useLazyQuery, useQuery } from "@apollo/client";

import { makeStyles } from "@material-ui/core/styles";
import {
  Grid,
  Typography,
  Button,
  Container,
  useTheme,
  Theme,
  useMediaQuery,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import withLayout from "hocs/withLayout";
import { EssentialData as EventCardData } from "graphql/EventFragment";
import Org from "types/Org";
import Event from "types/Event";
import { OrgSuggestCard } from "components/orgCard";
import {
  GiantEventCard,
  LargeEventCard,
  ResponsiveSideEventCard,
} from "components/eventCard";
import { Title, Content } from "components/header";
import { useAuthState } from "contexts/auth";
import IEvent from "types/Event";
import IEventCard from "types/Event";
import EventFilter from "widgets/EventFilter";
import Loading from "components/Loading";
import shuffleArray from "utils/shuffle";
import { BigSearchField, SearchMobileButton } from "components/Search";
import FeedbackPopUp from "widgets/FeedbackPopUp";
import HeroSection from "widgets/HeroSection";

const useStyles = makeStyles((theme) => ({
  sectionSpacer: {
    margin: theme.spacing(4, 0),
  },
  searchField: {
    display: "flex",
    alignContent: "center",
    justifyContent: "space-between",
    alignItems: "center",
    position: "sticky",
    top: 0,
    zIndex: 1000,
    background: "white",
  },
  search: {
    borderRadius: "6px",
    position: "relative",
  },
  filterButton: {
    borderColor: "#DEE4ED",
    "&:hover": {
      borderColor: "#532BDC",
      cursor: "pointer",
      backgroundColor: "white",
    },
  },
  feature: {
    marginBottom: 32,
    "&:after": {
      content: '""',
      left: 0,
      bottom: 0,
      height: "32px",
      width: "calc(100% - 32px)",
      borderBottom: "1px solid #DEE4ED",
      margin: "auto",
    },
  },
}));

function HomePage() {
  return (
    <>
      <Head>
        <title>Projectube | Home</title>
        <link rel="shortcut icon" href="/favicon.svg" />
      </Head>

      <GuestHome />
    </>
  );
}

interface IFilter {
  location: string;
  start_time: string;
  categories: string[];
}

const filterInitialState: IFilter = {
  location: "",
  start_time: "",
  categories: [],
};
const multiFilter = (arr: IEventCard[], filters: Partial<IFilter>) => {
  const filterKeys = Object.keys(filters);

  return arr.filter((eachObj) => {
    return filterKeys.every((eachKey) => {
      if (!filters[eachKey].length) {
        return true;
      }
      if (eachKey == "start_time") {
        return (
          new Date(filters[eachKey]).toDateString() ==
          new Date(eachObj[eachKey]).toDateString()
        );
      }
      return filters[eachKey].includes(eachObj[eachKey]);
    });
  });
};

function GuestHome() {
  const classes = useStyles();
  const { user } = useAuthState();
  const theme = useTheme();
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down("xs")
  );
  const [openSearch, setOpenSearch] = useState(false);
  const [scrollTop, setScrollTop] = useState(0);
  const [featureEvent, setFeatureEvent] = useState<Event>();

  const [displayEvents, setDisplayEvents] = useState<IEvent[]>([]);

  const [loadingMore, setLoadingMore] = useState(false);

  const { data, loading, error, fetchMore } = useQuery<{
    orgs: Org[];
    events: Event[];
  }>(GUEST_QUERY, { variables: { offset: 0 }, fetchPolicy: "network-only" });

  if (error) {
  }

  /* 
  
  filter Button

  */
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [filter, setFilter] = useState(filterInitialState);
  const [showMoreButton, setshowMoreButton] = useState(false);
  const [showNotMoreEvent, setShowNotMoreEvent] = useState(false);
  const [filteredEvents, setFilterEvents] = useState<IEvent[]>([]);
  const [
    getEventsFiltered,
    {
      loading: loadingFiltered,
      data: dataFiltered,
      fetchMore: fetchMoreFiltered,
    },
  ] = useLazyQuery(EVENTS_FILTER_QUERY);

  useEffect(() => {
    if (
      filter.location !== "" ||
      filter.start_time !== "" ||
      filter.categories.length !== 0
    ) {
      getEventsFiltered({
        variables: {
          filter: {
            categories: filter.categories,
            location: { re: filter.location },
          },
          offset: 0,
          limit: 12,
        },
      });
    }
    {
      dataFiltered && setFilterEvents(dataFiltered.events);
    }
  }, [filter.location, filter.categories, dataFiltered]);

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) =>
      setAnchorEl(event.currentTarget),
    []
  );
  const handleClose = React.useCallback(() => setAnchorEl(null), []);

  const handleInputChange = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
      e.persist();
      setFilter((prev) => ({ ...prev, [e.target.name]: e.target.value }));
    },
    []
  );

  useEffect(() => {
    if (filter.start_time !== "") {
      setFilterEvents(
        multiFilter(displayEvents, { start_time: filter.start_time })
      );
      setshowMoreButton(() => true);
    } else {
      setshowMoreButton(() => false);
    }
  }, [displayEvents, filter.start_time]);

  const handleChooseCategories = React.useCallback(
    (label: string) => {
      if (filter.categories.includes(label)) {
        //bỏ đi category trong filter.categories khi người dùng click lại
        setFilter((prev) => ({
          ...prev,
          categories: prev.categories.filter((c) => c !== label),
        }));
      } else {
        //thêm category ms vào nếu trong filter categories chưa có
        setFilter((prev) => ({
          ...prev,
          categories: [...prev.categories, label],
        }));
      }
    },
    [filter.categories]
  );

  const handleRemoveFilter = React.useCallback(() => {
    setFilter(filterInitialState), setFilterEvents([]);
  }, []);

  const loadMoreFilterd = () => {
    if (loadingMore) return;
    fetchMoreFiltered({
      variables: {
        offset: filteredEvents.length,
        filter: {
          location: { re: filter.location },
          categories: filter.categories,
        },
      },
    }).then(({ data }) => {
      {
        data.events.length == 0 && setShowNotMoreEvent(true);
      }
      setTimeout(() => {
        setShowNotMoreEvent(false);
      }, 3000);
      setFilterEvents((prev) => [...prev, ...data.events]);
    });
  };

  const ref = React.useRef(null);

  useEffect(() => {
    if (data) {
      setFeatureEvent(
        data.events[Math.floor(Math.random() * data.events.length)]
      );
      setDisplayEvents(shuffleArray(data.events));
    }
  }, [data, loading]);

  useEffect(() => {
    const onScroll = (e) => setScrollTop(e.target.documentElement.scrollTop);
    window.addEventListener("scroll", onScroll);

    if (ref && ref.current) {
      const size = ref.current.getBoundingClientRect();
      const bottomPosition = size.y + size.height - screen.height;

      if (bottomPosition < 150 && !loadingMore) loadMore();
    }

    return () => window.removeEventListener("scroll", onScroll);
  }, [scrollTop]);

  const loadMore = () => {
    if (loadingMore) return;

    fetchMore({
      variables: { offset: displayEvents.length, limit: 12 },
    }).then(({ data }) => {
      setDisplayEvents((prev) => [...prev, ...shuffleArray(data.events)]);
      setLoadingMore(() => false);
    });

    setLoadingMore(() => true);
  };

  if (loading)
    return (
      <>
        <Loading />
      </>
    );

  if (error) {
    return <div>Errorrrrr :(</div>;
  }

  return (
    <div ref={ref}>
      <FeedbackPopUp user={user} scrollTop={scrollTop} />
      <Container
        maxWidth="lg"
        style={{ paddingTop: `${!user ? "70px" : "0px"}` }}
      >
        <HeroSection user={user} />
        {/*

         Search Field 
         
         */}
        <div
          style={{
            paddingBottom: "8px",
            paddingTop: "8px",
          }}
          className={classes.searchField}
        >
          <div>
            <div style={{ fontSize: "19px", fontWeight: "bold" }}>
              Các sự kiện
            </div>
          </div>
          {isDevicePhone ? (
            <div>
              <SearchMobileButton
                state={openSearch}
                onClick={() => setOpenSearch(true)}
              />
              <Button
                className={classes.filterButton}
                variant="outlined"
                aria-haspopup="true"
                onClick={(e) => handleClick(e)}
                style={{ padding: "12px", minWidth: "0" }}
              >
                <Image src="/Icon_filter.svg" width={16} height={16} />
              </Button>
            </div>
          ) : (
            <>
              <div
                className={classes.search}
                style={{ zIndex: -1, width: "40%" }}
              >
                <BigSearchField
                  queryevent={EVENT_SEARCH_QUERY}
                  queryorg={ORG_SEARCH_QUERY}
                />
              </div>
              <div>
                <Button
                  className={classes.filterButton}
                  variant="outlined"
                  onClick={handleClick}
                  style={{
                    color: anchorEl && "#532BDC",
                    borderColor: anchorEl && "#532BDC",
                  }}
                  aria-haspopup="true"
                >
                  <Typography variant="button">Bộ lọc sự kiện</Typography>
                </Button>
              </div>
            </>
          )}
        </div>
        {isDevicePhone && (
          <div
            className={classes.search}
            style={{
              display: openSearch ? "block" : "none",
              width: "100%",
              marginBottom: "24px",
            }}
          >
            <BigSearchField
              queryevent={EVENT_SEARCH_QUERY}
              queryorg={ORG_SEARCH_QUERY}
            />
          </div>
        )}

        <EventFilter
          anchorEl={anchorEl}
          filter={filter}
          handleClose={handleClose}
          handleInputChange={handleInputChange}
          handleChooseCategories={handleChooseCategories}
          handleRemoveFilter={handleRemoveFilter}
        />

        {/* Event list */}

        <div>
          <Grid container spacing={2} className={classes.feature}>
            <Grid item xs={12} md={6}>
              {displayEvents && featureEvent && (
                <GiantEventCard
                  name={featureEvent.name}
                  org_name={featureEvent.organizer.name}
                  id={featureEvent.id}
                  start_time={featureEvent.start_time}
                  coverUrl={featureEvent.coverUrl}
                />
              )}
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography
                style={{
                  marginLeft: "8px",
                  fontSize: "16px",
                  paddingTop: "16px",
                  paddingBottom: "8px",
                }}
              >
                Sự kiện dành cho bạn
              </Typography>
              <Grid
                container
                direction="column"
                justify="space-around"
                alignItems="center"
                spacing={1}
              >
                {displayEvents &&
                  displayEvents
                    .slice(0, 3)
                    .map((event) => (
                      <ResponsiveSideEventCard key={event.id} {...event} />
                    ))}
              </Grid>
            </Grid>
          </Grid>

          {/* show EventList */}
          {user && user.type === "User" && (
            <Grid container alignItems="center">
              <Typography
                align="center"
                display="inline"
                style={{
                  marginLeft: theme.spacing(1),
                  fontSize: "24px",
                  fontWeight: "bold",
                  marginBottom: "24px",
                }}>
                Các sự kiện thuộc lĩnh vực bạn yêu thích
              </Typography>
            </Grid>
          )}
          <Grid container spacing={2} style={{marginBottom:"24px"}}> {user &&
              user.type === "User" &&
             user.related_event_by_categories.length>0 && 
              
              user.related_event_by_categories.map((event) => (
                <Grid item xs={12} sm={6} md={3} key={event.id}>
                  <LargeEventCard {...event} />
                </Grid>
              ))}</Grid>
           {user && user.type === "User" && (
            <Grid container alignItems="center">
              <Typography
                align="center"
                display="inline"
                style={{
                  marginLeft: theme.spacing(1),
                  fontSize: "24px",
                  fontWeight: "bold",
                  marginBottom: "24px",
                }}>
                Tất cả sự kiện
              </Typography>
            </Grid>
          )}
          <Grid container spacing={2}>
            {filter.location === "" &&
            filter.start_time === "" &&
            filter.categories.length === 0 ? (
              displayEvents &&
              displayEvents.map((event, i) => {
                if ((i + 1) % 12 == 0 && i > 0) {
                  return (
                    <>
                      <Grid item xs={12} sm={6} md={3} key={event.id}>
                        <LargeEventCard {...event} />
                      </Grid>
                      <Grid item xs={12}>
                        {data.orgs && <OrgSuggestCard orgList={data.orgs} />}
                      </Grid>
                    </>
                  );
                } else {
                  return (
                    <Grid item xs={12} sm={6} md={3} key={event.id}>
                      <LargeEventCard {...event} />
                    </Grid>
                  );
                }
              })
            ) : !filteredEvents.length ? (
              loadingFiltered ? (
                <Loading />
              ) : !filteredEvents.length ? (
                <Typography
                  variant="h4"
                  color="textSecondary"
                  align="center"
                  gutterBottom
                >
                  Không có sự kiện nào phù hợp :(((
                </Typography>
              ) : (
                <Typography variant="h3" gutterBottom>
                  Error :(((
                </Typography>
              )
            ) : (
              <>
                <Grid container spacing={2}>
                  {filteredEvents.map((filteredEvent, i) => (
                    <>
                      <Grid item xs={12} sm={6} md={3} key={filteredEvent.id}>
                        <LargeEventCard {...filteredEvent} />
                      </Grid>
                    </>
                  ))}
                </Grid>
                {showNotMoreEvent && (
                  <div style={{ position: "fixed", bottom: 30, left: 30 }}>
                    <Alert severity="info">Hết sự kiện rùi :((</Alert>
                  </div>
                )}
                {!showMoreButton && (
                  <div
                    style={{
                      width: 300,
                      margin: "0 auto",
                      marginTop: theme.spacing(4),
                    }}
                  >
                    <Button
                      variant="outlined"
                      color="primary"
                      fullWidth
                      onClick={loadMoreFilterd}
                    >
                      Xem thêm
                    </Button>
                  </div>
                )}
              </>
            )}
          </Grid>
        </div>
      </Container>
    </div>
  );
}

const GUEST_QUERY = gql`
  query ($offset: Float!) {
    orgs {
      id
      name
      avatarUrl
      categories
      events {
        coverUrl
        subscribers {
          avatarUrl
        }
        ...EventEssentialFragment
        organizer {
          name
        }
      }
    }
    events(offset: $offset, limit: 12) {
      coverUrl
      subscribers {
        avatarUrl
      }
      ...EventEssentialFragment
      organizer {
        name
      }
    }
  }
  ${EventCardData()}
`;

const EVENTS_FILTER_QUERY = gql`
  query ($offset: Float!, $filter: EventFilterInput, $limit: Float!) {
    events(offset: $offset, limit: $limit, filter: $filter) {
      ...EventEssentialFragment
    }
  }
  ${EventCardData()}
`;

const EVENT_SEARCH_QUERY = gql`
  query ($filterEvent: EventFilterInput) {
    events(filter: $filterEvent) {
      id
      name
      start_time
      categories
      coverUrl
    }
  }
`;

const RECRUIT_SEARCH_QUERY = gql`
  query ($filterRecruitment: RecruitFilterInput) {
    recruitments(filter: $filterRecruitment) {
      id
      title
      deadline
      cover_url
    }
  }
`;

const ORG_SEARCH_QUERY = gql`
  query ($filterOrg: OrgFilterInput) {
    orgs(filter: $filterOrg) {
      id
      name
      avatarUrl
      categories
    }
  }
`;

export default withLayout(HomePage);
