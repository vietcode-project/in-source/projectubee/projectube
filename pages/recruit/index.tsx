import React, { useEffect, useState } from "react";
import withLayout from "hocs/withLayout";
import {
  GiantJobCard,
  LargeJobCard,
  MediumJobCard,
  CreateJobDialog,
} from "../../components/JobCard";
import {
  Button,
  Container,
  Grid,
  makeStyles,
  Typography,
  useMediaQuery,
  Theme,
} from "@material-ui/core";
import { gql, useQuery } from "@apollo/client";
import { EssentialData as RecruitEssData } from "graphql/RecruitmentFragment";
import Loading from "components/Loading";
import IRecruitment from "types/Recruitment";
import shuffleArray from "utils/shuffle";
import { SmallOrgCard } from "components/orgCard";
import { useAuthState } from "contexts/auth";

const useStyles = makeStyles(() => ({
  TopRecruitPage: {
    padding: "14px 0px",
    height: "70px",
  },
  typoTopic: {
    fontSize: "19px",
    fontWeight: "bold",
    color: "#1B2431",
  },
  typoOrgHeader: {
    fontSize: "23px",
    fontWeight: "bold",
    color: "#1B2431",
    margin: "16px 0px",
  },
}));

const Recruit = () => {
  const classes = useStyles();
  const [recruitmentList, setRecruitmentList] = useState<IRecruitment[]>([]);
  const { user } = useAuthState();
  const { loading, data, error } = useQuery(RECRUITMENT_QUERY);
  const [openJob, setOpenJob] = useState(false);
  const isDeviceLaptop = useMediaQuery((theme: Theme) =>
    theme.breakpoints.up("sm")
  );

  useEffect(() => {
    if (data) {
      setRecruitmentList(shuffleArray(data?.recruitments));
    }
  }, [data]);

  if (!data || loading || error) {
    return <Loading />;
  }

  if (data?.recruitments.length === 0) {
    return (
      <Container maxWidth="lg">
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
          className={classes.TopRecruitPage}>
          <Typography className={classes.typoTopic}>Tuyển dụng</Typography>
          {user && user.type === "Org" && (
            <Button
              variant="contained"
              color="primary"
              onClick={() => setOpenJob(true)}>
              Đăng tin
            </Button>
          )}
          <CreateJobDialog
            open={openJob}
            handleClose={() => setOpenJob(false)}
          />
        </Grid>
        <Typography className={classes.typoOrgHeader}>
          Các công việc nổi bật
        </Typography>
        Chưa có tin tuyển dụng nào
      </Container>
    );
  }

  return (
    <Container maxWidth="lg">
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        className={classes.TopRecruitPage}>
        <Typography className={classes.typoTopic}>Tuyển dụng</Typography>
        {user && user.type === "Org" && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => setOpenJob(true)}>
            Đăng tin
          </Button>
        )}
        <CreateJobDialog open={openJob} handleClose={() => setOpenJob(false)} />
      </Grid>

      <Typography className={classes.typoOrgHeader}>
        Các công việc nổi bật
      </Typography>

      <Grid
        container
        direction="row"
        justify="space-between"
        item
        xs={12}>
        <Grid item xs={isDeviceLaptop ? 7 : 12} style={{paddingRight: isDeviceLaptop && "24px"}}>
          <GiantJobCard data={data && recruitmentList[0]}></GiantJobCard>
        </Grid>
        {isDeviceLaptop && (
          <Grid item xs={5}>
            {data?.recruitments.length > 1 && (
              <LargeJobCard data={data && recruitmentList[1]}></LargeJobCard>
            )}
            <br />
            <br />
            {data?.recruitments.length > 1 && (
              <SmallOrgCard org={data && recruitmentList[1]?.org} />
            )}
          </Grid>
        )}
      </Grid>
      <Grid item xs={isDeviceLaptop ? 8 : 12} style={{ marginTop: "64px" }}>
        {data &&
          data?.recruitments.length > 2 &&
          data.recruitments.map((recruitment: IRecruitment, key) => (
            <MediumJobCard key={key} data={recruitment}></MediumJobCard>
          ))}
      </Grid>
    </Container>
  );
};

const RECRUITMENT_QUERY = gql`
  query {
    recruitments {
      ...RecruitmentEssentialFragment
      org {
        id
        name
        categories
        avatarUrl
      }
    }
  }
  ${RecruitEssData()}
`;

export default withLayout(Recruit);
