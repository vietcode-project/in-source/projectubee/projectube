import withLayout from "hocs/withLayout";
import React from "react";
import { DetailedData as RecruitDetailedData } from "graphql/RecruitmentFragment";
import { gql, useQuery } from "@apollo/client";
import { useRouter } from "next/router";
import Loading from "components/Loading";
import {
  Grid,
  Container,
  Button,
  Typography,
  useTheme,
  makeStyles,
  useMediaQuery,
  Theme,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { MediumJobCard } from "components/JobCard";
import { SideSmallOrgCard } from "components/orgCard";
import { useAuthState } from "contexts/auth";
import { renderText } from "utils/renderTextwithLink";

const useStyles = makeStyles(() => ({
  coverImg: {
    height: "381px",
    width: "100%",
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    borderRadius: "12px",
  },
  typoHeader: {
    marginTop: "40px",
    marginBottom: "16px",
    fontSize: "19px",
    color: "#1B2431",
    fontWeight: 400,
  },
  typoDescription: {
    fontSize: "19px",
    color: "#8D9198",
    fontWeight: 400,
  },
}));

const DetailedRecruit = () => {
  const classes = useStyles();
  const router = useRouter();
  const theme = useTheme();
  const { user } = useAuthState();
  const { loading, data, error } = useQuery(RECRUITMENT_QUERY, {
    variables: {
      recruitment_id: router.query.recruit_id,
    },
  });

  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  if (!data || loading || error) {
    return <Loading />;
  }

  return (
    <Container maxWidth="lg">
      <Grid
        container
        alignItems="center"
        justify="space-between"
        direction="row"
        style={{ marginTop: theme.spacing(2) }}
      >
        <Grid container alignItems="center">
          <Button
            onClick={() => router.back()}
            variant="outlined"
            startIcon={<ArrowBackIcon />}
          >
            Quay lại
          </Button>
          <Typography
            display="inline"
            variant="h3"
            style={{ marginLeft: theme.spacing(6) }}
          >
            {data.recruitment.title.slice(0, 80)}
          </Typography>
        </Grid>

        <Grid
          container
          item
          xs={12}
          style={{ marginTop: "24px" }}
          spacing={isDevicePhone ? 0 : 3}
        >
          <Grid item xs={12} md={8}>
            <Grid item xs={12}>
              <div
                style={{
                  backgroundImage: `url(${
                    data?.recruitment?.cover_url
                      ? data?.recruitment?.cover_url
                      : "/temp-cover.jpg"
                  })`,
                }}
                className={classes.coverImg}
              />
            </Grid>
            <Typography className={classes.typoHeader}>
              Mô tả công việc
            </Typography>

            <Grid item xs={12} style={{ marginBottom: "32px" }}>
              <Typography className={classes.typoDescription}>
                {renderText(data?.recruitment?.job_description)}
              </Typography>
            </Grid>

            <Typography className={classes.typoHeader}>
              Các vị trí tuyển dụng khác
            </Typography>

            {data?.recruitment?.org?.recruitments?.length > 0 &&
              data.recruitment.org.recruitments.map((recruitment, key) => (
                <MediumJobCard key={key} data={recruitment} />
              ))}
          </Grid>
          <Grid item xs={12} md={4}>
            <SideSmallOrgCard
              user={user}
              data={data && data?.recruitment.org}
            />
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};

const RECRUITMENT_QUERY = gql`
  query ($recruitment_id: String!) {
    recruitment(recruitment_id: $recruitment_id) {
      ...RecruitmentDetailedFragment
    }
  }
  ${RecruitDetailedData()}
`;

export default withLayout(DetailedRecruit);
