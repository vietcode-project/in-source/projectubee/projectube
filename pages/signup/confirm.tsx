import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { gql, useLazyQuery, useMutation, useQuery } from "@apollo/client";
import {
  Button,
  Grid,
  Typography,
  Modal,
  makeStyles,
  useMediaQuery,
  Theme,
} from "@material-ui/core";

import Auth from "layouts/Auth";
import CateList from "../../components/Catelist";
import cates from "config/categories.json";
import Loading from "components/Loading";

import { EssentialData as OrgEssential } from "graphql/OrgFragment";
import { EssentialData as UserEssential } from "graphql/UserFragment";

import IUser from "../../types/User";

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(5),
  },
  guideText: {
    marginBottom: theme.spacing(3),
  },
}));

export default function Confirm() {
  const router = useRouter();
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down("xs")
  );

  const [user, setUser] = useState<IUser>();

  const [getdata, { data: medata, loading: meloading }] =
    useLazyQuery(ME_MUTATION);

  const [signin] = useMutation(SIGNIN_MUTATION);
  useEffect(() => {
    if (router.query.id) {
      signin({
        variables: {
          confirm_id: router.query.id,
        },
      })
        .then((res) => {
          localStorage.setItem("refresh_token", res?.data.signin.refresh_token);
          localStorage.setItem("access_token", res?.data.signin.access_token);
          getdata();
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [router.query.id]);

  useEffect(() => {
    setUser(medata?.me);
  }, [router.query.id, medata]);

  const [
    updateProfile,
    { data: updateUserData, loading: updateLoading, error: updateError },
  ] = useMutation(UPDATE_PROFILE_MUTATION);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = async () => {
    await updateProfile({
      variables: {
        name: user.name,
        interests: user.interests,
      },
    }).catch((err) => console.error(err));
  };

  if (updateUserData) {
    router.push("/");
  }

  const classes = useStyles();

  const body = user && (
    <div
      style={{
        transform: isDevicePhone ? "translate(0,15%)" : "translate(30%, 15%)",
        width: isDevicePhone ? "100%" : "65%",
      }}
      className={classes.paper}
    >
      <Typography
        variant="h2"
        style={{ marginTop: "16px", marginBottom: "16px" }}
      >
        Trước khi bắt đầu, bạn hãy chọn các chủ đề yêu thích!
      </Typography>
      <Typography color="textSecondary" className={classes.guideText}>
        Hãy lựa chọn những chủ đề mà bạn yêu thích để Projectube đưa ra những
        lựa chọn phù hợp hơn!
      </Typography>
      <CateList
        cates={cates}
        selected={user.interests}
        handleChoose={(label) => {
          if (user.interests.includes(label)) {
            setUser((prev) => ({
              ...prev,
              interests: prev.interests.filter((c) => c !== label),
            }));
          } else {
            setUser((prev) => ({
              ...prev,
              interests: [...prev.interests, label],
            }));
          }
        }}
      />
      <Grid container spacing={2} style={{ marginTop: 24 }}>
        <Grid item>
          <Link href="/">
            <a>
              <Button variant="outlined" color="primary">
                Bỏ qua
              </Button>
            </a>
          </Link>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleClick()}
            disabled={
              (user.interests && !user.interests.length) ||
              updateLoading ||
              (!updateLoading && updateUserData)
            }
          >
            Lưu thay đổi
          </Button>
        </Grid>
      </Grid>
    </div>
  );
  return user ? (
    <Grid container style={{ height: "100vh" }}>
      <Grid item xs={6} style={{ display: isDevicePhone ? "none" : "" }}>
        <div
          style={{
            backgroundImage: `url("/Signinimg.svg")`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            height: "100%",
          }}
        ></div>
      </Grid>
      <Grid item xs={isDevicePhone ? 12 : 6}>
        <Auth>
          <Typography
            variant="h2"
            style={{ marginTop: "16px", marginBottom: "16px" }}
          >
            Đăng ký tài khoản thành công!
          </Typography>
          <Typography variant="h4" color="textSecondary">
            Bạn đã chính thức trở thành thành viên của cộng đồng Projectube!
          </Typography>
          <Grid container spacing={2} style={{ marginTop: 24 }}>
            <Grid item>
              {user.type === "User" ? (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => handleOpen()}
                >
                  Quay về trang chủ
                </Button>
              ) : null}
              <Modal
                open={open}
                onClose={() => handleClose()}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                style={{ overflow: "scroll" }}
              >
                {body}
              </Modal>
              {user.type === "Org" ? (
                <Link href="/myorg">
                  <a>
                    <Button variant="contained" color="primary">
                      Cài đặt tổ chức
                    </Button>
                  </a>
                </Link>
              ) : null}
            </Grid>
            <Grid item>
              <Link href="/about">
                <a>
                  <Button variant="outlined" color="primary">
                    Về chúng tôi
                  </Button>
                </a>
              </Link>
            </Grid>
          </Grid>
        </Auth>
      </Grid>
    </Grid>
  ) : (
    <Loading />
  );
}

const SIGNIN_MUTATION = gql`
  mutation ($confirm_id: String) {
    signin(confirm_id: $confirm_id) {
      access_token
      refresh_token
    }
  }
`;

const ME_MUTATION = gql`
  query {
    me {
      ...UserEssentialFragment
      ...OrgEssentialFragment
    }
  }
  ${OrgEssential()}
  ${UserEssential()}
`;

const UPDATE_PROFILE_MUTATION = gql`
  mutation ($name: String!, $interests: [String!]) {
    update_profile(data: { name: $name, interests: $interests }) {
      interests
    }
  }
`;
