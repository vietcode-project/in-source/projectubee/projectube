import React, { ReactElement, useState } from "react";
import {
  Grid,
  Button,
  Typography,
  useMediaQuery,
  Theme,
} from "@material-ui/core";

import Auth from "layouts/Auth";
import CateList from "../../components/Catelist";
import SignUpForm from "widgets/SignUpForm";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Link from "next/link";

export default function SignUp(): ReactElement {
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down("xs")
  );
  const [nextStep, setNextStep] = useState(false);
  const [userData, setUserData] = useState({
    email: "",
    password: "",
    name: "",
    phone_number: "",
    type: "",
  });

  return (
    <Grid container style={{ height: "100vh" }}>
      <Grid item xs={6} style={{ display: isDevicePhone ? "none" : "" }}>
        <div
          style={{
            backgroundImage: `url("/Signinimg.svg")`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            height: "100%",
          }}
        ></div>
      </Grid>
      <Grid item xs={isDevicePhone ? 12 : 6}>
        <Auth>
          <Link href="/">
            <a>
              <Button
                style={{ padding: "12px 16px" }}
                variant="outlined"
                startIcon={<ArrowBackIcon />}
              >
                <Typography variant="h5">Quay lại trang chủ</Typography>
              </Button>
            </a>
          </Link>
          {!nextStep ? (
            <TypeSelection
              type={userData.type}
              setUserData={setUserData}
              setNextStep={setNextStep}
            />
          ) : (
            <>
              <SignUpForm userData={userData} setUserData={setUserData} />
            </>
          )}
        </Auth>
      </Grid>
    </Grid>
  );
}

function TypeSelection(props) {
  let types = ["Cá nhân", "Tổ chức/Doanh nghiệp"];
  let [userType, setType] = useState(props.type);
  const handleNext = () => {
    if (userType === "Cá nhân") {
      props.setUserData((prev) => ({
        ...prev,
        type: "user",
      }));
    } else {
      props.setUserData((prev) => ({
        ...prev,
        type: "org",
      }));
    }
    props.setNextStep(true);
  };
  return (
    <>
      <Typography variant="h2" style={{ marginTop: "16px" }}>
        Chào mừng bạn đến với Projectube!
      </Typography>
      <Typography style={{ margin: "16px 0", color: "#8D9198" }}>
        Tôi tham gia Projectube với vai trò
      </Typography>
      <CateList
        cates={types}
        selected={userType}
        handleChoose={(label) => setType(label)}
      />
      <Button
        onClick={() => handleNext()}
        style={{ marginTop: "24px" }}
        variant="contained"
        color="primary"
        disabled={!userType}
        size="large"
      >
        Tiếp tục
      </Button>
    </>
  );
}
