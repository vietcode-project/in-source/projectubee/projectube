import React, { useState } from "react";
import Link from "next/link";
import { gql, useMutation } from "@apollo/client";
import Auth from "layouts/Auth";
import {
  Button,
  Typography,
  Grid,
  useMediaQuery,
  Theme,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useRouter } from "next/router";

export default function VerifyYourEmail() {
  const [message, setMessage] = useState(
    "Vui lòng xác nhận tài khoàn của bạn trong email"
  );
  const router = useRouter();

  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.down("xs")
  );

  const [re_verify] = useMutation(VERIFICATION_MUTATION, {
    variables: {
      email: router.query.email,
    },
  });
  const handleClick = async () => {
    if (router.query.email) {
      const { data, errors } = await re_verify();
      if (data) {
        setMessage("Chúng tôi đã gửi lại thông báo xác nhận cho bạn");
      }
      if (errors) {
      }
    }
  };

  return (
    <Grid container style={{ height: "100vh" }}>
      <Grid item xs={6} style={{ display: isDevicePhone ? "none" : "" }}>
        <div
          style={{
            backgroundImage: `url("/Signinimg.svg")`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            height: "100%",
          }}
        ></div>
      </Grid>
      <Grid item xs={isDevicePhone ? 12 : 6}>
        <Auth>
          <Link href="/signup">
            <a>
              <Button
                style={{ padding: "12px 16px", borderRadius: "6px" }}
                variant="outlined"
                startIcon={<ArrowBackIcon />}
              >
                <Typography variant="h5">Sử dụng email khác</Typography>
              </Button>
            </a>
          </Link>
          <Typography
            variant="h2"
            style={{ marginTop: "16px", marginBottom: "8px" }}
          >
            {message}
          </Typography>
          <Button
            style={{ border: "none", padding: "0" }}
            variant="outlined"
            onClick={handleClick}
          >
            <Typography variant="h5" color="primary">
              Tôi không nhận được thông báo nào
            </Typography>
          </Button>
        </Auth>
      </Grid>
    </Grid>
  );
}

const VERIFICATION_MUTATION = gql`
  mutation ($email: String!) {
    resend_verify_email(email: $email)
  }
`;
