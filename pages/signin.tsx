import React, { ChangeEvent, useEffect, useMemo, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import {
  Button,
  TextField,
  Grid,
  makeStyles,
  Typography,
  useTheme,
  useMediaQuery,
  Theme,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { gql, useMutation } from "@apollo/client";

import { useAuthDispatch, useAuthState } from "contexts/auth";
import Auth from "layouts/Auth";
import { validateEmail, validatePassword } from "utils/validators";

interface AuthInput {
  email: string;
  password: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  noti: {
    color: theme.palette.primary.main,
  },
  errnoti: {
    color: "red",
  },
  homeButton: {
    width: "40%",
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  label: {
    margin: 0,
    lineHeight: 0,
  },
  textField: {
    marginBottom: theme.spacing(3),
  },
  inputField: {
    margin: theme.spacing(1, 0),
  },
}));

export default function Signin() {
  const router = useRouter();
  const theme = useTheme();
  const classes = useStyles();
  const dispatch = useAuthDispatch();
  const { user, errorMessage } = useAuthState();
  const [signin, { error, loading, data }] = useMutation(SIGNIN_MUTATION);
  const [typed, setTyped] = useState({ email: false, password: false });

  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  const [signinInfo, setLoginInfo] = useState<AuthInput>({
    email: "",
    password: "",
  });

  const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
    const { name, value } = e.target;
    setLoginInfo((prev) => ({
      ...prev,
      [name]: value,
    }));

    setTyped((prev) => ({ ...prev, [name]: true }));
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    try {
      await signin({
        variables: signinInfo,
      }).then((res) => {
        localStorage.setItem("refresh_token", res?.data.signin.refresh_token);
        localStorage.setItem("access_token", res?.data.signin.access_token);
      });
    } catch (err) {}
  };

  useEffect(() => {
    if (!loading) {
      if (data && data.signin) {
        dispatch({
          type: "SET_USER",
          payload: data.signin,
        });
        router.push("/");
      }
    }
  }, [data, loading]);

  if (user) {
    router.replace("/");
  }

  const valid = useMemo(() => {
    const email = validateEmail(signinInfo.email);
    const password = validatePassword(signinInfo.password);

    return {
      email,
      password,
    };
  }, [signinInfo]);

  return (
    <Grid container style={{ height: "100vh" }}>
      <Grid item xs={6} style={{ display: isDevicePhone ? "none" : "" }}>
        <div
          style={{
            backgroundImage: `url("/Signinimg.svg")`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
            height: "100%",
          }}
        ></div>
      </Grid>
      <Grid item xs={isDevicePhone ? 12 : 6}>
        <Auth>
          <Link href="/">
            <a>
              <Button
                style={{ padding: "12px 16px" }}
                variant="outlined"
                startIcon={<ArrowBackIcon />}
              >
                <Typography variant="h5">Quay lại trang chủ</Typography>
              </Button>
            </a>
          </Link>

          <Typography
            variant="h2"
            style={{ marginTop: "16px", marginBottom: "16px" }}
          >
            Đăng nhập Projectube
          </Typography>

          <div className={classes.noti}>
            {loading && "Signing in, please wait"}
          </div>
          <div className={classes.errnoti}>
            {error && error.message}
            {errorMessage}
          </div>

          <form onSubmit={handleSubmit}>
            <Typography variant="h5">Email</Typography>
            <TextField
              name="email"
              variant="outlined"
              placeholder="Nhập địa chỉ email"
              className={classes.inputField}
              value={signinInfo.email}
              onChange={handleChange}
              error={!valid.email && typed.email}
              autoFocus
              required
              fullWidth
            />
            <Typography variant="h5">Mật khẩu</Typography>
            <TextField
              name="password"
              variant="outlined"
              placeholder="Nhập mật khẩu"
              className={classes.inputField}
              type="password"
              value={signinInfo.password}
              onChange={handleChange}
              error={!valid.password && typed.password}
              required
              fullWidth
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={
                !Object.values(valid).reduce((acc, cur) => acc && cur, true)
              }
              size="large"
            >
              Đăng nhập
            </Button>
          </form>
          <Grid container className={classes.root}>
            <Grid item xs>
              <Typography variant="body2">
                Bạn là thành viên mới?&nbsp;
                <Link href="/signup">
                  <a style={{ color: theme.palette.primary.main }}>
                    Đăng kí ngay
                  </a>
                </Link>
              </Typography>
            </Grid>
            {/* 
              <Grid item>
                <Link href="#" variant="body2">
                  {"Forgot password"}
                </Link>
              </Grid>
              */}
          </Grid>
        </Auth>
      </Grid>
    </Grid>
  );
}

const SIGNIN_MUTATION = gql`
  mutation ($email: String!, $password: String!) {
    signin(credential: { email: $email, password: $password }) {
      access_token
      refresh_token
    }
  }
`;
