import React, { useEffect, useState } from "react";
import { gql, useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import {
  Container,
  Grid,
  Button,
  Typography,
  TextField,
  Avatar,
  Select,
  MenuItem,
  Switch,
  makeStyles,
  InputAdornment,
  useMediaQuery,
  Theme,
  Divider,
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import CateList from "components/Catelist";
import withLayout from "hocs/withLayout";
import { useAuthState } from "contexts/auth";
import { validateName, validatePhoneNumber } from "utils/validators";
import cates from "config/categories.json";
import convert from "widgets/Convert";
import { ME_QUERY } from "graphql/Queries";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    marginTop: 34,
  },
  inputField: {
    marginTop: 8,
    marginBottom: 16,
  },
  select: {
    background: "white",
    fontWeight: 200,
    height: 36,
    "& > div": { padding: "7.5px 0 7.5px 16px" },
  },
  tick: {
    fontSize: "15px",
    color: "#532BDC",
  },
  divider: {
    margin: theme.spacing(4, 0),
    backgroundColor: "#DEE4ED",
    height: "2px",
    width: "100%",
  },
}));

interface IInput {
  name: string;
  avatar: File;
  phone_number: string | null;
  dob: Date | string | null;
  interests: string[] | null;
}

function EditProfile() {
  const router = useRouter();
  const classes = useStyles();
  const { user, loading } = useAuthState();
  const [avatarPreview, setAvatarPreview] = useState("");
  const [typed, setTyped] = useState({
    name: false,
    phone_number: false,
  });
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );
  const [locked, setLocked] = useState(false);
  const [categories, setCategories] = useState([]);

  const [input, setInput] = useState<IInput>({
    name: "",
    phone_number: "",
    avatar: null,
    dob: null,
    interests: null,
  });

  const [updateProfile, { data, loading: loading1, error }] = useMutation(
    UPDATE_PROFILE_MUTATION,
    {
      refetchQueries: [{ query: ME_QUERY }, "me"],
    }
  );

  useEffect(() => {
    if (!loading && !user) {
      router.push("/signin");
    }
    user &&
      setInput({
        name: user.name,
        phone_number: user.phone_number,
        avatar: null,
        dob: convert(new Date(user.dob)) || null,
        interests: user.interests || null,
      });
    user && setAvatarPreview(user.avatarUrl);
    user && setCategories(user.interests);
  }, [loading, user]);

  const valid = React.useMemo(() => {
    const name = validateName(input.name);
    const phone_number = validatePhoneNumber(input.phone_number);
    return {
      name,
      phone_number,
    };
  }, [input]);

  useEffect(() => {
    console.log(categories);
    console.log(input);
    setInput((prev) => ({ ...prev, interests: categories }));
  }, [categories]);

  const removeNullPoint = (obj: Object) => {
    return Object.keys(obj)
      .filter((k) => obj[k] != null)
      .reduce((a, k) => ({ ...a, [k]: obj[k] }), {});
  };

  function onSelectAvatar(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setInput((prev) => ({ ...prev, avatar: e.target.files[0] }));
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setAvatarPreview(objectUrl);
  }

  function handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.persist();
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }));
    setTyped((prev) => ({ ...prev, [e.target.name]: true }));
  }

  async function handleSubmit(e: any) {
    e && e.preventDefault();
    if (!locked) {
      updateProfile({
        variables: {
          ...removeNullPoint(input),
        },
      }).catch((err) => console.error(err));
      setLocked(true);
    }
  }

  if (data) {
    if (locked) setLocked(() => false);
    router.push("/profile");
  }

  if (error && locked) {
    setLocked(() => false);
  }

  return (
    <Container maxWidth="lg" className={classes.root}>
      <Grid container alignItems="center" style={{ marginBottom: 40 }}>
        <Button
          onClick={() => router.back()}
          variant="outlined"
          startIcon={<ArrowBackIcon />}
        >
          <Typography variant="button">Quay lại</Typography>
        </Button>
        <Typography
          variant="h2"
          align="center"
          display="inline"
          style={{ marginLeft: 8 }}
        >
          Cài Đặt
        </Typography>
      </Grid>
      <Grid container>
        <Grid
          item
          xs={isDevicePhone ? 12 : 5}
          style={{
            borderRight: !isDevicePhone && "1px solid #DEE4ED",
            paddingRight: 24,
          }}
        >
          <Typography variant="h3" style={{ marginBottom: 32 }}>
            <b>Thông tin cơ bản</b>
          </Typography>
          <Typography variant="body1">Email của bạn</Typography>
          <Typography
            variant="body2"
            color="textSecondary"
            style={{ marginBottom: 16 }}
          >
            Mọi thông tin mới nhất sẽ được gửi vào tài khoản bạn
          </Typography>
          <Grid
            container
            direction={isDevicePhone ? "column" : "row"}
            alignItems={isDevicePhone ? "flex-start" : "center"}
          >
            <Grid
              item
              xs={6}
              style={{ marginBottom: isDevicePhone && " 10px" }}
            >
              <Typography color="textSecondary">
                {user && user.email && user?.email}
              </Typography>
            </Grid>
            <Button variant="outlined" style={{ height: "40px" }}>
              <Typography variant="h5">Đổi mật khẩu</Typography>
            </Button>
          </Grid>
          <Grid style={{ marginTop: 24, marginBottom: 24 }}>
            <Typography variant="h5">Avatar tổ chức</Typography>
            <Typography variant="body2" color="secondary" gutterBottom>
              Dung lượng ảnh tối đa là 2MB
            </Typography>
            <Avatar
              src={avatarPreview}
              style={{
                width: 100,
                height: 100,
                margin: "16px 0",
              }}
            />
            <Button variant="outlined" component="label">
              <Typography variant="button">
                Chọn ảnh từ thiết bị của bạn{" "}
              </Typography>
              <input
                type="file"
                name="avatarUrl"
                accept="image/png, image/jpeg"
                hidden
                onChange={onSelectAvatar}
              />
            </Button>
          </Grid>

          <Typography variant="h5">Họ và tên</Typography>
          <TextField
            variant="outlined"
            color="primary"
            name="name"
            placeholder="Nhập tên của bạn"
            autoFocus
            fullWidth
            className={classes.inputField}
            onChange={handleInputChange}
            value={input.name}
            error={!valid.name && typed.name}
            InputProps={{
              endAdornment: valid.name && (
                <InputAdornment position="start">
                  <CheckIcon className={classes.tick} />
                </InputAdornment>
              ),
            }}
          />
          <Typography variant="h5">Số điện thoại</Typography>
          <TextField
            variant="outlined"
            color="primary"
            name="phone_number"
            placeholder="Nhập sđt của bạn"
            autoFocus
            fullWidth
            className={classes.inputField}
            type="number"
            onChange={handleInputChange}
            value={input.phone_number}
            error={!valid.phone_number && typed.phone_number}
            InputProps={{
              endAdornment: valid.phone_number && (
                <InputAdornment position="start">
                  <CheckIcon className={classes.tick} />
                </InputAdornment>
              ),
            }}
          />

          <Typography variant="h5">Ngày sinh</Typography>
          <TextField
            className={classes.inputField}
            onChange={handleInputChange}
            value={input.dob}
            variant="outlined"
            fullWidth
            name="dob"
            type="date"
          ></TextField>
        </Grid>
        {isDevicePhone && <Divider className={classes.divider} />}
        <Grid
          item
          xs={isDevicePhone ? 12 : 7}
          style={{ paddingLeft: !isDevicePhone && 24 }}
        >
          <Typography style={{ marginBottom: 32 }} variant="h3">
            <b>Thông tin nâng cao</b>
          </Typography>
          <Grid container direction="row">
            <Grid item xs={9}>
              <Typography variant="body1">Lựa chọn ngôn ngữ</Typography>
              <Typography
                variant="body2"
                color="textSecondary"
                style={{ marginBottom: 16 }}
              >
                English coming soon
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <Select
                className={classes.select}
                fullWidth
                MenuProps={{
                  getContentAnchorEl: null,
                  anchorOrigin: {
                    vertical: "bottom",
                    horizontal: "left",
                  },
                }}
                value={"Tiếng Việt"}
                variant="outlined"
              >
                <MenuItem value={"Tiếng Việt"} key={"Tiếng Việt"}>
                  Tiếng Việt
                </MenuItem>
                <MenuItem value={"Tiếng Anh"} key={"Tiếng Anh"}>
                  Tiếng Anh
                </MenuItem>
              </Select>
            </Grid>
            <Grid container direction="row">
              <Grid item xs={9}>
                <Typography variant="body1">Lưu trữ thông báo</Typography>
                <Typography
                  variant="body2"
                  color="textSecondary"
                  style={{ marginBottom: 16 }}
                >
                  Feature coming soon
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <Select
                  className={classes.select}
                  fullWidth
                  MenuProps={{
                    getContentAnchorEl: null,
                    anchorOrigin: {
                      vertical: "bottom",
                      horizontal: "left",
                    },
                  }}
                  value={"Không"}
                  variant="outlined"
                >
                  <MenuItem
                    style={{ height: 36 }}
                    value={"1 tháng"}
                    key={"1 tháng"}
                  >
                    01 tháng
                  </MenuItem>
                  <MenuItem value={"1 năm"} key={"1 năm"}>
                    01 năm
                  </MenuItem>
                  <MenuItem value={"Không"} key={"Không"}>
                    Không
                  </MenuItem>
                </Select>
              </Grid>
            </Grid>
          </Grid>

          <Grid
            container
            direction="row"
            alignItems="center"
            justify="space-between"
          >
            <Grid item xs={11}>
              <Typography variant="button">
                Nhận thông báo mới qua email
              </Typography>
              <Typography
                variant="body2"
                color="secondary"
                gutterBottom
                style={{ marginBottom: 16 }}
              >
                Thông báo mới từ Projectube sẽ luôn được cập nhật và gửi đến
                email của bạn
              </Typography>
            </Grid>
            <Grid item xs={1}>
              <Switch name="checkedB" color="primary" />
            </Grid>
          </Grid>
          <Grid
            container
            direction="row"
            alignItems="center"
            justify="space-between"
          >
            <Grid item xs={11}>
              <Typography variant="button">Gợi ý dựa theo vị trí </Typography>
              <Typography
                variant="body2"
                color="secondary"
                gutterBottom
                style={{ marginBottom: 16 }}
              >
                Các sự kiện được gọi ý dựa trên vị trí hiện tại của bạn
              </Typography>
            </Grid>
            <Grid item xs={1}>
              <Switch name="checkedB" color="primary" />
            </Grid>
          </Grid>
          <Typography>Gợi ý dựa theo chủ đề</Typography>
          <Typography
            variant="body2"
            color="secondary"
            gutterBottom
            style={{ marginBottom: 16 }}
          >
            Việc lựa chọn chủ đề yêu thích sẽ giúp bạn tiếp cận được nhiều sự
            kiện phù hợp hơn
          </Typography>
          <CateList
            cates={cates}
            selected={categories}
            handleChoose={(label) => {
              if (categories.includes(label)) {
                setCategories((prev) => prev.filter((c) => c !== label));
              } else {
                setCategories((prev) => [...prev, label]);
              }
            }}
          />
          <Typography variant="h3" style={{ marginTop: 40, marginBottom: 32 }}>
            <b>Projectube Premium</b>
          </Typography>
          <Grid
            container
            direction="row"
            alignItems="center"
            justify="space-between"
            style={{ marginBottom: 16 }}
          >
            <Grid item xs={11}>
              <Typography variant="button">
                Tham gia sự kiện qua email{" "}
              </Typography>
              <Typography variant="body2" color="secondary" gutterBottom>
                Coming soon, please wait
              </Typography>
            </Grid>
            <Grid item xs={1}>
              <Switch disabled name="checkedB" color="primary" />
            </Grid>
          </Grid>
          <Grid
            container
            direction="row"
            alignItems="center"
            justify="space-between"
            style={{ marginBottom: 16 }}
          >
            <Grid item xs={11}>
              <Typography variant="button">
                Projectube Recommendation System
              </Typography>
              <Typography variant="body2" color="secondary" gutterBottom>
                Coming soon, please wait
              </Typography>
            </Grid>
            <Grid item xs={1}>
              <Switch disabled name="checkedB" color="primary" />
            </Grid>
          </Grid>
          <Grid
            container
            direction="row"
            alignItems="center"
            justify="space-between"
            style={{ marginBottom: 16 }}
          >
            {/* <Grid item xs={11}>
              <Typography variant="button">Incomming</Typography>
              <Typography variant="body2" color="secondary" gutterBottom>
                Coming soon, please wait
              </Typography>
            </Grid>
            <Grid item xs={1}>
              <Switch name="checkedB" color="primary" />
            </Grid> */}
          </Grid>
        </Grid>
      </Grid>
      <Grid
        style={{
          marginTop: 24,
          paddingTop: 24,
          borderTop: "1px solid #DEE4ED",
        }}
        container
        justify="flex-end"
      >
        <Button
          variant="contained"
          color="primary"
          style={{
            padding: "9px 20px",
          }}
          disabled={
            !(
              Object.values(valid).reduce((acc, cur) => acc && cur, true) &&
              !locked
            )
          }
          onClick={handleSubmit}
        >
          <Typography variant="h5">Lưu thay đổi</Typography>
        </Button>
      </Grid>
    </Container>
  );
}

const UPDATE_PROFILE_MUTATION = gql`
  mutation (
    $name: String
    $phone_number: String
    $avatar: Upload
    $interests: [String!]
    $dob: DateTime
  ) {
    update_profile(
      data: {
        name: $name
        phone_number: $phone_number
        avatar: $avatar
        interests: $interests
        dob: $dob
      }
    ) {
      name
    }
  }
`;

export default withLayout(EditProfile);
