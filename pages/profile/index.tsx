import { useRouter } from "next/router";
import withLayout from "hocs/withLayout";
import React, { ReactElement, useState } from "react";
import {
  Button,
  Container,
  Grid,
  Typography,
  makeStyles,
  Divider,
  useTheme,
  Avatar,
} from "@material-ui/core";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { SubbedOrgCard } from "../../components/orgCard";
import Calendar from "../../components/Calendar/Calendar";
import { CalendarSubeventCard } from "../../components/eventCard";
import { useAuthState } from "contexts/auth";
import { useDate } from "components/Calendar";

const useStyles = makeStyles((theme) => ({
  avatar: {
    width: "100px",
    height: "100px",
  },
}));

function userProfile(): ReactElement {
  const classes = useStyles();
  const router = useRouter();
  const theme = useTheme();
  const { user, loading } = useAuthState();
  const { selectedDate } = useDate();

  const [expand, setExpand] = useState(true);

  React.useEffect(() => {
    if (!loading && !user) {
      router.push("/signin");
    }
  }, [loading, user]);

  return (
    <Container maxWidth="lg">
      <Grid
        container
        alignItems="center"
        style={{ marginTop: theme.spacing(8) }}
      >
        <Button
          onClick={() => router.back()}
          variant="outlined"
          startIcon={<ArrowBackIcon />}
        >
          Quay lại
        </Button>
        <Typography
          display="inline"
          style={{
            marginLeft: theme.spacing(2),
            fontSize: "24px",
            lineHeight: "30px",
            fontWeight: "bold",
          }}
        >
          Thông tin cá nhân
        </Typography>
      </Grid>
      <Grid
        container
        spacing={1}
        justify="space-between"
        style={{ padding: "24px 0px", margin: "0px" }}
      >
        <Grid item>
          <Grid container direction="row" justify="space-between">
            <Grid item>
              <Avatar src={user?.avatarUrl} className={classes.avatar}></Avatar>
            </Grid>
            <Grid item style={{ marginLeft: "16px" }}>
              <Typography variant="h2">{user && user.name}</Typography>
              <Typography variant="body1" color="secondary">
                {user?.phone_number} &bull; {user?.email}
              </Typography>
              <Typography
                variant="body1"
                color="secondary"
                style={{ marginTop: "16px" }}
              >
                <strong style={{ color: "black" }}>
                  {user?.subscribing_events?.length || 0} sự kiện{" "}
                </strong>
                đã quan tâm
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Button href="profile/edit" variant="outlined">
            Cài đặt
          </Button>
        </Grid>
      </Grid>
      <Divider
        style={{ marginBottom: "24px", background: "#DEE4ED" }}
      ></Divider>
      <Grid container spacing={4}>
        <Grid item xs={12} md={8}>
          <Grid item container xs={12}>
            <Grid item xs={4}>
              <Typography
                style={{
                  fontSize: "19px",
                  color: "#1B2431",
                  fontWeight: 400,
                  borderRight: "1px solid #DEE4ED",
                }}
              >
                Tổ chức đang theo dõi
              </Typography>
            </Grid>
            {/* <Grid item xs={4}>
              <Typography
                style={{
                  fontSize: "19px",
                  color: "#8D9198",
                  fontWeight: 400,
                  paddingLeft: "27px",
                  borderRight:"1px solid #DEE4ED"
                }}>
                Người đang theo dõi
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography
                style={{
                  fontSize: "19px",
                  color: "#8D9198",
                  fontWeight: 400,
                  paddingLeft: "27px",
                }}>
                Tin tuyển dụng đã lưu
              </Typography>
            </Grid> */}
          </Grid>
          <Grid>
            {user?.subscribing_orgs?.map((org) => (
              <SubbedOrgCard key={org.id} org={org}></SubbedOrgCard>
            ))}
          </Grid>
        </Grid>
        <Grid item xs={12} md={4} style={{ borderLeft: "1px solid #DEE4ED" }}>
          <Grid container alignItems="center" style={{ marginBottom: 32 }}>
            <Typography variant="h2">
              <strong>Lịch cá nhân</strong>
            </Typography>
            <Button style={{ opacity: 0, cursor: "default" }}>a</Button>
          </Grid>
          <Grid>
            <Calendar />
          </Grid>

          <Grid>
            <Typography color="primary" style={{ marginTop: 16 }}>
              Sự kiện trong ngày
            </Typography>
            {expand ? (
              <Grid style={{ marginTop: 16 }}>
                {user?.subscribing_events
                  ?.filter(
                    ({ start_time }) =>
                      new Date(start_time).toDateString() ==
                      selectedDate.toDateString()
                  )
                  .map((event, i) => (
                    <CalendarSubeventCard
                      key={i}
                      id={event.id}
                      coverUrl={event.coverUrl || "/temp-cover.jpg"}
                      name={event.name}
                      categories={event.categories.slice(0, 3)}
                      start_time={event.start_time}
                    ></CalendarSubeventCard>
                  )) || <></>}
              </Grid>
            ) : (
              <Grid style={{ marginTop: 16 }}>
                {user?.subscribing_events.map((event, i) => (
                  <CalendarSubeventCard
                    key={i}
                    id={event.id}
                    coverUrl={event.coverUrl || "/temp-cover.jpg"}
                    name={event.name}
                    categories={event.categories.slice(0, 3)}
                    start_time={event.start_time}
                  ></CalendarSubeventCard>
                )) || <></>}
              </Grid>
            )}
            {
              <>
                {!user?.subscribing_events?.some(
                  ({ start_time }) =>
                    new Date(start_time).toDateString() ==
                    selectedDate.toDateString()
                ) &&
                  (expand ? (
                    <Typography variant="body1" color="textPrimary">
                      Không có sự kiện nào trong ngày :(
                    </Typography>
                  ) : (
                    ""
                  ))}
              </>
            }
            <Button
              variant="outlined"
              fullWidth
              onClick={() => {
                setExpand(!expand);
              }}
            >
              {expand ? "Xem thêm" : "Rút gọn"}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
}

export default withLayout(userProfile);
