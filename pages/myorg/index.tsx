import React, { ReactElement, useEffect, useState } from "react";
import { useRouter } from "next/router";

import {
  Button,
  Container,
  Grid,
  makeStyles,
  useTheme,
  Typography,
  useMediaQuery,
  Theme,
} from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import { useAuthState } from "contexts/auth";
import withLayout from "hocs/withLayout";
// import { OrgTrackingCard } from "components/TrackingCard";
import {
  CreateEventDialogs,
  ResponsiveSideEventCard,
} from "components/eventCard";
import { OrgTrackingCard } from "components/orgCard";
import { CreateJobDialog, MediumJobCard } from "components/JobCard";

const useStyles = makeStyles((theme) => ({
  navbarSpacing: {
    height: theme.spacing(7),
    margin: 0,
  },
  pageHeader: {
    display: "flex",
    alignItems: "center",
    marginBottom: theme.spacing(3),
  },
  myOrgCard: {
    border: "1px solid #DEE4ED",
    borderRadius: "8px",
    padding: theme.spacing(3, 4),
    position: "relative",
  },
  cardImage: {
    width: 145,
    height: 145,
    borderRadius: "50%",
    margin: "0 auto",
    backgroundPosition: "center",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
  },
}));

function MyOrg(): ReactElement {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const [open, setOpen] = useState(false);
  const [openJob, setOpenJob] = useState(false);

  const { user, loading: userLoading } = useAuthState();
  const isDevicePhone = useMediaQuery((theme: Theme) =>
    theme.breakpoints.only("xs")
  );

  React.useEffect(() => {
    if (!userLoading && !user) {
      router.push("/signin");
    }
  }, [user, userLoading]);

  return (
    <Container maxWidth="lg">
      <div className={classes.navbarSpacing} />
      <div className={classes.pageHeader}>
        <Button
          onClick={() => router.back()}
          variant="outlined"
          startIcon={<ArrowBackIcon />}
        >
          <Typography
            variant="button"
            style={{ fontSize: isDevicePhone ? "12px" : "16px" }}
          >
            Quay lại
          </Typography>
        </Button>
        <Typography
          variant="h2"
          align="center"
          display="inline"
          style={{
            marginLeft: theme.spacing(2),
            fontSize: isDevicePhone ? "1.5em" : "24px",
          }}
        >
          Tổ chức của tôi
        </Typography>
      </div>
      {user && (
        <>
          <Grid container spacing={6}>
            <Grid item xs={12} md={4}>
              <OrgTrackingCard org={user} />
            </Grid>
            <Grid item xs={12} md={8}>
              <Typography variant="h2" style={{ marginBottom: 24 }}>
                <b>{user.name}</b>
              </Typography>
              <Typography variant="body1" gutterBottom>
                Giới thiệu tổ chức
              </Typography>
              <Typography variant="body1" color="textSecondary" gutterBottom>
                {user.description}
              </Typography>
              <Grid
                container
                justify="space-between"
                alignItems="center"
                style={{ marginTop: 32 }}
              >
                <Typography>
                  <b>Tuyển dụng</b>
                </Typography>
                <Button
                  variant="outlined"
                  startIcon="+"
                  onClick={() => setOpenJob(true)}
                >
                  Thêm vị trí tuyển dụng
                </Button>
                <CreateJobDialog
                  open={openJob}
                  handleClose={() => setOpenJob(false)}
                />
              </Grid>
              {user &&
                user?.recruitments?.map((recruitment, key) => (
                  <MediumJobCard key={key} data={recruitment} />
                ))}
              <Grid
                container
                justify="space-between"
                alignItems="center"
                style={{ marginTop: 32 }}
              >
                <Typography>
                  <b>Các sự kiện</b>
                </Typography>
                <Button
                  variant="outlined"
                  startIcon="+"
                  onClick={() => setOpen(true)}
                >
                  Thêm sự kiện
                </Button>
                <CreateEventDialogs
                  open={open}
                  handleClose={() => setOpen(false)}
                />
              </Grid>
              {user &&
                user?.events?.map((event) => (
                  <ResponsiveSideEventCard key={event.id} {...event} />
                ))}
            </Grid>
          </Grid>
        </>
      )}
    </Container>
  );
}

export default withLayout(MyOrg);
